# Singularity tests

The repository contains Singularity definitions and tests for the following computational software:

- FDMNES https://fdmnes.neel.cnrs.fr

The tests were run on the ESRF cluster and GSI's Virgo cluster. The nodes configuration is as follows:

**ESRF**
- Intel® Xeon® Gold 6248 Processor @ 2.50 GHz, 379 GB of RAM

**GSI** 
- Intel® Xeon® Gold 6248R Processor @ 3 GHz, 187 GB of RAM, and a comparably large swap partition
- AMD EPYC 7662 64-Core Processor @ 2 GHz, 1007 GB of RAM