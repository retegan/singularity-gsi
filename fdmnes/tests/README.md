# Results

The values in the table are the wall times in seconds.

| id    | ESRF | GSI (Intel) | GSI (AMD) |
| ----- | ---: | ----------: | --------: |
| test1 | 5031 |        8043 |     12576 |
