! FDMNES subroutine

! Set of subroutines for RIXS

!***********************************************************************

! nlm_f = nlmomax: number of harmonics in the outer sphere (= number of states at the current energy)
! nb_Emis = nbr of emission line ( 1 or 2, 2 for L23 for example)
! n_Emis first quantum number of the Emission line

subroutine main_RIXS(alfpot,Ampl_abs,Analyzer,Circular,coef_f,Coef_g,Core_resolved,dV0bdcF,E_cut,E_cut_imp,E_Emis, &
         E_Fermi,E_cut_man,Ecent,Eclie,Elarg,Eneg,Energ_loss_rixs,Energ_out_rixs,Energ_rixs,Energ_s, &
         Epsif,Epsif_ref,Epsif_ref_man,Epsii,Epsii_ref,Epsii_ref_man,Eseuil, &
         File_name_rixs,Full_potential,Gamma_hole,Gamma_hole_man,HERFD,Hubb_abs,Hubb_diag_abs, &
         iabsorig,icheck,isymeq,ip0,ip_max,is_f,is_g,J_Emis,Jseuil,L_Emis,Lmax_abs_t,Lmoins1,Lplus1,Lseuil,m_f,m_g,m_hubb, &
         Moment_conservation,mpirank0,multi_run,Multipole,n_atom_proto,n_atom_uc, &
         n_Emis,n_multi_run,n_q_rixs,n_rel,n_shift_core,n_theta_rixs,natomsym,nb_Emis,nbseuil,nenerg_in_rixs,nenerg_loss_rixs, &
         nenerg_out_rixs,nenerg_s,neqm,nfinal,nfinal1,nfinalr,ngamh,ninit,ninit1,ninitr,nlm_fe,nlm_pot, &
         nlm_probe,nlmamax,nlm_f,No_HERFD,nomfich,nr,nrm,nspin,nspino,nspinp,numat_abs,Only_indirect,Orthmati,Powder,Psi_Emis, &
         psii,q_rixs,r,Relativiste,Renorm,Rmtg_abs,Rmtsd_abs,Rot_atom_abs,Rot_int,rsato_abs,rsbdc,Shift_core, &
         Spinorbite,Tau_abs,Temp,Theta_rixs,V_intmax,V_hubb,Vcato,Vhbdc,VxcbdcF,Vxcato,Ylm_comp)

  use declarations
  implicit none

  integer, parameter:: ndim2 = 1
  integer, parameter:: nenerg_tddft = 0

  integer:: i_q, i_theta, iabsorig, ich, icheck, ie, ip0, ip_max, isp, J_emis, Jseuil, &
     L_emis, Lmax_pot, Lmax, Lmax_abs_t, Lseuil, m_hubb, mpirank0, multi_run, n_atom_proto, n_atom_uc, n_Emis, n_multi_run, &
     n_process, n_q_dim, n_q_rixs, n_rel, n_shift_core, n_theta_rixs, n_trans_out, natomsym, nb_Emis, nbseuil, ne_loss_or_emis, &
     nenerg_emis, nenerg_in, nenerg_in_rixs, nenerg_loss_rixs, nenerg_oc, nenerg_out_rixs, nenerg_unoc, nenerg_s, &
     neqm, nfinal, nfinal1, nfinalr, ngamh, ninit, ninit1, ninitr, nlm_f, nlm_p_fp, nlm_pot, nlm_probe, &
     nlmamax, nlms_n, npl, nr, nrm, nspin, nspino, nspinp, numat_abs, Output_option

  integer, dimension(natomsym):: isymeq
  integer, dimension(ninit,2):: m_g
  integer, dimension(ninit):: is_g
  integer, dimension(nfinal,2):: m_f
  integer, dimension(nfinal):: is_f
  integer, dimension(nenerg_s):: nlm_fe
  integer, dimension(0:n_atom_proto,neqm):: igreq

  character(len=Length_name):: File_name, nomfich
  character(len=Length_name), dimension(n_multi_run):: File_name_rixs

  complex(kind=db), dimension(nenerg_s,nlm_probe,nspinp,nlm_f,nspinp):: Ampl_abs
  complex(kind=db), dimension(-m_hubb:m_hubb,nspinp,-m_hubb:m_hubb,nspinp):: V_hubb
  complex(kind=db), dimension(nenerg_s,nlmamax,nspinp,nlmamax,nspinp):: Tau_abs

  complex(kind=db), dimension(:,:,:,:,:,:), allocatable:: Arof
  complex(kind=db), dimension(:,:,:,:,:,:,:), allocatable:: Rad_gon

  logical:: All_transitions, Analyzer, Circular, Core_resolved, Dip_rel, E_cut_man, E1E1, E1E2, E1E3, E1M1, E2E2, E3E3, &
    Epsif_ref_man, Epsii_ref_man, Extract_ten, Eneg, Energ_emission, Full_potential,  Gamma_hole_man, Gamma_max_man_L, &
    HERFD, Hubb_abs, Hubb_diag_abs, Lmoins1, Lplus1, M1M1, Magn_sens, Moment_conservation, Monocrystal, No_HERFD, Only_indirect, &
    Powder, Relativiste, Renorm, Spinorbite, Versus_loss, Ylm_comp 

  Logical, save:: Epsii_ref_RIXS_man, Epsif_ref_RIXS_man

  logical, dimension(10):: Multipole, Multipole_R

  real(kind=db), save:: Epsii_moy_min, Epsif_moy_min

  real(kind=db):: alfpot, Delta_E_emis, Delta_Epsif, E_cut, E_cut_imp, E_cut_o, E_cut_rixs, E_Fermi, Ecent, &
    Eclie, Eimag, Elarg, Energ_unoc_1, Epsif_moy, Epsif_ref, Epsii_moy, Epsii_ref, Gamma_max_L, &
    Rmtg_abs, Rmtsd_abs, Rot_sample, rsbdc, Shift_vs_XANES, Temp, Theta, Two_theta, V_intmax, Vhbdc

  real(kind=db), dimension(3):: Pol_i_s, Pol_i_p, Pol_s_s, Pol_s_p, Q_vec, Vec_i, Vec_s
  real(kind=db), dimension(10):: Gamma_hole
  real(kind=db), dimension(nenerg_s):: Energ_s
  real(kind=db), dimension(nbseuil):: Eseuil
  real(kind=db), dimension(nb_Emis):: E_Emis
  real(kind=db), dimension(ninitr):: Epsii
  real(kind=db), dimension(nfinalr):: Epsif
  real(kind=db), dimension(ninit,2):: coef_g
  real(kind=db), dimension(nfinal,2):: coef_f
  real(kind=db), dimension(nspin):: dV0bdcF, V0bdcF, VxcbdcF
  real(kind=db), dimension(nr):: r, rsato_abs
  real(kind=db), dimension(n_theta_rixs,2):: Theta_rixs
  real(kind=db), dimension(nenerg_loss_rixs):: Energ_loss_rixs
  real(kind=db), dimension(nenerg_out_rixs):: Energ_out_rixs
  real(kind=db), dimension(nenerg_in_rixs):: Energ_rixs
  real(kind=db), dimension(n_shift_core):: Shift_core
  real(kind=db), dimension(3,3):: Mat_orth, Orthmat, Orthmati, Rot_atom, Rot_atom_abs, Rot_int, &
                                  Trans_Lab_Dir_Q, Trans_rec_dir, Trans_rec_lab
  real(kind=db), dimension(3,n_atom_uc):: posn
  real(kind=db), dimension(4,n_q_rixs):: q_rixs
  real(kind=db), dimension(nrm,nbseuil):: psii
  real(kind=db), dimension(nrm,nb_Emis):: Psi_Emis
  real(kind=db), dimension(nr,nlm_pot):: Vcato
  real(kind=db), dimension(nr,nlm_pot,nspin):: Vxcato

  real(kind=db), dimension(:), allocatable:: Betalor, Conv_nelec, Energ_emis, Energ_in, Energ_oc, Exc_abs_i, Gamma, &
                                             Pseudo_shift_emis, Shift, Shift_emis, Vc_abs_i
  real(kind=db), dimension(:,:), allocatable:: V_abs_i
  real(kind=db), dimension(:,:,:), allocatable:: Pol_i_s_g, Pol_i_p_g, Pol_s_s_g, Pol_s_p_g, Vec_i_g, Vec_s_g

  if( icheck > 0 ) write(3,110)

  All_transitions = .true.
  
! When no momentum transfer direction given, calculation is done for powder 
  Monocrystal = .not. Powder

  if( No_HERFD .or. nenerg_in_rixs < 5 ) then
    Output_option = 1   ! no output versus Energ_in  (only emission or loss spectra plotable)
  elseif( nenerg_out_rixs < 5 .and. nenerg_loss_rixs < 5 ) then
    Output_option = 2  ! no output versus Energ_emis .or. Energ_loss  (only absorption spectra plotable )
  else
    Output_option = 0
  endif

  Energ_emission = nenerg_out_rixs > 0
  Eimag = 0._db
  Extract_ten = .false.
  
  if( multi_run == 1 ) then
    Epsii_ref_RIXS_man = Epsii_ref_man
    Epsif_ref_RIXS_man = Epsif_ref_man
  endif

  ich = max(icheck-1,1)
  Magn_sens = nspin == 2 .or. Spinorbite

! Spin position term (see Mas, Juhin et al.)  
  Dip_rel = n_rel > 1

! Number of in polarization to calculate  
  if( Circular ) then
    npl = 8
  elseif( Powder ) then
    npl = 6
  else
    npl = 4
  endif

  if( Hubb_abs .or. Full_potential ) then
    nlm_p_fp = nlm_probe
  else
    nlm_p_fp = 1
  endif

  Multipole_R(:) = Multipole(:)
  if( Multipole(6) ) Multipole_R(2) = .true.
  if( Multipole(8) ) Multipole_R(4) = .true.
  if( HERFD ) then
    Multipole_R(6) = .false.
    Multipole_R(8) = .false.
  endif
  
  E1E1 = Multipole_R(1); E1E2 = Multipole_R(2); E1E3 = Multipole_R(3);
  E1M1 = Multipole_R(4); E2E2 = Multipole_R(6);
  E3E3 = Multipole_R(7); M1M1 = Multipole_R(8)

  V0bdcF(:) = Vhbdc + VxcbdcF(:)  

  if( HERFD ) then
    allocate( Vc_abs_i(nr) ) 
    allocate( Exc_abs_i(nr) ) 
    allocate( V_abs_i(nr,nspin) )
     
    Vc_abs_i(:) = Vcato(:,1)
    do isp = 1,nspin
      V_abs_i(:,isp) = Vc_abs_i(:) +  Vxcato(:,1,isp)
    end do
    if( nspin == 1 ) then
      Exc_abs_i(:) = Vxcato(:,1,1)
    else
      Exc_abs_i(:) = 0.5_db * ( Vxcato(:,1,1) + Vxcato(:,1,2) )
    endif

    if( nb_Emis == 2 ) then
      Delta_Epsif = E_Emis(1) - E_Emis(2)
    else
      Delta_Epsif = 1000000._db
    endif
    call Energseuil(.true.,Core_resolved,Delta_Epsif,Delta_E_Emis,E_Fermi,.true.,Epsif,Epsif_moy,E_Emis, &
               Exc_abs_i,ich,is_f,J_emis,L_Emis,m_f,mpirank0,nb_Emis,nfinal1,nfinal,nfinalr,nr, nr,n_Emis, &
               nspin,numat_abs,Psi_Emis,R,Rmtg_abs,Rmtsd_abs,V_abs_i,V_intmax,Vc_abs_i,V0bdcF)

    deallocate( Vc_abs_i, Exc_abs_i, V_abs_i )
  endif
  
  Lmax = nint( sqrt( real( nlm_probe ) ) ) - 1
  Lmax_pot = nint( sqrt( real( nlm_pot ) ) ) - 1

  if( E_cut_man ) then
    E_cut_rixs = E_cut_imp
    E_cut_o = E_cut_imp / rydb
  else
    E_cut_rixs = E_cut
    E_cut_o = - 1001._db
  endif

! Energy grids
  
  do ie = 1,nenerg_s
    if( Energ_s(ie) > E_cut_RIXS + eps10 ) exit
  end do
  
  if( ie <= nenerg_s ) then
    Energ_unoc_1 = Energ_s(ie)
  else
    Energ_unoc_1 = Energ_s(nenerg_s)
  endif 

! E_cut rixs is not let equal to a grid point energy. 
  if( ie > 1 ) then
    if( abs( Energ_s(ie-1) - E_cut_RIXS ) < eps10 ) E_cut_RIXS = ( Energ_s(ie-1) + Energ_s(ie) ) / 2 
  endif
  
  nenerg_oc = ie - 1
  nenerg_unoc = nenerg_s - nenerg_oc 

  if( nenerg_in_rixs > 0 ) then
    nenerg_in = nenerg_in_rixs
  else 
    nenerg_in = nenerg_s
  endif

  if( HERFD ) then
    Versus_loss = .false.
    nenerg_emis = nfinalr
    ne_loss_or_emis = nenerg_emis 
  elseif( nenerg_loss_rixs > 0 ) then
    Versus_loss = .true.
    nenerg_emis = 0
    ne_loss_or_emis = nenerg_loss_rixs
  elseif( nenerg_out_rixs > 0 ) then
    Versus_loss = .false.
    nenerg_emis = nenerg_out_rixs
    ne_loss_or_emis = nenerg_emis 
  else 
    Versus_loss = .false.
    nenerg_emis = nenerg_oc
    ne_loss_or_emis = nenerg_emis 
  endif
  allocate( Energ_emis(nenerg_emis) )

  write(6,'(/A)') ' -- RIXS --- '
  write(6,'(a30,i5)') ' Number of incoming energies =', nenerg_in
  if( Versus_loss ) then
    write(6,'(a30,i5)') ' Number of loss energies     =', nenerg_loss_rixs
  else
    write(6,'(a30,i5)') ' Number of emission energies =', nenerg_emis
  endif
  
  allocate( Energ_oc(nenerg_oc) )
  allocate( Energ_in(nenerg_in) )
  allocate( Conv_nelec(nenerg_in) )

  do ie = 1,nenerg_oc
    Energ_oc(ie) = Energ_s(ie)
  end do
  if( nenerg_in_rixs > 0 ) then
    Energ_in(:) = Energ_rixs(:)
  else
    do ie = 1,nenerg_in
      Energ_in(ie) = Energ_s(ie + nenerg_s - nenerg_in)
    end do
  endif

  if( HERFD ) then    
  ! We neglect the magnetic splitting for the core level which must be smaller than for the emission level
  ! Epsif are difined as negative and Epsii as positive what explain the same sign in the formula below
    Energ_emis(:) = E_emis(nb_emis) - Shift_emis(:) - ( Epsii_moy - Epsii_ref ) 
  elseif( nenerg_out_rixs > 0 ) then
    Energ_emis(:) = Energ_out_rixs(:)
  else
    Energ_emis(:) = Energ_oc(:)
  endif

! Shifts of the core-hole levels
  allocate( Shift(ninitr) )
  call Cal_shift(.false.,Epsii,Epsii_moy,Epsii_moy_min,Epsii_ref,Epsii_ref_RIXS_man,icheck,multi_run,n_shift_core,ninit, &
                 ninit1,ninitr,Shift,Shift_core)

! Shift_vs_XANES is used in the final writing to shift all the spectra when the Epsii_moy minimum is not the first one 
  if( Epsii_ref_man .or. multi_run == 1 ) then
    Shift_vs_XANES = 0._db
  else
    Shift_vs_XANES = Epsii_ref - Epsii_moy_min  
  endif
  
  if( HERFD ) then
    allocate( Shift_emis(nfinalr) )
    allocate( Pseudo_shift_emis(0) )
    call Cal_shift(.true.,Epsif,Epsif_moy,Epsif_moy_min,Epsif_ref,Epsif_ref_RIXS_man,icheck,multi_run,0,nfinal, &
                   nfinal1,nfinalr,Shift_emis,Pseudo_shift_emis)
    deallocate( Pseudo_shift_emis )
  endif
  
! Broadening parameters
  allocate( Gamma(ninitr) )
  allocate( Betalor(nenerg_in) )

! Width of the core hole
  call Cal_width(Core_resolved,Eseuil,Gamma,Gamma_hole,Gamma_hole_man,Jseuil,nbseuil,ngamh,ninit1,ninitr,numat_abs)

  ! It seems, the broadening of the photoelectron states must not be taken into account in RIXS   
  Gamma_max_L = 0._db
  Gamma_max_man_L = .false.
  
  call GammArc(Ecent,Elarg,Gamma_max_L,E_cut_RIXS,nenerg_in,Energ_in,Betalor)
  Betalor(:) = 0.5_db * Betalor(:) 

! Output file name
  call Output_Rixs_Filename(E_cut_imp,E_cut_man,Epsii_ref,Epsii_ref_man,File_name,File_name_rixs,Gamma_hole(1), &
                                Gamma_hole_man,Gamma_max_L,Gamma_max_man_L,iabsorig,multi_run,n_multi_run,nomfich)

! Polarization and crystal 
  n_q_dim = max( n_q_rixs, 1 )
  
  allocate( Pol_i_s_g(3,n_theta_rixs,n_q_dim) ) 
  allocate( Pol_i_p_g(3,n_theta_rixs,n_q_dim) ) 
  allocate( Pol_s_s_g(3,n_theta_rixs,n_q_dim) ) 
  allocate( Pol_s_p_g(3,n_theta_rixs,n_q_dim) ) 
  allocate( Vec_i_g(3,n_theta_rixs,n_q_dim) ) 
  allocate( Vec_s_g(3,n_theta_rixs,n_q_dim) ) 

! Rotation to go from the local atom basis to the tensors R1 basis
  Rot_atom = matmul( Rot_int, transpose(Rot_atom_abs) )

! Transformation to go from crystal basis to orthonormal basis
  call invermat(Orthmati,Orthmat)
  Mat_orth = matmul( Rot_int, Orthmat )

  call Reciprocal_Vector(icheck,Mat_orth,Trans_rec_dir,Trans_rec_lab)

  do i_q = 1,n_q_dim

    if( n_q_rixs > 0 ) then
      Q_vec(:) = Q_rixs(1:3,i_q)
      Rot_sample = Q_rixs(4,i_q)
      Q_vec = matmul( Trans_rec_dir, Q_vec )
      Q_vec(:) = Q_vec(:) / sqrt( sum( Q_vec(:)**2 ) ) 
    else   ! default 
      Q_vec(1:2) = 0._db; Q_vec(3) = 1._db
      Rot_sample = 0._db
    endif

    call Cal_Mat_rot(icheck,Mat_orth,Q_vec,Rot_sample,Trans_Lab_Dir_Q,Trans_rec_dir,Trans_rec_lab)      

    do i_theta = 1,n_theta_rixs

      Theta = Theta_rixs(i_theta,1)      
      Two_theta = Theta_rixs(i_theta,2)   
      
      call Cal_Pol(icheck,Pol_i_s,Pol_i_p,Pol_s_s,Pol_s_p,Theta,Trans_Lab_Dir_Q,Two_theta,Vec_i,Vec_s)

      Pol_i_s_g(:,i_theta,i_q) = Pol_i_s(:)     
      Pol_i_p_g(:,i_theta,i_q) = Pol_i_p(:)     
      Pol_s_s_g(:,i_theta,i_q) = Pol_s_s(:)     
      Pol_s_p_g(:,i_theta,i_q) = Pol_s_p(:)     
      Pol_i_s_g(:,i_theta,i_q) = Pol_i_s(:)     
      Vec_i_g(:,i_theta,i_q) = Vec_i(:)     
      Vec_s_g(:,i_theta,i_q) = Vec_s(:)
           
    end do
  end do

  nlms_n = nlm_f * nspinp

! Calculation of the radial integral for the <g|o|n> core-continuum state transition  
  allocate( Rad_gon(nenerg_s,nlm_probe,nlm_p_fp,nspinp,nspino,ip0:ip_max,nbseuil) ) 

  call Cal_Rad_gon(alfpot,dV0bdcF,E_Fermi,Eclie,Eneg,Energ_s,Eseuil,Full_potential,Hubb_abs,Hubb_diag_abs,icheck, &
           ip0,ip_max,Lmax,Lmax_pot,m_hubb,nbseuil,nenerg_s,nlm_pot,nlm_p_fp,nlm_probe,nr,nrm,nspin,nspino,nspinp, &
           numat_abs,psii,r,Rad_gon,Relativiste,Renorm,Rmtg_abs,Rmtsd_abs,rsato_abs,rsbdc,Spinorbite,V_hubb,V_intmax, &
           Vcato,Vhbdc,VxcbdcF,Vxcato,Ylm_comp)

  allocate( Arof(nenerg_s,nlm_probe,nspinp,nlms_n,ip0:ip_max,nbseuil) )  

! Calculation of the radial inegral, time the amplitudes
  call Cal_Arof(Ampl_abs,Arof,ip0,ip_max,Lmax,nbseuil,nenerg_s,nlm_p_fp,nlm_probe,nlm_f,nlms_n, &
                nspino,nspinp,Rad_gon,Spinorbite)

  if( All_transitions .and. .not. HERFD ) then
    n_trans_out = 5
  else
    n_trans_out = 1
  endif

  if( HERFD ) then
    n_process = 1
  else
    n_process = 2
  endif
    
  if( icheck > 0 ) write(3,'(/A)') ' Direct process '
  write(6,'(/A)') ' Direct process '

  if( .not. Only_indirect ) &
    call Direct_process(Arof,Analyzer,Betalor,coef_f,Coef_g,Core_resolved,Dip_rel,E_cut_Rixs,E_Fermi,E1E1,E1E2,E1E3, &
      E1M1,E2E2,E3E3,Eclie,Eneg,Energ_emis,Energ_in,Energ_loss_rixs,Energ_s,Eseuil,File_name_rixs,Gamma, &
      HERFD,icheck,ip0,ip_max,is_g,isymeq,L_emis,Lmax,Lmoins1,Lplus1,Lseuil,m_f,m_g,M1M1, &
      Moment_conservation,Monocrystal,Multipole_R,multi_run,n_multi_run,n_process,n_q_dim,n_rel,n_theta_rixs,natomsym, &
      nb_Emis,nbseuil,ne_loss_or_emis,nenerg_emis,nenerg_in,nenerg_loss_rixs,nenerg_oc,nenerg_s,nfinal,nfinal1,nfinalr,ninit, &
      ninit1,ninitr,nlm_fe,nlm_probe,nlms_n,npl,nr,nrm,nspin, &
      nspinp,numat_abs,Output_option,Pol_i_s_g,Pol_i_p_g,Pol_s_s_g,Pol_s_p_g,Powder,psii,Psi_Emis,r, &
      Rot_atom,Shift,Spinorbite,Temp,Theta_rixs,V0bdcF,Vec_i_g,Vec_s_g,Versus_loss,Ylm_comp)

  if( .not. HERFD ) then
  
    if( icheck > 0 ) write(3,'(/A)') ' ----- Indirect process ------- '
    write(6,'(/A)') ' Indirect process'

    call Indirect_process(alfpot,Analyzer,Betalor,Coef_g,Core_resolved,Dip_rel,dV0bdcF,E_cut_Rixs, &
      E_Fermi,E1E1,E1E2,E1E3,E1M1,E2E2,E3E3,Eclie,Eneg,Energ_emis,Energ_in,Energ_loss_rixs,Energ_s,Eseuil,File_name_rixs, &
      Full_potential,Gamma,Hubb_abs,Hubb_diag_abs,icheck,ip0,ip_max,is_g,isymeq,Lmax,Lmax_abs_t,Lmax_pot,Lmoins1,Lplus1,Lseuil, &
      m_g,m_hubb,M1M1,Moment_conservation,Monocrystal,Multipole_R,multi_run,n_multi_run,n_process,n_q_dim,n_rel,n_theta_rixs, &
      n_trans_out,natomsym,nbseuil,ne_loss_or_emis,nenerg_emis,nenerg_in,nenerg_loss_rixs,nenerg_oc,nenerg_s,ninit,ninit1, &
      ninitr,nlm_f,nlm_p_fp,nlm_pot,nlm_probe,nlmamax,nlms_n,npl,nr,nspin, &
      nspino,nspinp,numat_abs,Output_option,Pol_i_s_g,Pol_i_p_g,Pol_s_s_g,Pol_s_p_g,Powder,r,Rad_gon,Relativiste, &
      Renorm,Rmtg_abs,Rmtsd_abs,Rot_atom,rsato_abs,rsbdc,Shift,Spinorbite,Tau_abs,Temp,Theta_rixs,V_hubb,V_intmax, &
      V0bdcF,Vcato,Vec_i_g,Vec_s_g,Versus_loss,Vhbdc,VxcbdcF,Vxcato,Ylm_comp)

  endif

  if( Versus_loss ) then
    if( .not. Only_indirect .and. n_process == 2 ) &
      call Write_out_RIXS_tot(File_name_rixs,multi_run,n_multi_run,nenerg_loss_rixs,nenerg_in,Output_option,Versus_loss)

    if( n_multi_run > 1 .and. multi_run == n_multi_run ) &
      call Write_out_RIXS_sum(Analyzer,Energ_loss_rixs,Energ_in,File_name_rixs,HERFD,multi_run,n_multi_run,n_process,n_theta_rixs, &
                         n_trans_out,nenerg_loss_rixs,nenerg_in,npl,Only_indirect,Output_option,Powder,Shift_vs_XANES,Versus_loss)
  else
    if( .not. Only_indirect .and. n_process == 2 ) &
      call Write_out_RIXS_tot(File_name_rixs,multi_run,n_multi_run,nenerg_emis,nenerg_in,Output_option,Versus_loss)

    if( n_multi_run > 1 .and. multi_run == n_multi_run ) &
      call Write_out_RIXS_sum(Analyzer,Energ_emis,Energ_in,File_name_rixs,HERFD,multi_run,n_multi_run,n_process,n_theta_rixs, &
                         n_trans_out,nenerg_emis,nenerg_in,npl,Only_indirect,Output_option,Powder,Shift_vs_XANES,Versus_loss)
  endif

  deallocate( Arof, Betalor, Energ_emis, Energ_in, Energ_oc, Gamma )
  deallocate( Pol_i_s_g, Pol_i_p_g, Pol_s_s_g, Pol_s_p_g, Rad_gon, Shift )
  deallocate( Vec_i_g, Vec_s_g )
  
  return
  110 format(/150('-'),//' RIXS step')
end

!***********************************************************************

subroutine Output_Rixs_Filename(E_cut_imp,E_cut_man,Epsii_ref,Epsii_ref_man,File_name,File_name_rixs,Gamma_hole, &
                                Gamma_hole_man,Gamma_max,Gamma_max_man,iabsorig,multi_run,n_multi_run,nomfich)

  use declarations
  implicit none

  integer:: i, iabsorig, k, L, Length, multi_run, n_multi_run
  
  character(len=10):: mot10
  character(len=Length_name):: File_name, nomfich
  character(len=Length_name), dimension(n_multi_run):: File_name_rixs
  
  logical:: E_cut_man, Epsii_ref_man, Gamma_hole_man, Gamma_max_man

  real(kind=db):: E_cut_imp, Epsii_ref, Gamma_hole, Gamma_max, x, y

  File_name = nomfich
  Length = len_trim( File_name )
  do k = 1,4
  
    select case(k)
      case(1)
       if( .not. E_cut_man ) cycle
       y = E_cut_imp * Rydb
      case(2)
       if( .not. Gamma_max_man ) cycle
       y = Gamma_max * Rydb
      case(3)
       if( .not. Gamma_hole_man ) cycle
       y = Gamma_hole * Rydb
      case(4)
       if( .not. Epsii_ref_man ) cycle
       y = Epsii_ref * Rydb          
    end select
    
    x = y / 10
    do i = 0,4
      x = 10 * x
      if( abs( x - nint(x) ) < eps10 ) exit
    end do
    
    select case(i)
      case(0)
        write(mot10,'(i10)') nint( y )
      case(1)
        write(mot10,'(f10.1)') y
      case(2)
        write(mot10,'(f10.2)') y
      case(3)
        write(mot10,'(f10.3)') y
      case default
        write(mot10,'(f10.4)') y
    end select
    
    mot10 = adjustl( mot10 )   
    L = len_trim( mot10 )
    if( i > 0 ) mot10(L-i:L-i) = 'p'
  
    Length = len_trim( File_name ) + 1

    select case(k)
      case(1)          
        File_name(Length:Length+2) = '_EF'
      case(2)
        File_name(Length:Length+2) = '_Gm'
      case(3)
        File_name(Length:Length+2) = '_GH'
      case(4)
        File_name(Length:Length+2) = '_Ep'
      case(5)
        File_name(Length:Length+2) = '_Ga'
      case(6)
        File_name(Length:Length+2) = '_Ui'
    end select
    Length = Length + 2
    
    File_name(Length+1:Length+L) = mot10(1:L)
    
  end do
  
  Length = len_trim( File_name )

  File_name(Length+1:Length+5) = '_rixs'

  if( n_multi_run > 1 ) then
    Length = Length + 6
    File_name(Length:Length) = '_'
    call ad_number(iabsorig,File_name,Length_name)
  endif
  Length = len_trim(File_name)
  File_name(Length+1:Length+4) = '.txt'
  File_name_rixs(multi_run) = File_name

  return
end

!***************************************************************

! Calculation of the shifts

subroutine Cal_shift(Emission,Epsii,Epsii_moy,Epsii_moy_min,Epsii_ref,Epsii_ref_RIXS_man,icheck,multi_run,n_shift_core,ninit, &
                     ninit1,ninitr,Shift,Shift_core)

  use declarations
  implicit none

  integer:: icheck, multi_run, n_shift_core, ninit, ninit1, ninitr
  
  logical:: Emission, Epsii_ref_RIXS_man
  
  real(kind=db):: Epsii_moy, Epsii_moy_min, Epsii_ref
  real(kind=db), dimension(ninitr):: Epsii, Shift
  real(kind=db), dimension(n_shift_core):: Shift_core
   
  select case(ninitr)
    case(1)
      Epsii_moy = Epsii(1)
    case(2)
      if( ninit1 /= ninit ) then
        Epsii_moy = Epsii(2)
      else
        Epsii_moy = sum( Epsii(1:ninitr) ) / ninitr
      endif
    case(4,6,10)
 ! in these case ninit = ninitr
      if( ninit1 /= ninit ) then
        Epsii_moy = sum( Epsii(ninit1+1:ninitr) ) / ( ninitr - ninit1 )
      else
        Epsii_moy = sum( Epsii(1:ninitr) ) / ninitr
      endif
  end select

  if( .not. Epsii_ref_RIXS_man ) then
! First calculation site becomes the reference
    Epsii_ref = Epsii_moy
    Epsii_moy_min = Epsii_moy
    Epsii_ref_RIXS_man = .true.
  endif
  Epsii_moy_min = min( Epsii_moy, Epsii_moy_min ) 

  Shift(:) = Epsii(:) - Epsii_ref 

 ! Shift_core is in option, not in the manual, to impose shifts at specific sites
  if( multi_run <= n_shift_core ) Shift(:) = Shift(:) + Shift_core(multi_run) 

  if( icheck > 0 ) then
    if( Emission ) then
      write(3,'(/A)') ' Shift_emission(i = 1,nfinalr)'
    else
      write(3,110) 
      write(3,'(/A)') ' Shift(i = 1,ninitr)'
    endif
    write(3,'(10f9.5)') Shift(:) * rydb
  endif  
    
  return
110 format(/'---------- RIXS shift ',80('-'))
end

!*****************************************************************************************************************

! calculation of core state width

subroutine Cal_width(Core_resolved,Eseuil,Gamma,Gamma_hole,Gamma_hole_man,Jseuil,nbseuil,ngamh,ninit1,ninitr,numat_abs)

  use declarations
  implicit none

  integer:: initr, iseuil, Js, Jseuil, nbseuil, ngamh, ninit1, ninitr, numat_abs
  
  logical:: Core_resolved, Gamma_hole_man
 
  real(kind=db):: Tab_width 
  real(kind=db), dimension(10):: Gamma_hole
  real(kind=db), dimension(ninitr):: Gamma
  real(kind=db), dimension(nbseuil):: Eseuil
  
  do initr = 1,ninitr       ! ----------> Loop over edges or core states

    if( Core_resolved ) then
      if( initr <= ninit1 ) then
        iseuil = 1
      else
        iseuil = min(2, nbseuil)
      endif
    else
      iseuil = min(initr, nbseuil)
    endif

    if( Gamma_hole_man ) then
      if( ngamh == 1 ) then
        Gamma(initr) = Gamma_hole(1)
      elseif( ngamh == ninitr ) then
        Gamma(initr) = Gamma_hole(initr)
      elseif( initr <= ninit1 ) then
        Gamma(initr) = Gamma_hole(1)
      else
        Gamma(initr) = Gamma_hole(2)
      endif
    else
      Js = Jseuil + iseuil - 1 
      Gamma_hole(1) = Tab_width(Eseuil(iseuil),Js,nbseuil,numat_abs)
      Gamma(initr) = Gamma_hole(1)
    endif
    Gamma(initr) = 0.5_db * Gamma(initr)  ! It is in fact Gamma / 2 in the formula  

  end do
  
  return
end

!***************************************************************

! Calculation of the reciprocal vector in the lab frame

subroutine Reciprocal_Vector(icheck,Mat_orth,Trans_rec_dir,Trans_rec_lab)

  use declarations
  implicit none

  integer:: i, icheck
  
  real(kind=db):: Cos_a, Cos_b, Cos_t, Phi, Sin_t, Theta, Vol, Wx_mod, Wy_mod, Wz_mod 
  real(kind=db), dimension(3):: Vec, Vx, Vy, Vz, Wx, Wy, Wx_lab, Wy_lab, Wz, Wz_lab
  real(kind=db), dimension(3,3):: Mat_orth, Trans_rec_dir, Trans_rec_lab 
  
! Diffraction vector in the internal orthogonal basis
  Vx(:) = Mat_orth(:,1)
  Vy(:) = Mat_orth(:,2)
  Vz(:) = Mat_orth(:,3)

! Wx, Wy, Wz : reciprocal cell basis
  call prodvec(Wx,vy,vz)

  vol = sum( Wx(:) * vx(:) )
  Wx(:) = Wx(:) / vol
  call prodvec(Wy,vz,vx)
  Wy(:) = Wy(:) / vol
  call prodvec(Wz,vx,vy)
  Wz(:) = Wz(:) / vol
  Trans_rec_dir(1,:) = Wx(:)
  Trans_rec_dir(2,:) = Wy(:)
  Trans_rec_dir(3,:) = Wz(:)

! Calculation of the matrix to transform the diffraction vector in the Lab basis, at origin, that is before any rotation:
! x: vertical
! y: horizontal, along the incoming beam
! z: horizontal, perpendicular to the incoming beam

  Wx_mod = sqrt( dot_product( Wx, Wx ) )
  Wy_mod = sqrt( dot_product( Wy, Wy ) )
  Wz_mod = sqrt( dot_product( Wz, Wz ) )

  call prodvec(Vec,Wx,Wy)
  Vec(:) = Vec(:) / sqrt( dot_product( Vec, Vec ) )
        
! Basis Vx vertical, Wy in the (incoming, Vx) plane
  Wx_lab(1) = Wx_mod; Wx_lab(2) = 0._db; Wx_lab(3) = 0._db;
  Cos_t = dot_product( Wx, Wy ) / ( Wx_mod * Wy_mod ) 
  Theta = acos( Cos_t )
  Sin_t = sin( Theta )
  Wy_lab(1) = Cos_t * Wy_mod; Wy_lab(2) = sin_t * Wy_mod; Wy_lab(3) = 0._db;

  call prodvec(Vz,Wx,Wy)
  Vz(:) = Vz(:) / sqrt( dot_product( Vz, Vz ) )
  call prodvec(Vy,Vz,Wx)
  Vy(:) = Vy(:) / sqrt( dot_product( Vy, Vy ) )
  Cos_t = dot_product( Wz, Vz ) / Wz_mod
  Theta = acos( Cos_t )
  Sin_t = sin( Theta ) 

  if( abs( Cos_t - 1._db ) < eps10 ) then
    Wz_lab(1) = 0._db; Wz_lab(2) = 0._db; Wz_lab(3) = Wz_mod
  elseif(  abs( Cos_t + 1._db ) < eps10 ) then
    Wz_lab(1) = 0._db; Wz_lab(2) = 0._db; Wz_lab(3) = -Wz_mod
  else
    Cos_a = dot_product( Wz, Vx ) / Wz_mod 
    Cos_b = dot_product( Wz, Vy ) / Wz_mod
    if( abs( Cos_a ) < eps10 ) then
      Wz_lab(1) = 0._db; Wz_lab(2) = Sin_t * Wz_mod; Wz_lab(3) = Cos_t * Wz_mod
    else
      Phi = atan( Cos_b / Cos_a )
      if( Cos_a < 0._db ) Phi = Phi + pi  
      Wz_lab(1) = Sin_t * cos( Phi ) * Wz_mod ; Wz_lab(2) = Sin_t * sin( Phi ) * Wz_mod; Wz_lab(3) = Cos_t * Wz_mod
    endif
  endif

  Trans_rec_lab(:,1) = Wx_lab(:)
  Trans_rec_lab(:,2) = Wy_lab(:)
  Trans_rec_lab(:,3) = Wz_lab(:)
  
  if( icheck > 0 ) then
    write(3,110)
    write(3,'(/A)') ' Reciprocal vector in lab frame:'
    write(3,'(A)') '      A         B        C'
    do i = 1,3
      write(3,'(3f10.5)') Trans_rec_lab(i,:)
    end do
  endif 

  return
110 format(/'---------- Basis in RIXS ',80('-'))
end

!***************************************************************

! Calculation of transormation matrix to have polarizations and wave vectors in crystal basis

subroutine Cal_Mat_rot(icheck,Mat_orth,Q_vec,Rot_sample,Trans_Lab_Dir_Q,Trans_rec_dir,Trans_rec_lab)

  use declarations
  implicit none

  integer:: i, icheck
  
  real(kind=db):: Cos_p, Cos_t, Phi, Q_mod, Rot_sample, Sin_p, Sin_t, Theta 
  real(kind=db), dimension(3):: Q_lab, Q_dir, Q_vec, Vec
  real(kind=db), dimension(3,3):: Mat, Mat_orth, Rot, Rot_1, Rot_2, Trans_Lab_Dir_Q, Trans_rec_dir, Trans_rec_lab
  
  Q_lab(:) = Matmul( Trans_rec_lab, Q_vec )
  Q_mod = sqrt( sum( Q_lab(:)**2 ) )
  if( abs(Q_mod) > eps10 ) Q_lab(:) = Q_lab(:) / Q_mod
    
! Crystal rotation to be with Q along z lab
  if( abs( Q_lab(2) ) > eps10 ) then
    if( abs( Q_lab(3) ) < eps10 .and. Q_lab(2) > eps10 ) then
      Phi = pi / 2
    elseif( abs( Q_lab(3) ) < eps10 .and. Q_lab(2) < - eps10 ) then
      Phi = - pi / 2
    else
      Phi = atan( Q_lab(2) / Q_lab(3) )
      if( Q_lab(2) < 0._db ) Phi = Phi + pi
      Phi = pi / 2 - Phi
    endif
    Cos_p = cos( Phi )
    Sin_p = sin( Phi )
    Rot_1(1,1) = 1._db; Rot_1(1,2) = 0._db; Rot_1(1,3) = 0._db  
    Rot_1(2,1) = 0._db; Rot_1(2,2) = Cos_p; Rot_1(2,3) = - Sin_p  
    Rot_1(3,1) = 0._db; Rot_1(3,2) = Sin_p; Rot_1(3,3) = Cos_p  
  else
    Rot_1(1,1) = 1._db; Rot_1(1,2) = 0._db; Rot_1(1,3) = 0._db  
    Rot_1(2,1) = 0._db; Rot_1(2,2) = 1._db; Rot_1(2,3) = 0._db  
    Rot_1(3,1) = 0._db; Rot_1(3,2) = 0._db; Rot_1(3,3) = 1._db
  endif
  
  Theta = - pi/2 + acos( Q_lab(1) )   ! rotation is negative
  Sin_t = sin( Theta )
  Cos_t = cos( Theta )
  Rot_2(1,1) = Cos_t; Rot_2(1,2) = 0._db; Rot_2(1,3) = Sin_t  
  Rot_2(2,1) = 0._db; Rot_2(2,2) = 1._db; Rot_2(2,3) = 0._db  
  Rot_2(3,1) = - Sin_t; Rot_2(3,2) = 0._db; Rot_2(3,3) = Cos_t
  
  Rot = Matmul( Rot_2, Rot_1 ) 

! Origin of crystal rotation around Q is such that a is perpendicular to the incoming beam
  Q_dir = Matmul( Trans_rec_dir, Q_vec )

  Mat = Matmul( Rot, Mat_orth )
  
  if( abs( Q_dir(2) ) > eps10 .or. abs( Q_dir(3) ) > eps10 ) then
    Vec(1) = 1._db; Vec(2) = 0._db;; Vec(3) = 0._db; 
  else
    Vec(1) = 0._db; Vec(2) = 0._db;; Vec(3) = 1._db; 
  endif  
  Vec = Matmul( Mat, Vec )
  Vec(:) = Vec(:) / sqrt( sum( Vec(:)**2 ) )

  if( abs( Vec(2) ) > eps10 ) then
  
    if( abs( Vec(1) ) > eps10 ) then
      Phi = atan( Vec(2) / Vec(1) )
      if( Vec(2) < 0._db ) Phi = Phi + pi
    elseif( Vec(2) > eps10 ) then
      Phi = - pi / 2
    else
      Phi = pi / 2
    endif
  else
    Phi = 0._db
  endif
  
  Phi = Phi + Rot_sample 

  if( abs( Phi ) > eps10 ) then
    Cos_p = cos( Phi )
    Sin_p = sin( Phi )
    Rot_1(1,1) = Cos_p; Rot_1(1,2) = - Sin_p; Rot_1(1,3) = 0._db  
    Rot_1(2,1) = Sin_p; Rot_1(2,2) = Cos_p;   Rot_1(2,3) = 0._db   
    Rot_1(3,1) = 0._db; Rot_1(3,2) = 0._db;   Rot_1(3,3) = 1._db   

    Rot = Matmul( Rot_1, Rot ) 
  endif
  
  Trans_Lab_Dir_Q = Transpose( Rot )  ! it is to transform vec and pol in lab frame to crystal frame 

  if( icheck > 0 ) then
    write(3,110) Q_vec(:), Rot_sample / radian
    write(3,'(/A)') ' Tr Lab frame - Othonormal unit cell basis'
    do i = 1,3
      write(3,'(3f10.5)') Trans_Lab_Dir_Q(i,:)
    end do 
  endif
  
  return
  110 format(/' Q =',3f10.5,', Azimuthal rotation sample = ',f8.3,' degrees')
end

!***************************************************************

! Calculation of the Polarization and wave vector

subroutine Cal_Pol(icheck,Pol_i_s,Pol_i_p,Pol_s_s,Pol_s_p,Theta,Trans_Lab_Dir_Q,Two_theta_L,Vec_i,Vec_s)

  use declarations
  implicit none

  integer:: i, icheck
  
  real(kind=db):: Cos_2t, Cos_t, Sin_2t, Sin_t, Theta, Two_theta_L 
  real(kind=db), dimension(3):: Pol_i_s, Pol_i_p, Pol_s_s, Pol_s_p, Vec_i, Vec_s
  real(kind=db), dimension(3,3):: Rot, Trans_Lab_Dir_Q
  
  Cos_2t = cos( pi - Two_theta_L )
  Sin_2t = sin( pi - Two_theta_L )

  Cos_2t = cos( Two_theta_L )
  Sin_2t = sin( Two_theta_L )

! Wave vector and polarization in lab frame with 
! x vertical, y along the incoming beam, z horizontal perp to the incoming beam
! The scheme corresponds to a vertical sample, sigma polarization is perpendicular to the scattering plane,
! that is vertical in the lab, along x
  Vec_i(1) = 0._db;   Vec_i(2) = 1._db;  Vec_i(3) = 0._db  
  Vec_s(1) = 0._db;   Vec_s(2) = Cos_2t; Vec_s(3) = Sin_2t
  Pol_i_s(1) = 1._db; Pol_i_s(2) = 0._db; Pol_i_s(3) = 0._db   
  Pol_i_p(1) = 0._db; Pol_i_p(2) = 0._db; Pol_i_p(3) = 1._db   
  Pol_s_s(1) = 1._db; Pol_s_s(2) = 0._db; Pol_s_s(3) = 0._db   
  Pol_s_p(1) = 0._db; Pol_s_p(2) = - Sin_2t; Pol_s_p(3) = Cos_2t   

  Cos_t = cos( pi/2 - Theta )
  Sin_t = sin( pi/2 - Theta )  

  Cos_t = cos( Theta )
  Sin_t = sin( Theta )  

  Rot(1,1) = 1._db; Rot(1,2) = 0._db; Rot(1,3) = 0._db
  Rot(2,1) = 0._db; Rot(2,2) = Cos_t; Rot(2,3) = - Sin_t
  Rot(3,1) = 0._db; Rot(3,2) = Sin_t; Rot(3,3) = Cos_t
  Rot = Transpose( Rot )  ! it is to transform vec and pol in lab frame to crystal frame 
  
  Rot = Matmul( Trans_Lab_Dir_Q, Rot )
  
  Pol_i_s = Matmul( Rot, Pol_i_s )
  Pol_i_s(:) = Pol_i_s(:) / sqrt( sum( Pol_i_s(:)**2 ) ) 
  Pol_i_p = Matmul( Rot, Pol_i_p )
  Pol_i_p(:) = Pol_i_p(:) / sqrt( sum( Pol_i_p(:)**2 ) ) 
  Pol_s_s = Matmul( Rot, Pol_s_s )
  Pol_s_s(:) = Pol_s_s(:) / sqrt( sum( Pol_s_s(:)**2 ) ) 
  Pol_s_p = Matmul( Rot, Pol_s_p )
  Pol_s_p(:) = Pol_s_p(:) / sqrt( sum( Pol_s_p(:)**2 ) ) 
  Vec_i = Matmul( Rot, Vec_i )
  Vec_i(:) = Vec_i(:) / sqrt( sum( Vec_i(:)**2 ) ) 
  Vec_s = Matmul( Rot, Vec_s )
  Vec_s(:) = Vec_s(:) / sqrt( sum( Vec_s(:)**2 ) ) 

  if( icheck > 0 ) then
    write(3,110) Theta/radian, Two_theta_L/radian 
    write(3,'(/A)') '   Pol_i_sig  Pol_i_pi   Pol_s_sig  Pol_s_pi   WaveVec_i  WaveVec_s  (in orthonormal unit cell basis)'
    do i = 1,3
      write(3,'(7f11.5)') Pol_i_s(i), Pol_i_p(i), Pol_s_s(i), Pol_s_p(i), Vec_i(i), Vec_s(i)  
    end do   
  endif
  return
  110 format(/' Theta_in =',f10.5,', 2Theta =',f10.5,' degrees')
end

!*****************************************************************************************************************

! Calculation of the radial matrix <g|o|n>

subroutine Cal_Rad_gon(alfpot,dV0bdcF,E_Fermi,Eclie,Eneg,Energ_s,Eseuil,Full_potential,Hubb_abs,Hubb_diag_abs,icheck, &
           ip0,ip_max,Lmax,Lmax_pot,m_hubb,nbseuil,nenerg_s,nlm_pot,nlm_p_fp,nlm_probe,nr,nrm,nspin,nspino,nspinp, &
           numat_abs,psii,r,Rad_gon,Relativiste,Renorm,Rmtg_abs,Rmtsd_abs,rsato_abs,rsbdc,Spinorbite,V_hubb,V_intmax, &
           Vcato,Vhbdc,VxcbdcF,Vxcato,Ylm_comp)

  use declarations
  implicit none

  integer:: ich, icheck, ie, ip, ip0, ip_max, iseuil, iso, isp, Lm, Lmax, Lmax_pot, Lmp, m_hubb, nbseuil, nenerg_s, nlm_pot, &
    nlm_p_fp, nlm_probe, nr, nrm, nspin, nspino, nspinp, numat_abs

  character(len=4), dimension(16):: Lm_string
  character(len=1), dimension(2):: Spin

  complex(kind=db), dimension(-m_hubb:m_hubb,nspinp,-m_hubb:m_hubb,nspinp):: V_hubb
  complex(kind=db), dimension(nenerg_s,nlm_probe,nlm_p_fp,nspinp,nspino,ip0:ip_max,nbseuil):: Rad_gon
  complex(kind=db), dimension(:,:,:,:,:,:), allocatable:: rof

  logical:: Eneg, Final_tddft, Full_potential, Hubb_abs, Hubb_diag_abs, NRIXS, Relativiste, Renorm, Spinorbite, Ylm_comp
       
  real(kind=db):: alfpot, E_Fermi, Eclie, Eimag, Rmtg_abs, Rmtsd_abs, rsbdc, V_intmax, Vhbdc
  real(kind=db):: Enervide
  real(kind=db), dimension(nspin):: dV0bdcF, Ecinetic, V0bdc, VxcbdcF
  real(kind=db), dimension(nr):: r, rsato_abs
  real(kind=db), dimension(nbseuil):: Eseuil
  real(kind=db), dimension(nenerg_s):: Energ_s
  real(kind=db), dimension(nrm,nbseuil):: psii
  real(kind=db), dimension(nr,nlm_pot):: Vcato
  real(kind=db), dimension(nr,nlm_pot,nspin):: Vrato, Vxcato
  real(kind=db), dimension(nr,ip0:ip_max):: r_or_bess

  data Lm_string/ '0,+0','1,-1','1,+0','1,+1','2,-2','2,-1','2,+0','2,+1','2,+2','3,-3','3,-2','3,-1','3,+0','3,+1','3,+2','3,+3' /
  data Spin/ 'u','d' /

  Eimag = 0._db
  Final_tddft = .false.
  NRIXS = .false.

  ich = max(0, icheck-1)

  if( icheck > 1 ) write(3,100)

  allocate( rof(nlm_probe,nlm_p_fp,nspinp,nspino,ip0:ip_max,nbseuil) )

  do ip = ip0,ip_max
    select case(ip)
      case(0)
        r_or_bess(1:nr,ip) = 1._db
      case(1)
        r_or_bess(1:nr,ip) = r(1:nr)
      case default
        r_or_bess(1:nr,ip) = r(1:nr)**ip
    end select
  end do

  Lmax = nint( sqrt( real( nlm_probe ) ) ) - 1
 
  do ie = 1,nenerg_s

    Enervide = Energ_s(ie) + E_Fermi

    call Potex_abs(alfpot,dV0bdcF,Enervide,ich,nlm_pot,nr,nspin,.false.,r,rsato_abs,rsbdc,V_intmax, &
                           V0bdc,Vcato,Vhbdc,Vrato,Vxcato,VxcbdcF)

    Ecinetic(:) = Enervide - V0bdc(:)
    if( .not. Eneg ) Ecinetic(:) = max( Ecinetic(:), Eclie )

    rof(:,:,:,:,:,:) = (0._db, 0._db)

    call radial_RIXS(E_Fermi,Ecinetic,Eimag,Energ_s(ie),Enervide,Eseuil,Final_tddft,Full_potential,Hubb_abs,Hubb_diag_abs,ich, &
         1,ip_max,ip0,Lmax,Lmax_pot,m_hubb,nbseuil,nlm_pot,nlm_probe,nlm_p_fp,nr,NRIXS,nrm,nspin,nspino, &
         nspinp,numat_abs,psii,r,r_or_bess,Relativiste,Renorm,Rmtg_abs,Rmtsd_abs,rof,Spinorbite,V_hubb,V_intmax,V0bdc,Vrato, &
         Ylm_comp)
         
    Rad_gon(ie,:,:,:,:,:,:) = rof(:,:,:,:,:,:) 

  end do
  
  if( icheck > 1 ) then
    if( nlm_p_fp == 1 .and. nbseuil == 1 .and. nspinp == 1 .and. nspino == 1 ) then
      write(3,110) (( Lm_string(Lm), ip, Lm = 1,nlm_probe), ip = ip0,ip_max )
    elseif( nlm_p_fp == 1 .and. nbseuil == 1 .and. nspinp == 1 ) then
      write(3,120) ((( Lm_string(Lm), Spin(isp), ip, Lm = 1,nlm_probe), isp = 1,nspinp), ip = ip0,ip_max )
    else
      write(3,130) (((((( Lm_string(Lm), Spin(isp), Spin(iso), ip, Lm = 1,nlm_probe), isp = 1,nspinp), iso = 1,nspino), &
                                                                 ip = ip0,ip_max ), Lmp = 1,nlm_p_fp), iseuil = 1,nbseuil ) 
    endif 
    do ie = 1,nenerg_s
      write(3,140) Energ_s(ie)*rydb, (((((( real( Rad_gon(ie,Lm,Lmp,:,:,:,:) ), Lm = 1,nlm_probe),  &
                                        isp = 1,nspinp), iso = 1,nspino), ip = ip0,ip_max), Lmp = 1,nlm_p_fp), iseuil = 1,nbseuil )
    end do   
  endif
    
  deallocate( rof )
  
  return
  100 format(/'---------- Cal_Rad_gon ',80('-'))
  110 format(/10x,'Rad_gon(L,m,ip)   ip = dipole,...',/'    Energy',10000(3x,'(',a4,',',i1,')',2x))
  120 format(/10x,'Rad_gon(L,m,spin,ip)   ip = dipole,...',/'    Energy',10000(2x,'(',a4,a1,',',i1,')',2x))
  130 format(10x,'Rad_gon(L,m,spin,sol,ip)   ip = dipole,...',/'    Energy',10000(2x,'(',a4,a1,a1,',',i1,')',1x))
  140 format(f10.3,1p,10000e13.5)
end

!*****************************************************************************************************************

! Calculation of the radial matrix rof times amplitude

subroutine Cal_Arof(Ampl_abs,Arof,ip0,ip_max,Lmax,nbseuil,nenerg_s,nlm_p_fp,nlm_probe,nlm_f,nlms_n, &
                nspino,nspinp,Rad_gon,Spinorbite)

  use declarations
  implicit none

  integer:: ie, ip, ip0, ip_max, iso, isp, isp_f, iss, L, Lm, Lm_f, Lmax, Lmp, Lms_f, &
    Lmv, Lp, m, mp, mv, nbseuil, nenerg_s, nlm_f, nlm_p_fp, nlm_probe, nlms_n, nspino, nspinp

  complex(kind=db), dimension(nenerg_s,nlm_probe,nspinp,nlm_f,nspinp):: Ampl_abs
  complex(kind=db), dimension(nenerg_s,nlm_probe,nspinp,nlms_n,ip0:ip_max,nbseuil):: Arof
  complex(kind=db), dimension(nenerg_s,nlm_probe,nlm_p_fp,nspinp,nspino,ip0:ip_max,nbseuil):: Rad_gon

  logical:: Spinorbite
  
  Arof(:,:,:,:,:,:) = ( 0._db, 0._db )  

  do ie = 1,nenerg_s

! Loop over the final states (outer sphere)
    Lms_f = 0
    do isp_f = 1,nspinp
      do Lm_f = 1,nlm_f
        Lms_f = Lms_f + 1 
      
        do isp = 1,nspinp
          if( .not. Spinorbite .and. isp /= isp_f ) cycle
          Lm = 0
          do L = 0,Lmax
            do m = - L,L
              Lm = Lm + 1

              Lmp = 0
              do Lp = 0,Lmax
                if( nlm_p_fp == 1 .and. Lp /= L ) cycle
                do mp = - Lp,Lp
                  if( nlm_p_fp == 1 .and. mp /= m ) cycle
                  Lmp = Lmp + 1
                  
                  if( nlm_p_fp == 1 ) then
                    Lmv = Lm
                  else
                    Lmv = Lmp
                  endif 

                  do iso = 1,nspino
                    if( Spinorbite ) then
                      mv = mp - isp + iso   ! faux m, celui de l'amplitude
                      if( mv > L .or. mv < -L ) cycle
                      Lmv = Lmv - isp + iso
                      iss = iso
                    else
                      iss = isp
                    endif

                    do ip = ip0,ip_max
                      Arof(ie,Lm,isp,Lms_f,ip,:) = Arof(ie,Lm,isp,Lms_f,ip,:) &
                                                 + Rad_gon(ie,Lm,Lmp,isp,iso,ip,:) * Ampl_abs(ie,Lmv,iss,Lm_f,isp_f)  
                    end do  
                  end do
                end do
                
              end do
               
            end do
          end do
        end do
    
      end do
    end do

  end do
  
  return
end

!**********************************************************************

! Calculation of radial integrals 

subroutine radial_RIXS(E_Fermi,Ecinetic,Eimag,Energ,Enervide,Eseuil,Final_tddft,Full_potential,Hubb_abs,Hubb_diag_abs,icheck, &
         initlv,ip_max,ip0,Lmax,Lmax_pot,m_hubb,nbseuil,nlm_pot,nlma,nlma2,nr,NRIXS,nrm,nspin,nspino, &
         nspinp,numat_abs,psii,r,r_or_bess,Relativiste,Renorm,Rmtg_abs,Rmtsd_abs,rof,Spinorbite,V_hubb,V_intmax,V0bdc,Vrato, &
         Ylm_comp)

  use declarations
  implicit none

  integer:: i, ich, icheck, initl, initlv, ip, ip_max, ip0, iseuil, isp, L, l_hubbard, lfin, lm, Lmax, &
    Lmax_pot, lmp, lp, m, m_hubb, nlm1, nlm2, mp, nbseuil, nlm, nlm_pot, nlma, nlma2, nr, nrm, &
    nrmtsd, nrmtg, nspin, nspino, nspinp, numat_abs

  character(len=104):: mot

  complex(kind=db), dimension(nspin):: konde
  complex(kind=db), dimension(nlma,nlma2,nspinp,nspino,ip0:ip_max,nbseuil):: rof
  complex(kind=db), dimension(-m_hubb:m_hubb,nspinp,-m_hubb:m_hubb,nspinp):: V_hubb
  complex(kind=db), dimension(:,:,:,:), allocatable:: Tau

  logical:: Ecomp, Final_tddft, Full_potential, Hubb_abs, &
    Hubb_diag_abs, Hubb_m, NRIXS, Radial_comp, Relativiste, Renorm, Spinorbite, Ylm_comp

  real(kind=db):: E_Fermi, Eimag, Energ, Enervide, Ephoton, Rmtg_abs, Rmtsd_abs, V_intmax
  real(kind=db), dimension(nspin):: Ecinetic, V0bdc
  real(kind=db), dimension(nr,nlm_pot,nspin):: Vrato

  real(kind=db), dimension(nr):: f2, r
  real(kind=db), dimension(nbseuil):: Eseuil, Vecond
  real(kind=db), dimension(nrm,nbseuil):: psii
  real(kind=db), dimension(nr,ip0:ip_max):: r_or_bess
  real(kind=db), dimension(nspinp*(Lmax+1)**2):: E_kin, E_V

  real(kind=db), dimension(:,:), allocatable:: g0, gm, gp,  gso
  real(kind=db), dimension(:,:,:,:), allocatable:: V
  real(kind=db), dimension(:,:,:,:,:), allocatable:: ui, ur

  E_kin = 0._db; E_V(:) = 0._db
  
  konde(:) = sqrt( cmplx(Ecinetic(:), Eimag,db) )

  ich = icheck - 1
  
  allocate( g0(nr,nspin) )
  allocate( gm(nr,nspin) )
  allocate( gp(nr,nspin) )
  allocate( gso(nr,nspino) )
  
  if( icheck > 1 ) then
    if( nspin == 1 ) then
      write(3,110) Energ*rydb, Ecinetic(:)*rydb, ( V0bdc(:) - E_Fermi )*rydb, real(konde(:))
    else
      write(3,120) Energ*rydb, Ecinetic(:)*rydb, ( V0bdc(:) - E_Fermi )*rydb, real(konde(:))
    endif
  endif

  if( Full_potential ) then
    nlm = ( Lmax + 1 )**2
  else
    nlm = 1
  endif
  allocate( V(nr,nlm,nlm,nspin) )

  do i = 1,nbseuil
    if( Final_tddft ) then
      if( i /= nbseuil ) cycle
      Ephoton = Energ + Eseuil(nbseuil)
    else
      Ephoton = Energ + Eseuil(i)
    endif
    Ephoton = max( 0.001_db, Ephoton ) ! Optic case
! Multiplicative term for quadrupolar transitions
! In S.I. vecond = k = E*alfa_sf*4*pi*epsilon0 / (e*e)
! In a.u. and Rydberg : k = 0.5_db * alfa_sf * E
    Vecond(i) = 0.5_db * alfa_sf * Ephoton
  end do

  call mod_V(ich,Lmax,Lmax_pot,nlm,nlm_pot,nr,nrmtg,nrmtsd,nspin,r,Rmtg_abs,Rmtsd_abs,V,V_intmax,V0bdc,Vrato,Ylm_comp)

  call coef_sch_rad(Enervide,f2,g0,gm,gp,gso,nlm,nr,nspin,nspino,numat_abs,r,Relativiste,Spinorbite,V)

  if( abs(Eimag) > eps10 .or. Ecinetic(1) < eps10 .or. Ecinetic(nspin) < eps10 ) then
    Ecomp = .true.
  else
    Ecomp = .false.
  endif
!  Radial_comp = Ecomp .or. ( Hubb_abs .and. Ylm_comp )

  if( Full_potential ) then
    lfin = 0
  else
    lfin = Lmax
  endif

  do L = 0,lfin

    if( Hubb_abs .and. L == l_hubbard( numat_abs ) )  then
      Hubb_m = .true.
    else
      Hubb_m = .false.
    endif
    Radial_comp = Ecomp .or. ( Hubb_m .and. Ylm_comp )

    if( Full_potential ) then
      nlm1 = nlm    ! = ( Lmax + 1 )**2
      nlm2 = nlm
    elseif( Hubb_m .and. .not. Hubb_diag_abs ) then
      nlm1 = 2*L + 1
      nlm2 = nlm1
    elseif( Spinorbite .or. Hubb_m ) then
      nlm1 = 2*L + 1
      nlm2 = 1
    else
      nlm1 = 1
      nlm2 = 1
    endif

    allocate( ui(nr,nlm1,nlm2,nspinp,nspino) )
    allocate( ur(nr,nlm1,nlm2,nspinp,nspino) )
    allocate( Tau(nlm1,nspinp,nlm1,nspinp) )

    call Sch_radial(Ecinetic,Ecomp,Eimag,f2,Full_potential,g0,gm,gp,gso,Hubb_abs,Hubb_diag_abs,ich,konde, &
         L,Lmax,m_hubb,nlm,nlm1,nlm2,nr,nrmtg,nspin,nspino,nspinp,numat_abs,r,Radial_comp,Relativiste,Renorm,Rmtg_abs, &
         Spinorbite,Tau,ui,ur,V,V_hubb)

!    Call Kinetic_and_total_energy(E_elecZ,E_Fermi,E_kin2,E_tot,E_V2,Energ,Full_potential,Hubb_m,icheck,L,Lmax,nlm,nlm1, &
!                        nlm2,nr,nspin,nspino,nspinp,r,Rmtsd_abs,Spinorbite,ur,V,Vc,numat_abs)

! Radial integral for the golden rule
    iseuil = 1
    call radial_matrix(Final_tddft,initlv,ip_max,ip0,iseuil,L,nlm1,nlm2,nbseuil, &
           nbseuil,nlma,nlma2,nr,NRIXS,nrm,nspino,nspinp,psii,r,r_or_bess,Radial_comp,Rmtsd_abs,rof,ui,ur,Vecond)

    deallocate( Tau, ui, ur )

  end do   ! end of loop over L

  if( icheck > 1 ) then
    mot = ' '
    if( Ecomp .and. nspino == 2 ) then
      mot(11:17) = 'up sol1'
      mot(37:43) = 'up sol2'
      mot(63:69) = 'dn sol1'
      mot(89:95) = 'dn sol2'
    elseif( nspino == 2 ) then
      mot(5:50) = 'up sol1      up sol2      dn sol1      dn sol2'
    elseif( nspinp == 2 ) then
      mot = '      up           dn'
    else
      mot = ' '
    endif

    do i = 1,nbseuil
      if( Final_tddft ) then
        if( i /= nbseuil ) cycle
        initl = initlv
      else
        initl = i
        if( nbseuil > 1 ) write(3,155) i
      endif

      do ip = ip0,ip_max
        select case(ip)
          case(0)
            Write(3,'(/A)') '    Radial monopole matrix'
          case(1)
            Write(3,'(/A)') '    Radial dipole matrix'
          case(2)
            Write(3,'(/A)') '    Radial quadrupole matrix'
          case(3)
            Write(3,'(/A)') '    Radial octupole matrix'
        end select
        if( Full_potential .or. Hubb_abs ) then
          write(3,170) '  L  m lp mp', mot
        else
          write(3,175) '  L  m', mot
        endif
        lm = 0
        do L = 0,Lmax
          do m = -L,L
            lm = lm + 1
            if( .not. ( Hubb_abs .or. Full_potential .or. Spinorbite ) .and. m /= 0 ) cycle
            lmp = 0
            do lp = 0,Lmax
              if( .not. Full_potential .and. L /= lp ) cycle
              do mp = -lp,lp
                if( .not. ( Hubb_abs .or. Full_potential .or. m == mp ) ) cycle
                lmp = lp**2 + lp + 1 + mp
                lmp = min(nlma2,lmp)
                if( Full_potential .or. Hubb_abs ) then
                  if( Ecomp) then
                    write(3,180) L, m, lp, mp, ( rof(lm,lmp,isp,:,ip,initl), isp = 1,nspinp)
                  else
                    write(3,180) L, m, lp, mp, ( real(rof(lm,lmp,isp,:,ip,initl)), isp = 1,nspinp)
                  endif
                else
                  if( Ecomp) then
                    write(3,185) L, m, ( rof(lm,lmp,isp,:,ip,initl), isp = 1,nspinp )
                  else
                    write(3,185) L, m, ( real(rof(lm,lmp,isp,:,ip,initl)), isp = 1,nspinp )
                  endif
                endif
              end do
            end do
          end do
        end do
      end do

    end do

  endif

  deallocate( g0, gm, gp, gso )
  deallocate( V )

  return
  110 format(/' Energ =',f8.3,', Ecinetic =',f8.3,', V0bdc =',f8.3,' eV, konde =',f8.5)
  120 format(/' Energ =',f8.3,', Ecinetic =',2f8.3,', V0bdc =',2f8.3,' eV, konde =',2f8.5)
  155 format(/' iseuil =',i2)
  170 format(3x,a12,a104)
  175 format(3x,a6,a104)
  180 format(3x,4i3,1p,8e13.5)
  185 format(3x,2i3,1p,8e13.5)
end

!*****************************************************************************************************************

Subroutine Direct_process(Arof,Analyzer,Betalor,coef_f,Coef_g,Core_resolved,Dip_rel,E_cut_Rixs,E_Fermi,E1E1,E1E2,E1E3, &
      E1M1,E2E2,E3E3,Eclie,Eneg,Energ_emis,Energ_in,Energ_loss_rixs,Energ_s,Eseuil,File_name_rixs,Gamma, &
      HERFD,icheck,ip0,ip_max,is_g,isymeq,L_emis,Lmax,Lmoins1,Lplus1,Lseuil,m_f,m_g,M1M1, &
      Moment_conservation,Monocrystal,Multipole_R,multi_run,n_multi_run,n_process,n_q_dim,n_rel,n_theta_rixs,natomsym, &
      nb_Emis,nbseuil,ne_loss_or_emis,nenerg_emis,nenerg_in,nenerg_loss_rixs,nenerg_oc,nenerg_s,nfinal,nfinal1,nfinalr,ninit, &
      ninit1,ninitr,nlm_fe,nlm_probe,nlms_n,npl,nr,nrm,nspin, &
      nspinp,numat_abs,Output_option,Pol_i_s_g,Pol_i_p_g,Pol_s_s_g,Pol_s_p_g,Powder,psii,Psi_Emis,r, &
      Rot_atom,Shift,Spinorbite,Temp,Theta_rixs,V0bdcF,Vec_i_g,Vec_s_g,Versus_loss,Ylm_comp)

  use declarations
  implicit none

  integer:: i_f_out, i_q, i_theta, icheck, ie, ie_in, ie_out, ip0, ip_max, L_emis, Lmax, Lseuil, multi_run, n_multi_run, &
    n_process, n_q_dim, n_rel, n_theta_rixs, natomsym, nb_Emis, nbseuil, ne_loss_or_emis, nenerg_emis, nenerg_in, &
    nenerg_loss_rixs, nenerg_oc, nenerg_s, nfinal, nfinal1, nfinalr, ninit, ninit1, ninitr, ninitr_dir, ninitr_indir, nlm_probe, &
    nlms_f, nlms_i, nlms_i_e, nlms_i_E1E1, nlms_i_E1E2, nlms_i_E1E3, nlms_i_E1M1, nlms_i_E2E2, nlms_i_E3E3, nlms_i_M1M1, &
    nlms_i_E1E1_e, nlms_i_E1E2_e, nlms_i_E1E3_e, nlms_i_E1M1_e, nlms_i_E2E2_e, nlms_i_E3E3_e, nlms_i_M1M1_e, &
    nlms_n, npl, nr, nrm, nspin, nspinp, numat_abs, Output_option

  integer, dimension(nenerg_s):: nlm_fe
  integer, dimension(ninit,2):: m_g
  integer, dimension(ninit):: is_g
  integer, dimension(nfinal,2):: m_f
  integer, dimension(natomsym):: isymeq

  character(len=Length_name), dimension(n_multi_run):: File_name_rixs
  
  complex(kind=db), dimension(nenerg_s,nlm_probe,nspinp,nlms_n,ip0:ip_max,nbseuil):: Arof

  complex(kind=db), dimension(:,:,:), allocatable:: secmm, secmm_e
  complex(kind=db), dimension(:,:,:,:), allocatable:: Arof_sh, Arof_sh_fake
  complex(kind=db), dimension(:,:,:,:), allocatable:: secdd, secdd_e, secmd, secmd_e
  complex(kind=db), dimension(:,:,:,:,:), allocatable:: secdq, secdq_e, secoo, secoo_e, secqq, secqq_e
  complex(kind=db), dimension(:,:,:,:,:,:), allocatable:: Mat_in, Mat_in_fake, secdo, secdo_e
  complex(kind=db), dimension(:,:,:,:,:,:,:), allocatable:: F_gng_L_fake, REXS
  
  logical:: Analyzer, Core_resolved, Dip_rel, E1E1, E1E2, E1E3, E1M1, E2E2, E3E3, Elastic, Eneg, HERFD, &
    k_not_possible, Lmoins1, Lplus1, M1M1, Moment_conservation, Monocrystal, Powder, Spinorbite, Versus_loss, Write_bav, Ylm_comp

  logical, dimension(10):: Multipole_R

  real(kind=db):: Conv_nelec_out, Ecinetic_i, Ecinetic_s, E_cut_Rixs, E_emis, E_Fermi, E_i, E_loss, E_s, Eclie, &
    k_elec_i, k_elec_s, Mod_Q, Temp, Theta, V0 

  real(kind=db), dimension(3):: Pol_i_s, Pol_i_p, Pol_s_s, Pol_s_p, Vec_i, Vec_s
  real(kind=db), dimension(3,3):: Rot_atom
  real(kind=db), dimension(nspin):: V0bdcF
  real(kind=db), dimension(nenerg_emis):: Energ_emis
  real(kind=db), dimension(nenerg_in):: Betalor, Energ_in
  real(kind=db), dimension(nenerg_loss_rixs):: Energ_loss_rixs
  real(kind=db), dimension(nenerg_s):: Energ_s
  real(kind=db), dimension(nbseuil):: Eseuil
  real(kind=db), dimension(ninitr):: Gamma, Shift
  real(kind=db), dimension(ninit,2):: coef_g
  real(kind=db), dimension(nfinal,2):: coef_f
  real(kind=db), dimension(nr):: r
  real(kind=db), dimension(n_theta_rixs,2):: Theta_rixs
  real(kind=db), dimension(nrm,nbseuil):: psii
  real(kind=db), dimension(nb_Emis,nbseuil):: RadMat_Emis
  real(kind=db), dimension(nrm,nb_Emis):: Psi_Emis
  real(kind=db), dimension(3,n_theta_rixs,n_q_dim):: Pol_i_s_g, Pol_i_p_g, Pol_s_s_g, Pol_s_p_g, Vec_i_g, Vec_s_g
  real(kind=db), dimension(nenerg_in,ne_loss_or_emis,npl,n_theta_rixs,n_q_dim,1):: Int_out

  real(kind=db), dimension(:,:,:), allocatable:: Int_RIXS
  
  Int_out(:,:,:,:,:,:) = 0._db

  ninitr_dir = ninitr
  ninitr_indir = 0

  allocate( F_gng_L_fake(nlm_probe,nspinp,nlm_probe,nspinp,ip0:ip_max,ip0:ip_max,ninitr_indir) )
  allocate( Arof_sh(nlm_probe,nspinp,ip0:ip_max,ninitr_dir) )  
  allocate( Mat_in(nenerg_in,nlm_probe,nspinp,nlms_n,ip0:ip_max,ninitr_dir) )
  
! Calculation of the integral over energy of the incoming matrix
  call Cal_Mat_in(Arof,Betalor,Core_resolved,E_cut_Rixs,E_Fermi,Eclie,Eneg,Energ_in,Energ_s,Eseuil,Gamma, &
                      icheck,ip0,ip_max,Mat_in,nbseuil,nenerg_in,nenerg_oc,nenerg_s,ninit1,ninitr,nlm_probe,nlms_n, &
                      nspin,nspinp,Shift,Temp,V0bdcF)

! Rad_matrix_Emis is in sphere.f90
  if( HERFD ) call Rad_matrix_Emis(nb_Emis,nbseuil,nr,nrm,Psi_Emis,Psii,RadMat_Emis,r)

  allocate( Int_RIXS(npl,n_theta_rixs,n_q_dim) )
  
  do ie_in = 1,nenerg_in  ! loop over photon incoming energy

    write(6,'(/a12,f9.3,a3)') ' Energy in =', Energ_in(ie_in) * Rydb, ' eV'
    if( Versus_loss ) then 
      write(6,'(A)') ' Energy loss Intensity_s  Intensity_p'
    else
      write(6,'(A)') ' Energy out  Intensity_s  Intensity_p'
    endif

    E_i = Eseuil(nbseuil) + Energ_in(ie_in)

    do ie = 1,nenerg_s
      if( Energ_s(ie) > Energ_in(ie_in) - eps10 ) exit 
    end do
    ie = min(ie, nenerg_s ) 
    nlms_i = nlm_fe( ie )  

    if( E1E1 ) then
      nlms_i_E1E1 = nlms_i
      nlms_i_E1E1_e = 1
    else
      nlms_i_E1E1 = 0
      nlms_i_E1E1_e = 0
    endif
    if( E1E2 ) then
      nlms_i_E1E2 = nlms_i
      nlms_i_E1E2_e = 1
    else
      nlms_i_E1E2 = 0
      nlms_i_E1E2_e = 0
    endif
    if( E1E3 ) then
      nlms_i_E1E3 = nlms_i
      nlms_i_E1E3_e = 1
    else
      nlms_i_E1E3 = 0
      nlms_i_E1E3_e = 0
    endif
    if( E1M1 ) then
      nlms_i_E1M1 = nlms_i
      nlms_i_E1M1_e = 1
    else
      nlms_i_E1M1 = 0
      nlms_i_E1M1_e = 0
    endif
    if( E2E2 ) then
      nlms_i_E2E2 = nlms_i
      nlms_i_E2E2_e = 1
    else
      nlms_i_E2E2 = 0
      nlms_i_E2E2_e = 0
    endif
    if( E3E3 ) then
      nlms_i_E3E3 = nlms_i
      nlms_i_E3E3_e = 1
    else
      nlms_i_E3E3 = 0
      nlms_i_E3E3_e = 0
    endif
    if( M1M1 ) then
      nlms_i_M1M1 = nlms_i
      nlms_i_M1M1_e = 1
    else
      nlms_i_M1M1 = 0
      nlms_i_M1M1_e = 0
    endif
    allocate( secdd(3,3,n_rel,nlms_i_E1E1 ) )
    allocate( secdq(3,3,3,2,nlms_i_E1E2) )
    allocate( secdo(3,3,3,3,2,nlms_i_E1E3) )
    allocate( secmd(3,3,2,nlms_i_E1M1) )
    allocate( secqq(3,3,3,3,nlms_i_E2E2) )
    allocate( secoo(3,9,3,9,nlms_i_E3E3) )
    allocate( secmm(3,3,nlms_i_E3E3) )

    if( icheck > 1 ) then
      write(3,'(/A)') ' ------------------------------------------------------------------------------------------ '
      write(3,'(/a27,f8.3,a3)') ' Incoming energy - E_edge =', Energ_in(ie_in) * Rydb, ' eV'
    endif
  
    do ie_out = 1,ne_loss_or_emis ! Loop over photon loss or emission energy

      if( HERFD ) then
      
        Elastic = .false.
        E_s = Energ_emis(ie_out)
        nlms_f = nfinal

      else
      
        if( Versus_loss ) then
          E_loss = Energ_loss_rixs(ie_out)
          E_emis = Energ_in(ie_in) - E_loss
        else
          E_loss = Energ_in(ie_in) - Energ_emis(ie_out)
          E_emis = Energ_emis(ie_out) 
        endif 

               
        if( abs( E_loss ) < eps10 ) then
      
          Elastic = .true.
      
! It is really the photon energy, because shift and convolution are done inside to get Arof_sh 
          E_s = Eseuil(nbseuil) + Energ_in(ie_in)

          allocate( REXS(nlm_probe,nspinp,nlm_probe,nspinp,ip0:ip_max,ip0:ip_max,ninitr) )

          call Elastic_case(Arof,Betalor(ie_in),Core_resolved,E_cut_Rixs,E_Fermi,Energ_in(ie_in),Energ_s,Eseuil,REXS,Gamma, &
           icheck,ip0,ip_max,nbseuil,nenerg_oc,nenerg_s,ninit1,ninitr,nlm_probe,nlms_n,nspin,nspinp,Shift,Spinorbite,Temp)

          nlms_f = 1
          
        else 
           
          if( E_emis > E_cut_rixs .or. E_emis > Energ_in(ie_in) ) cycle

          Elastic = .false.
        
! It is really the photon energy, because shift and convolution are done inside to get Arof_sh 
          E_s = Eseuil(nbseuil) + E_emis

          do ie = 1,nenerg_s
            if( Energ_s(ie) > E_emis - eps10 ) exit 
          end do
          ie = min(ie, nenerg_s ) 
          nlms_f = nlm_fe( ie )
          
        endif  

      endif
      
      Conv_nelec_out = sqrt( 0.5_db ) * E_s 
      
      if( icheck > 2 ) then
        write(3,'(/A)') ' ------------------------------------------------------------------------------------------ '
        if( HERFD ) then
          write(3,'(/a27,f8.3,a3)') ' Outgoing energy - E_edge =', Energ_s(ie_out) * Rydb, ' eV'
        else
          write(3,'(/a27,f8.3,a3)') ' Outgoing energy - E_edge =', Energ_s(ie_out) * Rydb, ' eV'
        endif
      endif 

      do i_f_out = 1,nlms_f
      
        if( HERFD ) then
          if( nfinalr == nfinal ) then
            if( i_f_out /= ie_out ) cycle
            Int_RIXS(:,:,:) = 0._db     
          elseif( ie_out == 1 ) then
            if( i_f_out == 1 ) Int_RIXS(:,:,:) = 0._db     
            if( i_f_out > nfinal1 ) exit
          else
            if( i_f_out <= nfinal1 ) cycle
            if( i_f_out == nfinal1+1 ) Int_RIXS(:,:,:) = 0._db     
          endif
        else
          Int_RIXS(:,:,:) = 0._db     
        endif
            
        Write_bav = icheck > 3 .or. ( icheck > 1 .and. ie_in == 1 .and. ie_out == 1 &
                                      .and. ( ( i_f_out == 3 .and. .not. HERFD ) .or. HERFD ) )
 
        if( Elastic ) then
          allocate( Mat_in_fake(nenerg_in,nlm_probe,nspinp,nlms_n,ip0:ip_max,ninitr_indir) )
          allocate( Arof_sh_fake(nlm_probe,nspinp,ip0:ip_max,ninitr_indir) )  
          allocate( secdd_e(3,3,n_rel,nlms_i_E1E1_e) )
          allocate( secdq_e(3,3,3,2,nlms_i_E1E2_e) )
          allocate( secdo_e(3,3,3,3,2,nlms_i_E1E3_e) )
          allocate( secmd_e(3,3,2,nlms_i_E1M1_e) )
          allocate( secqq_e(3,3,3,3,nlms_i_E2E2_e) )
          allocate( secoo_e(3,9,3,9,nlms_i_E3E3_e) )
          allocate( secmm_e(3,3,nlms_i_E3E3_e) )
          nlms_i_e = 1
      
! in this call, "ninitr_indir" and "ninitr_dir" are inversed 
          call Tensor_rixs(Arof_sh_fake,Coef_f,Coef_g,Conv_nelec_out,Core_resolved,REXS,HERFD,i_f_out,icheck,ie_in,ip_max,ip0, &
          is_g,L_emis,Lmax,Lmoins1,Lplus1,Lseuil,m_f,m_g,Mat_in_fake,Multipole_R,n_rel,nb_Emis,nbseuil,nenerg_in,nfinal,nfinal1, &
          ninit1,ninit,ninitr_indir,ninitr_dir,nlm_probe,nlms_i_e,nlms_i_E1E1_e,nlms_i_E1E2_e,nlms_i_E1E3_e,nlms_i_E1M1_e, &
          nlms_i_E2E2_e, &
          nlms_i_E3E3_e,nlms_i_M1M1_e,nspinp,nlms_n,numat_abs,RadMat_Emis,Rot_atom,secdd_e,secdo_e,secdq_e,secmd_e,secmm_e, &
          secoo_e,secqq_e, &
          Write_bav,Ylm_comp)
          
          deallocate( Arof_sh_fake, Mat_in_fake, REXS )

        else
        
          if( .not. HERFD ) &
            call Conv_Arof(Arof,Arof_sh,Core_resolved,E_cut_Rixs,E_emis,Energ_s,Gamma,i_f_out,ip0,ip_max,nbseuil, &
                    nenerg_s,nenerg_oc,ninit1,ninitr,nlms_n,nlm_probe,nspinp,Shift)
        
          call Tensor_rixs(Arof_sh,Coef_f,Coef_g,Conv_nelec_out,Core_resolved,F_gng_L_fake,HERFD,i_f_out,icheck,ie_in,ip_max,ip0, &
          is_g,L_emis,Lmax,Lmoins1,Lplus1,Lseuil,m_f,m_g,Mat_in,Multipole_R,n_rel,nb_Emis,nbseuil,nenerg_in,nfinal,nfinal1, &
          ninit1,ninit,ninitr_dir,ninitr_indir,nlm_probe,nlms_i,nlms_i_E1E1,nlms_i_E1E2,nlms_i_E1E3,nlms_i_E1M1,nlms_i_E2E2, &
          nlms_i_E3E3,nlms_i_M1M1,nspinp,nlms_n,numat_abs,RadMat_Emis,Rot_atom,secdd,secdo,secdq,secmd,secmm,secoo,secqq, &
          Write_bav,Ylm_comp)

        endif

        do i_q = 1,n_q_dim
          do i_theta = 1,n_theta_rixs
            if( Moment_conservation ) &
              write(6,'(a6,i3,a17,2f8.3)') ' i_q =',i_q,', Theta, 2Theta =', Theta_rixs(i_theta,:)*180/pi 
        
            Mod_Q = 0.5_db * alfa_sf * sqrt( E_s**2 + E_i**2 - 2 * E_s * E_i * cos( pi - Theta_rixs(i_theta,2) ) )
        
            if( Moment_conservation .and. .not. HERFD ) then
        
              V0 = sum( V0bdcF(:) ) / nspin
              Ecinetic_s = E_emis + E_Fermi - V0
              if( .not. Eneg ) Ecinetic_s = max( Ecinetic_s, Eclie ) 
              k_elec_s = sqrt( Ecinetic_s )
        
              Ecinetic_i = Energ_in(ie_in) + E_Fermi - V0
              if( .not. Eneg ) Ecinetic_i = max( Ecinetic_i, Eclie ) 
              k_elec_i = sqrt( Ecinetic_i )
        
              if( k_elec_s + k_elec_i < Mod_Q ) then
                if( icheck > 0 ) &
                  Write(3,120) Energ_in(ie_in)*rydb, E_emis*rydb, k_elec_s, '+', k_elec_i, '<', Mod_Q
                k_not_possible = .true.
              endif
            
              if( abs( k_elec_i - k_elec_s ) > Mod_Q ) then
                if( icheck > 0 ) &
                  Write(3,130) Energ_in(ie_in)*rydb, E_emis*rydb, k_elec_s, '-', k_elec_i, '>', Mod_Q
                k_not_possible = .true.
              endif
            
              if( k_not_possible ) then
                if( icheck > 0 ) Write(3,'(5x,A)') 'No possible channel at this energy loss'    
                cycle
              endif
              
            endif

            Pol_i_s(:) = Pol_i_s_g(:,i_theta,i_q)     
            Pol_i_p(:) = Pol_i_p_g(:,i_theta,i_q)     
            Pol_s_s(:) = Pol_s_s_g(:,i_theta,i_q)     
            Pol_s_p(:) = Pol_s_p_g(:,i_theta,i_q)     
            Pol_i_s(:) = Pol_i_s_g(:,i_theta,i_q)     
            Theta = Theta_rixs(i_theta,1)
            Vec_i(:) = Vec_i_g(:,i_theta,i_q)     
            Vec_s(:) = Vec_s_g(:,i_theta,i_q)

            if( Elastic ) then
              call Cal_RIXS_int(Dip_rel,E_i,E_s,E1E1,E1E2,E1E3,E1M1,E2E2,E3E3,i_q,i_theta,isymeq,Int_RIXS,M1M1,Monocrystal, &
                    n_q_dim,n_rel,n_theta_rixs,natomsym,nlms_i_e,nlms_i_E1E1_e,nlms_i_E1E2_e, &
                    nlms_i_E1E3_e,nlms_i_E1M1_e,nlms_i_E2E2_e,nlms_i_E3E3_e,nlms_i_M1M1_e,npl,pol_i_s,pol_i_p,pol_s_s,pol_s_p, &
                    secdd_e,secdo_e,secdq_e,secmd_e,secmm_e,secoo_e,secqq_e,Theta,Vec_i,Vec_s)
            else         
              call Cal_RIXS_int(Dip_rel,E_i,E_s,E1E1,E1E2,E1E3,E1M1,E2E2,E3E3,i_q,i_theta,isymeq,Int_RIXS,M1M1,Monocrystal, &
                    n_q_dim,n_rel,n_theta_rixs,natomsym,nlms_i,nlms_i_E1E1,nlms_i_E1E2, &
                    nlms_i_E1E3,nlms_i_E1M1,nlms_i_E2E2,nlms_i_E3E3,nlms_i_M1M1,npl,pol_i_s,pol_i_p,pol_s_s,pol_s_p, &
                    secdd,secdo,secdq,secmd,secmm,secoo,secqq,Theta,Vec_i,Vec_s)
            endif

          end do
        end do

        Int_out(ie_in,ie_out,:,:,:,1) = Int_out(ie_in,ie_out,:,:,:,1) + Int_RIXS(:,:,:) 

        if( Elastic ) deallocate( secdd_e, secdq_e, secdo_e, secmd_e, secqq_e, secoo_e, secmm_e ) 
                          
      end do  ! end of loop over sum on outgoing final states (i_f_out)    

      Int_out(ie_in,ie_out,:,:,:,:) = ( E_i / E_s ) * Int_out(ie_in,ie_out,:,:,:,:) 

      if( Versus_loss ) then 
        write(6,'(f11.3,1p,2e13.5)') E_loss * Rydb, Int_out(ie_in,ie_out,1,1,1,1) + Int_out(ie_in,ie_out,2,1,1,1), &
                                                    Int_out(ie_in,ie_out,3,1,1,1) + Int_out(ie_in,ie_out,4,1,1,1)
      else
        write(6,'(f11.3,1p,2e13.5)') E_emis * Rydb, Int_out(ie_in,ie_out,1,1,1,1) + Int_out(ie_in,ie_out,2,1,1,1), &
                                                    Int_out(ie_in,ie_out,3,1,1,1) + Int_out(ie_in,ie_out,4,1,1,1)
      endif

    end do ! end of loop over ie_out

    deallocate( secdd, secdq, secdo, secmd, secqq, secoo,secmm )

  end do

  deallocate( Arof_sh, F_gng_L_fake, Int_RIXS, Mat_in )

  if( Versus_loss ) then
    call Write_out_RIXS(Analyzer,Energ_loss_rixs,Energ_in,File_name_rixs,HERFD,1,Int_out,multi_run,n_multi_run, &
                      n_process,n_q_dim,n_theta_rixs,1,nenerg_loss_rixs,nenerg_in,npl,Output_option,Powder,Versus_loss)
  else  
    call Write_out_RIXS(Analyzer,Energ_emis,Energ_in,File_name_rixs,HERFD,1,Int_out,multi_run,n_multi_run, &
                      n_process,n_q_dim,n_theta_rixs,1,nenerg_emis,nenerg_in,npl,Output_option,Powder,Versus_loss)
  endif

  return
  120 format(5x,'E_unoc =',f10.5,', E_occ =',f10.5,' eV, k_e_s =',f10.5,' ',a1,' k_e_i =',f10.5,' ',a1,' Q =',f10.5)
  130 format(5x,'E_unoc =',f10.5,', E_occ =',f10.5,' eV, abs( k_e_s =',f10.5,' ',a1,' k_e_i =',f10.5,') ',a1,' Q =',f10.5)
end

!*****************************************************************************************************************

! Calculation of radial integrals 

subroutine Cal_Mat_in(Arof,Betalor,Core_resolved,E_cut_Rixs,E_Fermi,Eclie,Eneg,Energ_in,Energ_s,Eseuil,Gamma, &
                      icheck,ip0,ip_max,Mat_in,nbseuil,nenerg_in,nenerg_oc,nenerg_s,ninit1,ninitr,nlm_probe,nlms_n, &
                      nspin,nspinp,Shift,Temp,V0bdcF)

  use declarations
  implicit none

  integer:: icheck, ie, ie_min, ie_in, initr, ip, ip0, ip_max, iseuil, isp, je, Lm, Lms, n, n_e, nbseuil, nenerg_e, nenerg_in, &
    nenerg_oc, nenerg_s, ninit1, ninitr, nlm_probe, nlms_n, nspin, nspinp
    
  character(len=4), dimension(16):: Lm_string
  character(len=1), dimension(2):: Spin

  complex(kind=db):: f, f_interp2_cp, f0, fm, fp, CFac
  complex(kind=db), dimension(nenerg_s,nlm_probe,nspinp,nlms_n,ip0:ip_max,nbseuil):: Arof
  complex(kind=db), dimension(nenerg_in,nlm_probe,nspinp,nlms_n,ip0:ip_max,ninitr):: Mat_in
  
  logical:: Core_resolved, Eneg, Thomas_Fermi
    
  real(kind=db):: Conv_nelec, Delta, Delta_E_inf, Delta_E_sup, E, E_cut_Rixs, E_Fermi, Ecinetic, &
    Eclie, Ephoton, F_TM, Gamma_tot, k_elec, kBT_i, Lorentzian_i, Lorentzian_r, Step, Temp, V0

  real(kind=db), dimension(nenerg_in):: Betalor, Energ_in
  real(kind=db), dimension(nenerg_s):: Energ_s
  real(kind=db), dimension(nbseuil):: Eseuil
  real(kind=db), dimension(nspin):: V0bdcF
  real(kind=db), dimension(ninitr):: Gamma, Shift
  real(kind=db), dimension(:), allocatable:: Energ_e, E_inf, E_sup

  data Lm_string/ '0,+0','1,-1','1,+0','1,+1','2,-2','2,-1','2,+0','2,+1','2,+2','3,-3','3,-2','3,-1','3,+0','3,+1','3,+2','3,+3' /
  data Spin/ 'u','d' /

  Thomas_Fermi = Temp > eps10
  
  Mat_in(:,:,:,:,:,:) = (0._db, 0._db)
  V0 = sum( V0bdcF(:) ) / nspin
  
  if( Thomas_Fermi ) then
    ie_min = 1
  else
    ie_min = nenerg_oc + 1
  endif  

  if( nenerg_s > 1 ) then
    Step = max( Energ_s(nenerg_s) - Energ_s(nenerg_s-1), 2._db / Rydb ) 
    Delta = 50 * ( Betalor(nenerg_in) + max( 2 * Gamma(1), 1._db ) )   ! Gamma hole is already divided by 2
    n_e = nint( Delta / Step )
    Step = Delta / n_e
    nenerg_e = nenerg_s + n_e
  else
    n_e = 0
    nenerg_e = nenerg_s
  endif
  allocate( Energ_e(nenerg_e) )
  Energ_e(1:nenerg_s) =  Energ_s(1:nenerg_s)
  do ie = 1,n_e
    Energ_e(nenerg_s+ie) = Energ_s(nenerg_s) + ie * Step 
  end do
  
  allocate( E_inf(ie_min:nenerg_e) )
  allocate( E_sup(ie_min:nenerg_e) )
  
  if( nenerg_e - nenerg_oc == 1 ) then
    E_sup(ie_min) = Energ_e(nenerg_e) + 0.05_db / Rydb
    E_inf(ie_min) = Energ_e(nenerg_e) - 0.05_db / Rydb
  else
    if( Thomas_Fermi ) then
      E_inf(ie_min) = 1.5_db * Energ_e(ie_min) - 0.5_db * Energ_e(ie_min+1)
    else
      E_inf(ie_min) = E_cut_Rixs
    endif  
    E_sup(ie_min) = ( Energ_e(ie_min+1) + Energ_e(ie_min) ) / 2
    do ie = ie_min+1,nenerg_e-1
      E_sup(ie) = ( Energ_e(ie+1) + Energ_e(ie) ) / 2
      E_inf(ie) = ( Energ_e(ie-1) + Energ_e(ie) ) / 2
    end do
    E_inf(nenerg_e) = ( Energ_e(nenerg_e-1) + Energ_e(nenerg_e ) ) / 2
    E_sup(nenerg_e) = 1.5_db * Energ_e(nenerg_e) - 0.5_db * Energ_e(nenerg_e-1) 
  endif
  
  kBT_i = 10000 * Rydb / ( k_Boltzmann * e_electron * Temp )

  do initr = 1,ninitr       ! ----------> Loop over edges or core states

    if( Core_resolved ) then
      if( initr <= ninit1 ) then
        iseuil = 1
      else
        iseuil = min(2, nbseuil)
      endif
    else
      iseuil = min(initr, nbseuil)
    endif
             
    do ie_in = 1,nenerg_in ! loop over incoming energy
 
      Ephoton = Eseuil(iseuil) + Energ_in(ie_in)
! For very low energy edges
      Ephoton = max(0.001_db/Rydb, Ephoton)
 !     Conv_nelec = cst * Conv_mbarn_nelec(Ephoton) / pi
 ! here it is the square root
      Conv_nelec = sqrt( 0.5_db ) * Ephoton

      Gamma_tot = Gamma(initr) + Betalor(ie_in) 
      
      do ie = ie_min, nenerg_e  ! Loop over non occupied electron states
            
! The factor CFac = Delta_E / (  Energ_in(ie_in) - Energ_s(ie) - Shift(initr) + img * Gamma )
! is replaced by the corresponding integral over energy for this energy step
         
        Delta_E_inf = Energ_in(ie_in) - E_inf(ie) - Shift(initr)
        Delta_E_sup = Energ_in(ie_in) - E_sup(ie) - Shift(initr)
         
        if( Gamma_tot > eps10 ) then                 
          Lorentzian_i = atan( Delta_E_sup / Gamma_tot ) - atan( Delta_E_inf / Gamma_tot )
          Lorentzian_r = - 0.5_db * log( ( Gamma_tot**2 + Delta_E_sup**2 ) / ( Gamma_tot**2 + Delta_E_inf**2 ) )
        else
          if( Delta_E_inf * Delta_E_sup < eps10 ) then
            if( abs( Delta_E_inf + Delta_E_sup ) < eps10 ) then
              Lorentzian_r = 0._db
            elseif( abs( Delta_E_inf ) < eps10 .or. abs( Delta_E_sup ) < eps10 ) then
              Lorentzian_r = - log( 4._db )   ! limitation on divergence
            else
              Lorentzian_r = log( - Delta_E_inf / Delta_E_sup )
            endif
            Lorentzian_i = - pi
          else
            Lorentzian_r = log( Delta_E_inf / Delta_E_sup )
            Lorentzian_i = 0._db
          endif
        endif  

        CFac = cmplx( Lorentzian_r, Lorentzian_i, db ) * Conv_nelec

! Thomas-Fermi distribution (to avoid peak at E_F on f')
        if( Thomas_Fermi ) then
          F_TM = 1 / ( 1 + exp( ( E_cut_Rixs - Energ_e(ie) ) * kBT_i ) )
          F_TM = sqrt( F_TM )  ! because it is applied to the square root of the DOS  
          CFac = Cfac * F_TM
        endif
  
  ! Step is in fact dk = (1/2k)*dE
        Ecinetic = Energ_e(ie) + E_Fermi - V0
        if( .not. Eneg ) Ecinetic = max( Ecinetic, Eclie ) 
        k_elec = sqrt( Ecinetic )
        CFac = Cfac / ( 2 * k_elec )
        
        je = min( ie, nenerg_s )

        E = ( E_sup(ie) + E_inf(ie) ) / 2          
        Delta = E - Energ_e(ie)
                         
        if( abs( Delta ) < eps10 .or. ie >= nenerg_s .or. ie == 1 ) then
 
          Mat_in(ie_in,:,:,:,:,initr) = Mat_in(ie_in,:,:,:,:,initr) + CFac * Arof(je,:,:,:,:,iseuil)
 
        else
! interpolation        
          do Lm = 1,nlm_probe
            do isp = 1,nspinp
              do ip = ip0,ip_max
                do Lms = 1,nlms_n
                  fm = Arof(ie-1,Lm,isp,Lms,ip,iseuil)          
                  f0 = Arof(ie,Lm,isp,Lms,ip,iseuil)          
                  fp = Arof(ie+1,Lm,isp,Lms,ip,iseuil)          
                  f = f_interp2_cp(E,Energ_e(ie-1),Energ_e(ie),Energ_e(ie+1),fm,f0,fp)
                  Mat_in(ie_in,Lm,isp,Lms,ip,initr) = Mat_in(ie_in,Lm,isp,Lms,ip,initr) + CFac * f
                end do
              end do
            end do 
           end do
        endif

      end do
    end do
  end do
  
  if( icheck > 0 ) then
    n = min( nlms_n, 9 )
    write(3,'(/A)') ' Mat_in' 
    write(3,110) ((((( Lm_string(Lm), Spin(isp), Lms, ip, initr, Lm = 1,nlm_probe), isp = 1,nspinp), Lms = 1,n), &
                                                                                ip = ip0,ip_max), initr = 1,ninitr )
    do ie_in = 1,nenerg_in
      write(3,120) Energ_in(ie_in)*rydb, (((( Mat_in(ie_in,:,isp,Lms,ip,initr), isp = 1,nspinp), Lms = 1,n), &
                                                                            ip = ip0,ip_max), initr = 1,ninitr ) 
    end do
  endif

  deallocate( E_inf, E_sup, Energ_e )
    
  return
  110 format('    Energy   ', 10000('(',a4,',',a1,3(',',i1),')r',2x,'im',4x))
  120 format(f13.3,1p,10000(1x,2e11.3))
end

!*****************************************************************************************************************

! Evaluation of the scattering amplitude for the elastic case 

subroutine Elastic_case(Arof,Betalor,Core_resolved,E_cut_Rixs,E_Fermi,Energ_in,Energ_s,Eseuil,REXS,Gamma, &
           icheck,ip0,ip_max,nbseuil,nenerg_oc,nenerg_s,ninit1,ninitr,nlm_probe,nlms_n,nspin,nspinp,Shift,Spinorbite,Temp)

  use declarations
  implicit none

  integer:: icheck, ie, ie_min, initr, ip0, ip_max, ip_1, ip_2, iseuil, isp_1, isp_2, je, Lm_1, Lm_2, &
    n_e, nbseuil, nenerg_e, nenerg_oc, nenerg_s, ninit1, ninitr, nlm_probe, nlms_n, nspin, nspinp
    
  character(len=4), dimension(16):: Lm_string
  character(len=1), dimension(2):: Spin

  complex(kind=db):: Coef_Lorentz, f, f_interp2_cp, f0, fm, fp

  complex(kind=db), dimension(nenerg_s,nlm_probe,nspinp,nlms_n,ip0:ip_max,nbseuil):: Arof
  complex(kind=db), dimension(nlm_probe,nspinp,nlm_probe,nspinp,ip0:ip_max,ip0:ip_max,ninitr):: REXS

  complex(kind=db), dimension(:), allocatable:: Cfac
  
  logical:: Core_resolved, Spinorbite  
    
  real(kind=db):: Betalor, Conv_nelec, Delta, E, E_cut_Rixs, E_Fermi, E_m_V0, Energ_in, &
    Eph, Eph_min, Ephoton_in, Gamma_tot, Step, Temp, V0

  real(kind=db), dimension(nenerg_s):: Energ_s
  real(kind=db), dimension(nbseuil):: Eseuil
  real(kind=db), dimension(nspin):: V0bdcF
  real(kind=db), dimension(ninitr):: Gamma, Shift

  real(kind=db), dimension(:), allocatable:: Energ_e, E_inf, E_sup

  data Lm_string/ '0,+0','1,-1','1,+0','1,+1','2,-2','2,-1','2,+0','2,+1','2,+2','3,-3','3,-2','3,-1','3,+0','3,+1','3,+2','3,+3' /
  data Spin/ 'u','d' /

  REXS(:,:,:,:,:,:,:) = (0._db, 0._db)

  V0 = sum( V0bdcF(:) ) / nspin
  E_m_V0 = E_Fermi - V0

  if( Temp > eps10 ) then
    ie_min = 1
  else
    ie_min = nenerg_oc + 1
  endif

  if( nenerg_s > 1 ) then
    Step = max( Energ_s(nenerg_s) - Energ_s(nenerg_s-1), 2._db / Rydb ) 
    Delta = 50 * ( Betalor + max( 2 * Gamma(1), 1._db ) )   ! Gamma hole is already divided by 2
    n_e = nint( Delta / Step )
    Step = Delta / n_e
    nenerg_e = nenerg_s + n_e
  else
    n_e = 0
    nenerg_e = nenerg_s
  endif
  allocate( Energ_e(nenerg_e) )
  Energ_e(1:nenerg_s) =  Energ_s(1:nenerg_s)
  do ie = 1,n_e
    Energ_e(nenerg_s+ie) = Energ_s(nenerg_s) + ie * Step 
  end do

! To avoid photon energy < 0 !  
  Eph_min = 0.001_db / Rydb
  
  allocate( E_inf(ie_min:nenerg_e) )
  allocate( E_sup(ie_min:nenerg_e) )
  allocate( Cfac(ie_min:nenerg_e) )
  
  if( nenerg_e - nenerg_oc == 1 ) then
    E_sup(ie_min) = Energ_e(nenerg_e) + 0.05_db / Rydb
    E_inf(ie_min) = Energ_e(nenerg_e) - 0.05_db / Rydb
  else
    if( Temp > eps10 ) then
      E_inf(ie_min) = 1.5_db * Energ_e(ie_min) - 0.5_db * Energ_e(ie_min+1)
    else
      E_inf(ie_min) = E_cut_Rixs
    endif  
    E_sup(ie_min) = ( Energ_e(ie_min+1) + Energ_e(ie_min) ) / 2
    do ie = ie_min+1,nenerg_e-1
      E_sup(ie) = ( Energ_e(ie+1) + Energ_e(ie) ) / 2
      E_inf(ie) = ( Energ_e(ie-1) + Energ_e(ie) ) / 2
    end do
    E_inf(nenerg_e) = ( Energ_e(nenerg_e-1) + Energ_e(nenerg_e ) ) / 2
    E_sup(nenerg_e) = 1.5_db * Energ_e(nenerg_e) - 0.5_db * Energ_e(nenerg_e-1) 
  endif
  
  do initr = 1,ninitr       ! ----------> Loop over edges or core states

    if( Core_resolved ) then
      if( initr <= ninit1 ) then
        iseuil = 1
      else
        iseuil = min(2, nbseuil)
      endif
    else
      iseuil = min(initr, nbseuil)
    endif
             
    Ephoton_in = Eseuil(iseuil) + Energ_in
    
    Gamma_tot = Betalor + Gamma(initr)

    do ie = ie_min, nenerg_e  
      CFac(ie) = Coef_Lorentz(E_cut_Rixs,E_inf(ie),E_sup(ie),Energ_e(ie),Energ_in,Gamma_tot,Shift(initr),Temp)
!    Conv_nelec = sqrt( cst * Conv_mbarn_nelec(Ephoton) / pi )
      Eph = Ephoton_in + Energ_e(ie)
      Eph  = max( Eph_min, Eph )   
      Conv_nelec = 0.5_db * Eph**2
      CFac(ie) = CFac(ie) * Conv_nelec 
    end do

! In fact Betalor = 0, thus one does not mind that it is different for in and out energies 
    Gamma_tot = Gamma(initr) + Betalor
    
    do ie = ie_min, nenerg_e  ! Inward loop over non occupied electron states

      je = min( ie, nenerg_s )
    
      E = ( E_sup(ie) + E_inf(ie) ) / 2          
      Delta = E - Energ_e(ie)

      do Lm_1 = 1,nlm_probe
        do isp_1 = 1,nspinp
          do ip_1 = ip0,ip_max
          
            do Lm_2 = 1,nlm_probe
              do isp_2 = 1,nspinp
                if( Spinorbite .and. isp_1 /= isp_2 ) cycle
                do ip_2 = ip0,ip_max
               
                  if( abs( Delta ) < eps10 .or. ie >= nenerg_s .or. ie == 1 ) then
                    f = sum( conjg( Arof(je,Lm_1,isp_1,:,ip_1,iseuil) ) * Arof(je,Lm_2,isp_2,:,ip_2,iseuil) )
                  else
                    fm = sum( conjg( Arof(je-1,Lm_1,isp_1,:,ip_1,iseuil) ) * Arof(je-1,Lm_2,isp_2,:,ip_2,iseuil) )
                    f0 = sum( conjg( Arof(je,Lm_1,isp_1,:,ip_1,iseuil) ) * Arof(je,Lm_2,isp_2,:,ip_2,iseuil) )
                    fp = sum( conjg( Arof(je+1,Lm_1,isp_1,:,ip_1,iseuil) ) * Arof(je+1,Lm_2,isp_2,:,ip_2,iseuil) )
                    f = f_interp2_cp(E,Energ_e(ie-1),Energ_e(ie),Energ_e(ie+1),fm,f0,fp)
                  endif
               
                  REXS(Lm_1,isp_1,Lm_2,isp_2,ip_1,ip_2,initr) = REXS(Lm_1,isp_1,Lm_2,isp_2,ip_1,ip_2,initr) + CFac(ie) * f            

                end do   ! end of loop on ip_2
              end do    ! end of loop on isp_2
            end do      ! end of loop on Lm_2
                
          end do  ! end of loop on ip_1
        end do    ! end of loop on isp_1
      end do      ! end of loop on Lm_1
        
    end do ! end of loop on ie 

  end do ! end of loop on initr
  
  if( icheck > 0 ) then
    write(3,'(/A)') ' REXS(L_1,m_1,isp_1,L_2,m_2,isp_2,ip_1,ip_2,initr)' 
    write(3,110) Energ_in*rydb 
        
    write(3,120) ((((( Spin(isp_1), Spin(isp_2), ip_1, ip_2, initr, isp_1 = 1,nspinp), isp_2 = 1,nspinp), ip_1 = ip0,ip_max), &
                                                                                ip_2 = ip0,ip_max), initr = 1,ninitr )
    do Lm_1 = 1,nlm_probe
      do Lm_2 = 1,nlm_probe
        write(3,130) Lm_string(Lm_1), Lm_string(Lm_2), ((((( REXS(Lm_1,isp_1,lm_2,isp_2,ip_1,ip_2,initr), &
                                       isp_1 = 1,nspinp), isp_2 = 1,nspinp), ip_1 = ip0,ip_max), &
                                                                                ip_2 = ip0,ip_max), initr = 1,ninitr )
      end do
    end do
  endif

  deallocate( Cfac, E_inf, E_sup, Energ_e )
    
  return
  110 format('  Energ_in =',f13.3)
  120 format(' L1 m1 L2 m2   ', 10000( 1x,2a1,3(1x,i1),' r',7x,'im',3x))
  130 format(2(2x,a4),1p,10000(1x,2e11.3))
end

!***********************************************************************

! calculation of the occupied states at a shifted loss or emission energy because of the different core-energies

subroutine Conv_Arof(Arof,Arof_sh,Core_resolved,E_cut_Rixs,E_emis,Energ_s,Gamma,i_f_out,ip0,ip_max,nbseuil, &
                      nenerg_s,nenerg_oc,ninit1,ninitr,nlms_n,nlm_probe,nspinp,Shift)

  use declarations
  implicit none

  integer:: i_f_out, ie, initr, ip0, ip_max, iseuil, nbseuil, nenerg_s, nenerg_oc, ninit1, ninitr, nlms_n, &
            nlm_probe, nspinp  
  
  complex(kind=db), dimension(nenerg_s,nlm_probe,nspinp,nlms_n,ip0:ip_max,nbseuil):: Arof
  complex(kind=db), dimension(nlm_probe,nspinp,ip0:ip_max,ninitr):: Arof_sh
  
  logical:: Core_resolved

  real(kind=db):: Delta_E_inf, Delta_E_sup, E_cut_Rixs, E_emis, Int_Lorentzian, Lorentzian_i, p
  real(kind=db), dimension(nenerg_oc):: E_inf, E_sup
  real(kind=db), dimension(nenerg_s):: Energ_s
  real(kind=db), dimension(ninitr):: Gamma, Shift

  if( nenerg_oc == 1 ) then
    do initr = 1,ninitr       ! ----------> Loop over edges or core states

      if( Core_resolved ) then
        if( initr <= ninit1 ) then
          iseuil = 1
        else
          iseuil = min(2, nbseuil)
        endif
      else
        iseuil = min(initr, nbseuil)
      endif

      Arof_sh(:,:,:,initr) = Arof(1,:,:,i_f_out,:,iseuil)
    end do

    return
  endif
    
  Arof_sh(:,:,:,:) = ( 0._db, 0._db )
  
  E_inf(1) = 1.5_db * Energ_s(1) - 0.5_db * Energ_s(2)   
  do ie = 2,nenerg_oc
    E_inf(ie) = ( Energ_s(ie-1) + Energ_s(ie+1) ) / 2
    E_sup(ie-1) = E_inf(ie)
  end do
  E_sup(nenerg_oc) = E_cut_Rixs 

  do initr = 1,ninitr       ! ----------> Loop over edges or core states

    if( Core_resolved ) then
      if( initr <= ninit1 ) then
        iseuil = 1
      else
        iseuil = min(2, nbseuil)
      endif
    else
      iseuil = min(initr, nbseuil)
    endif

    Int_Lorentzian = 0._db
                 
    if( Gamma(initr) > eps10 ) then
                     
      do ie = 1,nenerg_oc  ! Loop over occupied electron states
          
! The factor CFac = Delta_E / (  Energ_out - Energ_s(ie) - Shift(initr) + img * Gamma )
! is replaced by the corresponding integral over energy for this energy step
         
        Delta_E_inf = E_emis - E_inf(ie) - Shift(initr)
        Delta_E_sup = E_emis - E_sup(ie) - Shift(initr)
       
        Lorentzian_i = ( atan( Delta_E_sup / Gamma(initr) ) - atan( Delta_E_inf / Gamma(initr) ) ) / pi
        Int_Lorentzian = Int_Lorentzian + Lorentzian_i  
 
 ! The modulus, because only the amplitude can be important                
        Arof_sh(:,:,:,initr) = Arof_sh(:,:,:,initr) + Lorentzian_i * abs( Arof(ie,:,:,i_f_out,:,iseuil) )

      end do
  
      if( Gamma(initr) > eps10 ) Arof_sh(:,:,:,initr) = Arof_sh(:,:,:,initr) / Int_Lorentzian                 
  
    else
    
      do ie = 2,nenerg_oc  ! Loop over occupied electron states
        if( Energ_s(ie) > E_emis - eps10 ) exit  
      end do
      if( ie > nenerg_oc ) then
        Arof_sh(:,:,:,initr) = ( 0._db, 0._db )
      else
        p = ( Energ_s(ie) - E_emis ) / ( Energ_s(ie) - Energ_s(ie-1) )
        Arof_sh(:,:,:,initr) = p * Arof(ie-1,:,:,i_f_out,:,iseuil) + ( 1 - p ) * Arof(ie,:,:,i_f_out,:,iseuil)   
      endif
    
    endif  
  
  end do
  
  return
end

!***********************************************************************

! Calculation of the cartesian tensors

! < g_1 | o*( L_s m_s, k_s, j_s, L_s, irang ) | f_1 > < f_2 | o*( L_i, m_i, k_i, j_i, L_i, jrang  ) | g_2 >  

subroutine Tensor_rixs(Arof_sh,Coef_f,Coef_g,Conv_nelec_out,Core_resolved,F_gng_L,HERFD,i_f_out,icheck,ie_in,ip_max,ip0, &
          is_g,L_emis,Lmax,Lmoins1,Lplus1,Lseuil,m_f,m_g,Mat_in,Multipole_R,n_rel,nb_Emis,nbseuil,nenerg_in,nfinal,nfinal1, &
          ninit1,ninit,ninitr_dir,ninitr_indir,nlm_probe,nlms_i,nlms_i_E1E1,nlms_i_E1E2,nlms_i_E1E3,nlms_i_E1M1,nlms_i_E2E2, &
          nlms_i_E3E3,nlms_i_M1M1,nspinp,nlms_n,numat_abs,RadMat_Emis,Rot_atom,secdd,secdo,secdq,secmd,secmm,secoo,secqq, &
          Write_bav,Ylm_comp)

  use declarations
  implicit none

  integer:: h_s, h_i, i, i_f_out, icheck, ie_in, index_cross, ip_max, ip0, ipr, irang, irang1, isp, isp1, isp2, isp3, isp4, &
    ispfg, j, j_i, j_s, jh_i, jh_s, jrang, k, k_i, k_s, L, L_emis, L_f_max, L_i, L_s, Lm_i, lm_s, Lmax, Lmomax, Lms_f, Lomax, &
    Lseuil, m, m_i, m_s, n_rel, nb_Emis, nbseuil, nenerg_in, nfinal, nfinal1, ninit1, ninit, ninitr_dir, ninitr_indir, &
    nh_i, nh_s, nj_i, nj_s, &
    nlm_probe, nlms_i, nlms_i_E1E1, nlms_i_E1E2, nlms_i_E1E3, nlms_i_E1M1, nlms_i_E2E2, nlms_i_E3E3, nlms_i_M1M1, nlms_n, nrang, &
    nspinp, numat_abs

  parameter( Lomax = 3, Lmomax = ( Lomax + 1 )**2 )

  character(len=1), dimension(2):: Spin
 
  complex(kind=db), dimension(3,3) :: Mat2
  complex(kind=db), dimension(3,3,3) :: Mat3
  complex(kind=db), dimension(3,3,3,3) :: Mat4
  complex(kind=db), dimension(3,3,3,3,3,3) :: Mat6
  complex(kind=db), dimension(3,3,nlms_i_M1M1):: secmm
  complex(kind=db), dimension(3,3,2,nlms_i_E1M1):: secmd
  complex(kind=db), dimension(3,3,3,2,nlms_i_E1E2):: secdq
  complex(kind=db), dimension(3,3,3,3,nlms_i_E2E2):: secqq
  complex(kind=db), dimension(3,3,3,3,2,nlms_i_E1E3):: secdo
  complex(kind=db), dimension(3,3,n_rel,nlms_i_E1E1):: secdd
  complex(kind=db), dimension(3,9,3,9,nlms_i_E3E3):: secoo
  complex(kind=db), dimension(nlms_i):: Ten, Tens
  complex(kind=db), dimension(nlm_probe,nspinp,ip0:ip_max,ninitr_dir):: Arof_sh
  complex(kind=db), dimension(nenerg_in,nlm_probe,nspinp,nlms_n,ip0:ip_max,ninitr_dir):: Mat_in
  complex(kind=db), dimension(nlm_probe,nspinp,nlm_probe,nspinp,ip0:ip_max,ip0:ip_max,ninitr_indir):: F_gng_L

  complex(kind=db), dimension(:,:,:,:), allocatable:: Tens_lm

  integer, dimension(nfinal,2):: m_f
  integer, dimension(ninit,2):: m_g
  integer, dimension(ninit):: is_g

  logical:: Core_resolved, Dip_rel, E1E1, E1E2, E1E3, E1M1, E2E2, E3E3, HERFD, Lmoins1, Lplus1, M1M1, Write_bav, Ylm_comp

  logical, dimension(10):: Multipole_R

  real(kind=db):: c, c0, c1, c12, c120, c3, c5, c8, clm_s, clm_i, Conv_nelec_out
  real(kind=db), dimension(0:Lomax,3,3,3,Lmomax):: clm
  real(kind=db), dimension(3,3):: Rot_atom
  real(kind=db), dimension(nfinal,2):: coef_f
  real(kind=db), dimension(ninit,2):: coef_g
  real(kind=db), dimension(nb_Emis,nbseuil):: RadMat_Emis

  data Spin/ 'u','d' /

  allocate( Tens_lm(Lmomax,Lmomax,n_rel,nlms_i) )
  
  if( icheck > 2 .or. Write_bav ) write(3,'(/5x,A)') '--- Tensor_rixs -----------'

  E1E1 = Multipole_R(1); E1E2 = Multipole_R(2); E1E3 = Multipole_R(3);
  E1M1 = Multipole_R(4); E2E2 = Multipole_R(6);
  E3E3 = Multipole_R(7); M1M1 = Multipole_R(8)

  Dip_rel = n_rel > 1

  if( E1E3 .or. E3E3 ) then
    nrang = 3
  elseif( E1E2 .or. E2E2 ) then
    nrang = 2
  else
    nrang = 1
  endif
  if( E1M1 .or. M1M1 ) then
    irang1 = 0
  else
    irang1 = 1
  endif

  if( nrang > Lomax ) then
    call write_error
    do ipr = 6,9,3
      write(ipr,'(/A)') ' nrang > Lomax in Tensor_rixs !'
    end do
    stop
  endif

! Calcul des composantes de la transf base-cartesienne - base-spherique:

  do irang = irang1,nrang

    clm(irang,:,:,:,:) = 0._db

    select case(irang)

      case(0)
! Magnetic dipole
        c = sqrt( 4 * pi )
! Dans ce cas, correspond a l=0,m=0 mais sert a definir x, y ou z.
        clm(irang,1,:,:,4) = c
        clm(irang,2,:,:,2) = c
        clm(irang,3,:,:,3) = c

      case(1)
! Dipole
        c = sqrt( 4 * pi / 3 )
        clm(irang,1,:,:,4) = c
        clm(irang,2,:,:,2) = c
        clm(irang,3,:,:,3) = c

      case(2)
! Quadrupole
        c0 = sqrt( 4 * pi ) / 3
        c = sqrt( 4 * pi / 15 )
        c3 = c / sqrt( 3._db )

        clm(irang,1,1,:,1) = c0
        clm(irang,1,1,:,7) = - c3;   clm(irang,1,1,:,9) = c
        clm(irang,1,2,:,5) = c
        clm(irang,1,3,:,db) = c
        clm(irang,2,2,:,1) = c0
        clm(irang,2,2,:,7) = - c3;   clm(irang,2,2,:,9) = - c
        clm(irang,2,3,:,6) = c
        clm(irang,3,3,:,1) = c0
        clm(irang,3,3,:,7) = 2 * c3

        do i = 1,3
          do j = i+1,3
            clm(irang,j,i,:,:) = clm(irang,i,j,:,:)
          end do
        end do

      case(3)
! Octupole
        c1 = sqrt( 4 * pi / 75 )

        c = sqrt( 4 * pi / 35 )
        c5 = c / sqrt( 5._db )
        c8 = c / sqrt( 2._db )
        c12 = c / sqrt( 3._db )
        c120 = c / sqrt( 30._db )

        clm(irang,1,1,1,4) = 3 * c1
        clm(irang,1,1,1,14) = - 3 * c120; clm(irang,1,1,1,16) = c8
        clm(irang,2,2,2,2) = 3 * c1
        clm(irang,2,2,2,12) = - 3 * c120; clm(irang,2,2,2,10) = - c8
        clm(irang,3,3,3,3) = 3 * c1
        clm(irang,3,3,3,13) = 2 * c5
        clm(irang,1,1,2,2) = c1
        clm(irang,1,1,2,10) = c8;        clm(irang,1,1,2,12) = - c120
        clm(irang,1,2,2,4) = c1
        clm(irang,1,2,2,16) = - c8;      clm(irang,1,2,2,14) = - c120
        clm(irang,1,1,3,3) = c1
        clm(irang,1,1,3,13) = - c5;      clm(irang,1,1,3,15) = c12
        clm(irang,2,2,3,3) = c1
        clm(irang,2,2,3,13) = - c5;      clm(irang,2,2,3,15) = - c12
        clm(irang,2,3,3,2) = c1
        clm(irang,2,3,3,12) = 4 * c120
        clm(irang,1,3,3,4) = c1
        clm(irang,1,3,3,14) = 4 * c120
        clm(irang,1,2,3,11) = c12

        do i = 1,3
          do j = i,3
            do k = j,3
              if( i == j .and. i == k ) cycle
              clm(irang,i,k,j,:) = clm(irang,i,j,k,:)
              clm(irang,j,i,k,:) = clm(irang,i,j,k,:)
              clm(irang,k,i,j,:) = clm(irang,i,j,k,:)
              clm(irang,j,k,i,:) = clm(irang,i,j,k,:)
              clm(irang,k,j,i,:) = clm(irang,i,j,k,:)
            end do
          end do
        end do

    end select

  end do

  secmm(:,:,:) = (0._db,0._db)
  secmd(:,:,:,:) = (0._db,0._db)
  secdq(:,:,:,:,:) = (0._db,0._db)
  secqq(:,:,:,:,:) = (0._db,0._db)
  secdo(:,:,:,:,:,:) = (0._db,0._db)
  secdd(:,:,:,:) = (0._db,0._db)
  if( E3E3 ) secoo(:,:,:,:,:) = (0._db,0._db)

  if( icheck > 2 ) write(3,120) Lseuil

! Loops over tensor rank
  do irang = irang1,nrang
    do jrang = irang1,nrang

      Tens_lm(:,:,:,:) = (0._db,0._db)
      if( .not. E1E1 .and. irang == 1 .and. jrang == 1 ) cycle
      if( .not. E1E2 .and. ( ( irang == 1 .and. jrang == 2 ) .or. ( irang == 2 .and. jrang == 1 ) ) ) cycle
      if( .not. E2E2 .and. irang == 2 .and. jrang == 2 ) cycle
      if( .not. E1E3 .and. ( ( irang == 1 .and. jrang == 3 ) .or. ( irang == 3 .and. jrang == 1 ) ) ) cycle
      if( .not. M1M1 .and. irang == 0 .and. jrang == 0 ) cycle
      if( .not. E1M1 .and. ( ( irang == 0 .and. jrang == 1 ) .or. ( irang == 1 .and. jrang == 0 ) ) ) cycle
      if( .not. E3E3 .and. irang == 3 .and. jrang == 3 ) cycle
      if( ( irang == 0 .and. jrang > 1 ) .or. ( irang > 1 .and. jrang == 0 ) ) cycle
      if( ( jrang == 3 .and. irang == 2 ) .or. ( jrang == 2 .and. irang == 3 ) ) cycle

      if( icheck > 3 ) write(3,130) irang, jrang

      if( irang < jrang ) then
        index_cross = 1
      else
        index_cross = 2
      endif
      
! Loops over tensor indices
      do k_s = 1,3

        if( irang > 1 ) then
          nj_s = 3
        else
          nj_s = 1
        endif

        do j_s = 1,nj_s

          if( irang == 3 ) then
            nh_s = 3
          else
            nh_s = 1
          endif

          do h_s = 1,nh_s

            jh_s = 3 * ( j_s - 1 ) + h_s

            do k_i = 1,3

              if( jrang > 1 ) then
                nj_i = 3
              else
                nj_i = 1
              endif

              do j_i = 1,nj_i

                if( jrang == 3 ) then
                  nh_i = 3
                else
                  nh_i = 1
                endif

                do h_i = 1,nh_i

                  jh_i = 3 * ( j_i - 1 ) + h_i

! To take into account the relativistic transition channel (just for E1E1).
                  ispfg = 0
                  boucle_dip_pos: do isp1 = 1,2
                  do isp2 = 1,2
                  do isp3 = 1,2
                  do isp4 = 1,2
                    if( Lseuil == 0 .and. isp1 /= isp4 ) cycle  ! at K edge, core states are mono-spin. 
                    ispfg = ispfg + 1
                    if( ispfg > 1 .and. ( ( irang /= 1 .or. jrang /= 1 ) .or. .not. dip_rel ) ) exit boucle_dip_pos 

                    Tens(:) = (0._db,0._db)

                    if( icheck > 3 ) write(3,140) k_s, j_s, h_s, k_i, j_i, h_i, ispfg

! Loops over spherical components of the tensors
                  lm_s = 0
                  do L_s = 0,max(irang,1)
                    do m_s = -L_s,L_s
                      lm_s = lm_s + 1

                      clm_s = clm(irang,k_s,j_s,h_s,lm_s)
                      if( abs(clm_s) < eps10 ) cycle

                      Lm_i = 0
                      do L_i = 0,max(jrang,1)
                        do m_i = -L_i,L_i
                          Lm_i = Lm_i + 1

                          clm_i = clm(jrang,k_i,j_i,h_i,Lm_i)
                          if( abs(clm_i) < eps10 ) cycle

! Calcul de la composante du tenseur
                          if( sum( abs( Tens_lm(lm_s,Lm_i,ispfg,:) ) ) < 1.e-15_db ) then

                            if( icheck > 3 ) write(3,150) L_s, m_s, L_i, m_i, clm_s, clm_i
                            
                            call Tens_ab_rixs(Arof_sh,coef_f,coef_g,Core_resolved,Dip_rel,F_gng_L,HERFD,i_f_out,icheck, &
                              ie_in, &
                              ip_max,ip0,irang,is_g,isp1,isp2,isp3,isp4,jrang,L_s,m_s,L_i,m_f,m_g,m_i,L_emis,Lmax,Lmoins1,Lplus1, &
                              Lseuil,Mat_in,nb_emis,nbseuil,nenerg_in,nfinal,nfinal1,ninit,ninit1,ninitr_dir,ninitr_indir, &
                              nlms_i,nlms_n,nlm_probe,nspinp,RadMat_Emis,Ten,Ylm_comp)

                           ! For hydrogen there is only 1 core state
                            if( numat_abs == 1) Ten(:) = Ten(:) / 2
                            
                            if( HERFD .or. ninitr_dir > 0 ) Ten(:) = Ten(:) * Conv_nelec_out

                            Tens_lm(lm_s,Lm_i,ispfg,:) = Ten(:)

                          endif

                          Tens(:) = Tens(:) + clm_s * clm_i * Tens_lm(lm_s,Lm_i,ispfg,:)
                        end do
                      end do
                    end do
                  end do ! fin boucle L_s

! Remplissage de la valeur calculee dans tous les elements du tenseur equivalent par symetrie.

! M1-M1 (dipole magnetique - dipole magnetique)
                  if( irang == 0 .and. jrang == 0 ) secmm(k_s,k_i,:) = Tens(:)

! M1-E1 (dipole magnetique - dipole electrique)
                  if( ( irang == 0 .and. jrang == 1 ) .or. ( irang == 1 .and. jrang == 0 ) ) &
                    secmd(k_s,k_i,index_cross,:) = Tens(:)

! E1-E1 (Dipole-dipole)
                  if( irang == 1 .and. jrang == 1 ) secdd(k_s,k_i,ispfg,:) = Tens(:)

! Dipole-quadrupole
                  if( ( irang == 1 .and. jrang == 2 ) .or. ( irang == 2 .and. jrang == 1 ) ) then
                    secdq(k_s,k_i,j_i,index_cross,:) = Tens(:)
       !             if( index_cross == 1 ) then 
       !               secdq(k_s,j_i,k_i,index_cross,:) = Tens(:)
       !             else
       !               secdq(j_i,k_i,k_s,index_cross,:) = Tens(:)
       !             endif
                  endif

! Dipole-octupole
                  if( ( irang == 1 .and. jrang == 3 ) .or. ( irang == 3 .and. jrang == 1 ) ) then
                    secdo(k_s,k_i,j_i,h_i,index_cross,:) = Tens(:)
       !                 secdo(k_s,k_i,h_i,j_i,index_cross,:) = Tens(:)
       !                 if( index_cross == 1 ) then
       !                   secdo(k_s,j_i,k_i,h_i,index_cross,:) = Tens(:)
       !                   secdo(k_s,h_i,k_i,j_i,index_cross,:) = Tens(:)
       !                   secdo(k_s,j_i,h_i,k_i,index_cross,:) = Tens(:)
       !                   secdo(k_s,h_i,j_i,k_i,index_cross,:) = Tens(:)
       !                 else
       !                   secdo(j_i,k_i,k_s,h_i,index_cross,:) = Tens(:)
       !                   secdo(h_i,k_i,k_s,j_i,index_cross,:) = Tens(:)
       !                   secdo(j_i,k_i,h_i,k_s,index_cross,:) = Tens(:)
       !                   secdo(h_i,k_i,j_i,k_s,index_cross,:) = Tens(:)
       !                 endif
                  endif

! Quadrupole-quadrupole
                  if( irang == 2 .and. jrang == 2 ) then
                    secqq(k_s,j_s,k_i,j_i,:) = Tens(:)
       !             secqq(j_s,k_s,k_i,j_i,:) = Tens(:)
       !             secqq(k_s,j_s,j_i,k_i,:) = Tens(:)
       !             secqq(j_s,k_s,j_i,k_i,:) = Tens(:)
                  endif
                  
! Octupole-octupole
                  if( irang == 3 .and. jrang == 3 ) secoo(k_s,jh_s,k_i,jh_i,:) = Tens(:)

                  end do
                  end do
                  end do 
                  end do boucle_dip_pos

                end do
              end do
            end do
          end do
        end do
      end do

      if( icheck < 3 ) cycle

      if( .not. E1E2 .and. irang == 1 .and. jrang == 2 ) cycle
      if( jrang == 3 .and. irang == 2 ) cycle
      if( irang == 0 .and. jrang > 1 ) cycle
      if( .not. HERFD ) then
        if( nspinp == 2 ) then
          L_f_max = nint( sqrt( real( nlms_i / 2  ) ) ) - 1 
          write(3,160) ((( L, m, Spin(isp), m = -L,L), L = 0,L_f_max), isp = 1,nspinp )
        else
          L_f_max = nint( sqrt( real( nlms_i ) ) ) - 1 
          write(3,165) (( L, m, m = -L,L), L = 0,L_f_max )
        endif
      endif 
      lm_s = 0
      do L_s = 0,max(irang,1)
        do m_s = -L_s,L_s
         lm_s = lm_s + 1
         Lm_i = 0
         do L_i = 0,max(jrang,1)
            do m_i = -L_i,L_i
              Lm_i = Lm_i + 1
              do ispfg = 1,n_rel
                if( ( irang /= 1 .or. jrang /= 1 ) .and. ispfg > 1 ) exit
                if( sum( abs(Tens_lm(lm_s,Lm_i,ispfg,:)) ) > eps15 ) write(3,170) irang, jrang, L_s, m_s, L_i, m_i, &
                    ispfg, Tens_lm(lm_s,Lm_i,ispfg,:)
              end do
            end do
          end do
        end do
      end do

    end do
  end do

! Rotation to get the tensors in R1 basis

  do Lms_f = 1,nlms_i
    
    if( E1E1 ) then
      do ispfg = 1,n_rel
        mat2(:,:) = secdd(:,:,ispfg,Lms_f)
        call rot_tensor_2( mat2, Rot_atom )
        secdd(:,:,ispfg,Lms_f) = mat2(:,:)
      end do
    endif
  
    if( E1E2 ) then
      do index_cross = 1,2
        mat3(:,:,:) = secdq(:,:,:,index_cross,Lms_f)
        call rot_tensor_3( mat3, Rot_atom )
        secdq(:,:,:,index_cross,Lms_f) = mat3(:,:,:)
      end do
    endif
  
    if( E2E2 ) then
      mat4(:,:,:,:) = secqq(:,:,:,:,Lms_f)
      call rot_tensor_4( mat4, Rot_atom )
      secqq(:,:,:,:,Lms_f) = mat4(:,:,:,:)
    endif
  
    if( E1E3 ) then
      do index_cross = 1,2
        mat4(:,:,:,:) = secdo(:,:,:,:,index_cross,Lms_f)
        call rot_tensor_4( mat4, Rot_atom )
        secdo(:,:,:,:,index_cross,Lms_f) = mat4(:,:,:,:)
      end do
    endif
  
    if( E1M1 ) then
      do index_cross = 1,2
        mat2(:,:) = secmd(:,:,index_cross,Lms_f)
        call rot_tensor_2( mat2, Rot_atom )
        secmd(:,:,index_cross,Lms_f) = mat2(:,:)
      end do
    endif
  
    if( M1M1 ) then
      mat2(:,:) = secmm(:,:,Lms_f)
      call rot_tensor_2( mat2, Rot_atom )
      secmm(:,:,Lms_f) = mat2(:,:)
    endif
  
    if( E3E3 ) then
      jh_s = 0
      do j_s = 1,3
        do h_s = 1,3
          jh_s = jh_s + 1
          jh_i = 0
          do j_i = 1,3
            do h_i = 1,3
              jh_i = jh_i + 1
              Mat6(:,j_s,h_s,:,j_i,h_i) = secoo(:,jh_s,:,jh_i,Lms_f)
            end do
          end do
        end do
      end do
      call rot_tensor_6( Mat6, Rot_atom )
      jh_s = 0
      do j_s = 1,3
        do h_s = 1,3
          jh_s = jh_s + 1
          jh_i = 0
          do j_i = 1,3
            do h_i = 1,3
              jh_i = jh_i + 1
              secoo(:,jh_s,:,jh_i,Lms_f) = Mat6(:,j_s,h_s,:,j_i,h_i)
            end do
          end do
        end do
      end do
    endif

  end do

  if( Write_bav ) Call Write_tensor_rixs(E1E1,E1E2,E2E2,HERFD,n_rel,nlms_i,nlms_i_E1E1,nlms_i_E1E2,nlms_i_E2E2,nspinp, &
                                         secdd,secdq,secqq)

  deallocate( Tens_lm )
                                                         
  return
  110 format(/'  --- Tensor_rixs -----------')
  120 format(/' Lseuil =',i2)
  130 format(/' -- irang =',i2,', jrang =',i2,' --')
  140 format(/' k_s, j_s, h_s =',3i3,',  k_i, j_i, h_i =',3i3,',  ispfg =',i2)
  150 format(/' L_s, m_s =',2i3,',  L_i, m_i =',2i3,', clm_s, clm_i =',1p, 2e13.5)
  160 format(/' Tensor by harmonics (basis R4) versus (L_f,m_f,spin) '/, &
              ' ir jr  L_s m_s  L_i m_i ispfg', 10000(7x,'(',i2,',',i3,',',a1,')',10x))
  165 format(/' Tensor by harmonics (basis R4) versus (L_f,m_f)'/, &
              ' ir jr  L_s m_s  L_i m_i ispfg', 10000(8x,'(',i2,',',i3,')',11x))
  170 format(2i3,i4,i3,2x,i4,i3,i5,1p,10000(1x,2e13.5))

end

!***********************************************************************

subroutine Tens_ab_rixs(Arof_sh,coef_f,coef_g,Core_resolved,Dip_rel,F_gng_L,HERFD,i_f_out,icheck,ie_in, &
                              ip_max,ip0,irang,is_g,isp1,isp2,isp3,isp4,jrang,L_s,m_s,L_i,m_f,m_g,m_i,L_emis,Lmax,Lmoins1,Lplus1, &
                              Lseuil,Mat_in,nb_emis,nbseuil,nenerg_in,nfinal,nfinal1,ninit,ninit1,ninitr_dir,ninitr_indir, &
                              nlms_i,nlms_n,nlm_probe,nspinp,RadMat_Emis,Ten,Ylm_comp)

  use declarations
  implicit none

  integer:: i_em, i_em_1, i_em_n, i_emis, i_f_out, i_f1, i_g, i_seuil, icheck, ie_in, initr, ip_max, ip0, irang, isp1, isp2, &
    isp3, isp4, ispf1, ispf2, ispinf1, ispinf2, isping1, isping2, jrang, L_emis, L_i, L_f1, L_f2, L_s, Li, Lm01, Lm02, Lm_f1, &
    Lm_f2, Lmax, Lseuil, m_i, m_f1, m_f2, m_s, mi1, mi2, nb_emis, nbseuil, nenerg_in, nfinal, nfinal1, ninit, ninit1, &
    ninitr_dir, ninitr_indir, nlm_probe, nlms_i, nlms_n, nspinp

  integer, dimension(ninit,2), intent(in):: m_g
  integer, dimension(ninit), intent(in):: is_g
  integer, dimension(nfinal,2):: m_f

  complex(kind=db):: Cg

  complex(kind=db):: Gaunt_i, Gaunt_s, Gaunt_xrc, Gauntmag 
  complex(kind=db), dimension(nlms_i):: Ten
  complex(kind=db), dimension(nlm_probe,nspinp,ip0:ip_max,ninitr_dir):: Arof_sh
  complex(kind=db), dimension(nenerg_in,nlm_probe,nspinp,nlms_n,ip0:ip_max,ninitr_dir):: Mat_in
  complex(kind=db), dimension(nlm_probe,nspinp,nlm_probe,nspinp,ip0:ip_max,ip0:ip_max,ninitr_indir):: F_gng_L

  logical:: Core_resolved, Dip_rel, HERFD, Lmoins1, Lplus1, Titre, Ylm_comp

  real(kind=db):: Ci_1, Ci_2, Ci2, J_init, Jz
  real(kind=db), dimension(nfinal,2):: coef_f
  real(kind=db), dimension(ninit,2):: Coef_g
  real(kind=db), dimension(nb_Emis,nbseuil):: RadMat_Emis
  
  Li = Lseuil

  Ten(:) = (0._db,0._db)

! Loop over core states < g_1 ...  > < ... >
  do i_g = 1,ninit

    J_init = Li + 0.5_db * is_g(i_g)
    if( i_g <= ninit1 ) then
      Jz = - J_init + i_g - 1
    else
      Jz = - J_init + i_g - ninit1 - 1
    endif

    if( Core_resolved ) then
      initr = i_g
    elseif( i_g <= ninit1 ) then
      initr = 1
    else
      initr = 2
    endif
    
    if( i_g <= ninit1 ) then
      i_seuil = 1
    else
      i_seuil = 2
    endif

! Loop over core state spin < g_1 ...  > < ... >
    do isping1 = 1,2 
    
      if( irang == 1 .and. jrang == 1 .and. Dip_rel .and. isp1 /= isping1 ) cycle

      mi1 = m_g(i_g,isping1)
      Ci_1 = Coef_g(i_g,isping1)

      if( abs( Ci_1 ) < eps6 ) cycle

! Loop over core state spin < ...  > < ... g_2 >
      do isping2 = 1,2  
        
        if( Li == 0 .and. isping1 /= isping2 ) cycle
        if( irang == 1 .and. jrang == 1 .and. Dip_rel .and. isp4 /= isping2 ) cycle

        mi2 = m_g(i_g,isping2)
        Ci_2 = Coef_g(i_g,isping2)

        if( abs( Ci_2 ) < eps6 ) cycle

        Ci2 = Ci_1 * Ci_2

        if( icheck > 3 ) then
          write(3,'(/A)') ' init   Jz ispg1 mi1   Ci1  ispg2 mi2  Ci2'
          write(3,'(i5,f5.1,2(2i5,f7.3))') i_g, Jz, isping1, mi1, Ci_1, isping2, mi2, Ci_2
        endif
        Titre = .true.

! Loop over spherical harmonics of final states < ... f_1 > < ... >
        if( HERFD ) then
          i_em_1 = 0
          i_em_n = 0
        else
          i_em_1 = 0
          i_em_n = Lmax
        endif
        
        do i_em = i_em_1,i_em_n

          if( HERFD ) then
            L_f1 = L_emis
            if( i_f_out <= nfinal1 ) then
              i_emis = 1
            else
              i_emis = 2
            endif
          else
            L_f1 = i_em
            if( Lplus1 .and. L_f1 < Li + 1 ) cycle
            if( Lmoins1 .and. L_f1 > Li ) cycle
            if( L_f1 > Li + irang .or. L_f1 < Li - irang .or. mod(L_f1,2) /= mod(Li+irang,2) ) cycle
            Lm01 = L_f1**2 + L_f1 + 1
          endif
          
          do i_f1 = -i_em_n,i_em_n
            if( .not. HERFD ) then
              m_f1 = i_f1
              Lm_f1 = Lm01 + m_f1
              if( Lm_f1 > nlm_probe ) cycle
            endif

! Loop over spin of final states < ... f_1 > < ... >
            do ispinf1 = 1,2

              if( HERFD ) then
                if( ispinf1 /= isping1 ) cycle
                m_f1 = m_f(i_f_out,ispinf1)
              else
                if( irang == 1 .and. jrang == 1 .and. Dip_rel .and. isp2 /= ispinf1 ) cycle
! Il n'y a que pour L_s dipole magnetique qu'on peut avoir du spin-flip (ou pour L_s terme dipolaire relativiste)
                if( ispinf1 /= isping1 .and. ( irang > 1 .or. ( irang == 1 .and. &
                                                                           ( .not. Dip_rel .or. jrang /= 1 ) ) ) ) cycle
              endif

              ispf1 = min( ispinf1, nspinp )

              if( irang == 0 ) then
                Gaunt_s = Gauntmag(L_f1,m_f1,ispinf1,m_s,Li,mi1,isping1,Ylm_comp,.true.)
              elseif( HERFD ) then
                Gaunt_s = Gaunt_xrc(L_f1,m_f1,L_s,m_s,Li,mi1,.true.)
                Gaunt_s = Gaunt_s * Coef_f(i_f_out,ispinf1)
              else
                Gaunt_s = Gaunt_xrc(L_f1,m_f1,L_s,m_s,Li,mi1,Ylm_comp)
              endif
              if( abs(Gaunt_s) < eps10 ) cycle

! Loop over spin of final states < ... > < f_2 ... >
              do L_f2 = 0,Lmax
                if( Lplus1 .and. L_f2 < Li + 1 ) cycle
                if( Lmoins1 .and. L_f2 > Li ) cycle
                if( L_f2 > Li + jrang .or. L_f2 < Li - jrang .or. mod(L_f2,2) /= mod(Li+jrang,2) ) cycle

                Lm02 = L_f2**2 + L_f2 + 1
                do m_f2 = -L_f2,L_f2
                  Lm_f2 = Lm02 + m_f2
                  if( Lm_f2 > nlm_probe ) cycle

                  do ispinf2 = 1,2  ! spin etat final en sortie
                    ispf2 = min( ispinf2, nspinp )

                    if( irang == 1 .and. jrang == 1 .and. Dip_rel .and. isp3 /= ispinf2 ) cycle

                    if( ispinf2 /= isping2 .and. ( jrang > 1 .or. ( jrang == 1 &
                                                                           .and. ( .not. Dip_rel .or. irang /= 1 ) ) ) ) cycle 

                    if( jrang == 0 ) then
                      Gaunt_i = Gauntmag(L_f2,m_f2,ispinf2,m_i,Li,mi2,isping2,Ylm_comp,.true.)
                    else
                      Gaunt_i = Gaunt_xrc(L_f2,m_f2,L_i,m_i,Li,mi2,Ylm_comp)
                    endif
                    if( abs(Gaunt_i) < eps10 ) cycle

                    Cg = Ci2 * conjg( Gaunt_s ) * Gaunt_i

                    if( HERFD ) then
                      Ten(1:nlms_i) = Ten(1:nlms_i) &
                                    + Cg * RadMat_Emis(i_emis,i_seuil) * Mat_in(ie_in,Lm_f2,ispf2,1:nlms_i,jrang,initr)    
                    elseif( ninitr_dir > 0 ) then
                      Ten(1:nlms_i) = Ten(1:nlms_i) + Cg * conjg( Arof_sh(Lm_f1,ispf1,irang,initr) ) &
                                                         * Mat_in(ie_in,Lm_f2,ispf2,1:nlms_i,jrang,initr)
                    else
                      Ten(1) = Ten(1) + Cg * F_gng_L(Lm_f1,ispf1,Lm_f2,ispf2,irang,jrang,initr)
                    endif

                    if( icheck > 3 ) then
                      if( Titre ) then
                        Titre = .false.
                        write(3,130)
                      endif
                      write(3,140) L_f1, m_f1, ispinf1, L_f2, m_f2, ispinf2, Gaunt_i, Gaunt_s, Ten(:)
                    endif

                  end do ! end of loop over ispinf2, spin of final state ( incoming )
                end do !  end of loop over  m_f2 final state
              end do !  end of loop over  L_f2 final state

            end do  !  end of loop over spin of final state ( outgoing )
          end do  !  end of loop over  m_f1 final state
        end do !  end of loop over  L_f1 final state

      end do    !  end of loop over spin of core states ( incoming )
    end do    !  end of loop over spin of core states ( outgoing )
  end do   !  end of loop over core states

! One multiplies by "i" in order the real part of the tensor is the absorption. (useless here)
  Ten(:) = img * Ten(:)

  return
  130 format('  L1  m1 is1  L2  m2 is2',15x,'Gaunt_i',24x,'Gaunt_s',25x,'Ten')
  140 format(6i4,1p,1000(1x,2e15.7))
end

!***************************************************************

! Writing of the tensor

subroutine Write_tensor_rixs(E1E1,E1E2,E2E2,HERFD,n_rel,nlms_i,nlms_i_E1E1,nlms_i_E1E2,nlms_i_E2E2,nspinp, &
                             secdd,secdq,secqq)

  use declarations
  implicit none

  character(len=1), dimension(2):: Spin

  integer:: i, ind_cross, isp, j, k, L, L_f_max, Lms_f, m, n_rel, nlms_i, nlms_i_E1E1, nlms_i_E1E2, nlms_i_E2E2, nspinp

  complex(kind=db), dimension(3,3,3,2,nlms_i_E1E2):: secdq
  complex(kind=db), dimension(3,3,3,3,nlms_i_E2E2):: secqq
  complex(kind=db), dimension(3,3,n_rel,nlms_i_E1E1):: secdd
  
  logical:: E1E1, E1E2, E2E2, HERFD

  data Spin/ 'u','d' /

  if( nspinp == 2 ) then
    L_f_max = nint( sqrt( real( nlms_i / 2  ) ) ) - 1 
  else
    L_f_max = nint( sqrt( real( nlms_i ) ) ) - 1 
  endif 
  
  if( E1E1 ) then
    write(3,'(/A)')  '   Tensor E1E1     (Lms_f)' 
    if( nspinp == 2 ) then
      write(3,110) ((( L, m, Spin(isp), m = -L,L), L = 0,L_f_max), isp = 1,nspinp )
    else
      write(3,120) (( L, m, m = -L,L), L = 0,L_f_max )
    endif
    do i = 1,3
      write(3,'(1p,1000(2x,3(1x,2e13.5)))') ( secdd(i,:,1,Lms_f), Lms_f = 1,nlms_i )
    end do
  endif

  if( E1E2 ) then
    if( HERFD ) then
      write(3,'(/A)')  '   Tensor E1E2    1, ..., nfinal' 
    else
      write(3,'(/A)') ' Tensor E1E2 (i,:,j,k) (Lm_f, isp, initr) '
    endif
    do ind_cross = 1,2
      write(3,'(a16,i2)') '     ind_cross =', ind_cross
      do k = 1,3
        write(3,125) ( ( j, k, Lms_f, j = 1,3 ), Lms_f = 1,nlms_i ) 
        do i = 1,3
          write(3,'(1p,1000(2x,3(1x,2e13.5)))') (secdq(i,:,k,ind_cross,Lms_f), Lms_f = 1,nlms_i)
        end do     
      end do
      if( HERFD ) exit
    end do
  endif

  if( E2E2 ) then
    if( HERFD ) then
      write(3,'(/A)')  '   Tensor E2E2    1, ..., nfinal' 
    else
      write(3,'(/A)') ' Tensor E2E2 (i,:,k,L) (Lm_f, isp, initr) '
    endif
    do L = 1,3
      do k = 1,3
        write(3,130) ( ( j, k, L, Lms_f, j = 1,3 ), Lms_f = 1,nlms_i ) 
        do i = 1,3
          write(3,'(1p,1000(2x,3(1x,2e13.5)))') (secqq(i,:,k,L,Lms_f), Lms_f = 1,nlms_i)
        end do     
      end do
    end do
  endif

  return
  110 format(10000(7x,'(',i2,',',i3,',',a1,')',66x))
  120 format(10000(8x,'(',i2,',',i3,')',67x))
  125 format(3x,10000(3(11x,'(i,',i1,',',i1,')', i3, 6x),1x ) )
  130 format(3x,10000(3(10x,'(i,',i1,',',i1,',',i1,')', i3, 5x),1x ) )
end

!*****************************************************************************************************************

subroutine Cal_RIXS_int(Dip_rel,E_i,E_s,E1E1,E1E2,E1E3,E1M1,E2E2,E3E3,i_q,i_theta,isymeq,Int_RIXS,M1M1,Monocrystal, &
                        n_q_dim,n_rel,n_theta_rixs,natomsym,nlms_i,nlms_i_E1E1,nlms_i_E1E2, &
                        nlms_i_E1E3,nlms_i_E1M1,nlms_i_E2E2,nlms_i_E3E3,nlms_i_M1M1,npl,pol_i_s,pol_i_p,pol_s_s,pol_s_p, &
                        secdd,secdo,secdq,secmd,secmm,secoo,secqq,Theta,Vec_i,Vec_s)

  use declarations
  implicit none

  real(kind=db), parameter:: quatre_mc2 = 4 * 2 / alfa_sf**2  ! in Rydberg
  
  integer:: i_q, i_theta, ia, ipl, isym, Lms_f, n_q_dim, n_rel, &
    n_theta_rixs, natomsym, nlms_i, nlms_i_E1E1, nlms_i_E1E2, nlms_i_E1E3, nlms_i_E1M1, nlms_i_E2E2, &
    nlms_i_E3E3, nlms_i_M1M1, npl

  integer, dimension(natomsym):: isymeq
  
  logical:: Dip_rel, E1E1, E1E2, E1E3, E1M1, E2E2, E3E3, M1M1, Monocrystal

  complex(kind=db), dimension(3):: Pol_i, Pol_s
  complex(kind=db), dimension(npl):: Amplitude
  complex(kind=db), dimension(3,npl):: Polar_i, Polar_s

  complex(kind=db), dimension(3,3,2):: Tens_RIXS_E1M1
  complex(kind=db), dimension(3,3):: Tens_RIXS_M1M1
  complex(kind=db), dimension(3,3,3,2):: Tens_RIXS_E1E2
  complex(kind=db), dimension(3,3,3,3,2):: Tens_RIXS_E1E3
  complex(kind=db), dimension(3,3,3,3):: Tens_RIXS_E2E2
  complex(kind=db), dimension(3,3,n_rel):: Tens_RIXS_E1E1
  complex(kind=db), dimension(3,9,3,9):: Tens_RIXS_E3E3
  complex(kind=db), dimension(3,3,2,nlms_i_E1M1):: secmd
  complex(kind=db), dimension(3,3,3,2,nlms_i_E1E2):: secdq
  complex(kind=db), dimension(3,3,3,3,nlms_i_E2E2):: secqq
  complex(kind=db), dimension(3,3,3,3,2,nlms_i_E1E3):: secdo
  complex(kind=db), dimension(3,3,n_rel,nlms_i_E1E1):: secdd
  complex(kind=db), dimension(3,9,3,9,nlms_i_E3E3):: secoo
  complex(kind=db), dimension(3,3,nlms_i_M1M1):: secmm

  real(kind=db):: E_i, E_s, Omega_i, Omega_s, Theta
  real(kind=db), dimension(3):: Pol_i_s, Pol_i_p, Pol_s_s, Pol_s_p, Vec_i, Vec_i_t, Vec_s, Vec_s_t
  real(kind=db), dimension(3,3):: Matopsym
  real(kind=db), dimension(npl,n_theta_rixs,n_q_dim):: Int_RIXS

  Omega_i = E_i / quatre_mc2 
  Omega_s = E_s / quatre_mc2
  
  Vec_i_t(:) = Vec_i(:) 
  Vec_s_t(:) = Vec_s(:) 

  do ia = 1,natomsym

    Vec_i(:) = Vec_i_t(:)     
    Vec_s(:) = Vec_s_t(:)

    isym = abs( isymeq(ia) )
    
    if( isym /= 1 ) then
      call opsym(isym,matopsym)
      Matopsym = Transpose( Matopsym )
      Vec_i = Matmul( Matopsym, Vec_i )
      Vec_s = matmul( Matopsym, Vec_s )
    endif

    do ipl = 1,npl
 
      select case(ipl)
        case(1)
          pol_i(:) = cmplx( Pol_i_s(:), 0._db, db )
          pol_s(:) = cmplx( Pol_s_s(:), 0._db, db )
        case(2)
          pol_i(:) = cmplx( Pol_i_s(:), 0._db, db )
          pol_s(:) = cmplx( pol_s_p(:), 0._db, db )
        case(3)
          pol_i(:) = cmplx( Pol_i_p(:), 0._db, db )
          pol_s(:) = cmplx( Pol_s_s(:), 0._db, db )
        case(4)
          pol_i(:) = cmplx( Pol_i_p(:), 0._db, db )
          pol_s(:) = cmplx( pol_s_p(:), 0._db, db )
        case(5)
          pol_i(:) = ( Pol_i_s(:) + img * Pol_i_p(:) ) / sqrt( 2._db ) 
          pol_s(:) = cmplx( Pol_s_s(:), 0._db, db )
        case(6)
          pol_i(:) = ( Pol_i_s(:) - img * pol_i_p(:) ) / sqrt( 2._db ) 
          pol_s(:) = cmplx( Pol_s_s(:), 0._db, db )
        case(7)
          pol_i(:) = ( Pol_i_s(:) + img * pol_i_p(:) ) / sqrt( 2._db ) 
          pol_s(:) = cmplx( Pol_s_p(:), 0._db, db )
        case(8)
          pol_i(:) = ( Pol_i_s(:) - img * pol_i_p(:) ) / sqrt( 2._db ) 
          pol_s(:) = cmplx( Pol_s_p(:), 0._db, db )
      end select

      if( isym /= 1 ) then
        Pol_i = Matmul( Matopsym, Pol_i )
        Pol_s = Matmul( Matopsym, Pol_s )
      endif

      if( ipl >= 5 .and. isymeq(ia) < 0 ) Pol_i(:) = conjg( Pol_i(:) )

      Polar_i(:,ipl) = Pol_i(:)
      Polar_s(:,ipl) = Pol_s(:)
                  
    end do

    do Lms_f = 1,nlms_i
      
      if( E1E1 ) Tens_RIXS_E1E1(:,:,:) = secdd(:,:,:,Lms_f)

      if( E1E2 ) Tens_RIXS_E1E2(:,:,:,:) = secdq(:,:,:,:,Lms_f)
 
      if( E2E2 ) Tens_RIXS_E2E2(:,:,:,:) = secqq(:,:,:,:,Lms_f) 
 
      if( E1E3 ) Tens_RIXS_E1E3(:,:,:,:,:) = secdo(:,:,:,:,:,Lms_f) 

      if( E3E3 ) Tens_RIXS_E3E3(:,:,:,:) = secoo(:,:,:,:,Lms_f) 
 
      if( E1M1 ) Tens_RIXS_E1M1(:,:,:) = - secmd(:,:,:,Lms_f)   ! note the change of sign
 
      if( M1M1 ) Tens_RIXS_M1M1(:,:) = secmm(:,:,Lms_f)

      call Cal_RIXS_ampl(Amplitude,Dip_rel,E1E1,E1E2,E1E3,E1M1,E2E2,E3E3,M1M1, &
                       Monocrystal,n_rel,npl,Omega_i,Omega_s, &
                       Polar_i,Polar_s,Tens_RIXS_E1E1,Tens_RIXS_E1E2,Tens_RIXS_E1E3,Tens_RIXS_E2E2,Tens_RIXS_E3E3,&
                       Tens_RIXS_E1M1,Tens_RIXS_M1M1,Theta,Vec_i,Vec_s)

      Int_RIXS(:,i_theta,i_q) = Int_RIXS(:,i_theta,i_q) + abs( Amplitude(:) )**2

    end do
          
  end do ! end of loop over ia

  return
end

!*****************************************************************************************************************

subroutine Cal_RIXS_ampl(Amplitude,Dip_rel,E1E1,E1E2,E1E3,E1M1,E2E2,E3E3,M1M1, &
                       Monocrystal,n_rel,npl,Omega_i,Omega_s, &
                       Polar_i,Polar_s,Tens_RIXS_E1E1,Tens_RIXS_E1E2,Tens_RIXS_E1E3,Tens_RIXS_E2E2,Tens_RIXS_E3E3,&
                       Tens_RIXS_E1M1,Tens_RIXS_M1M1,Theta,Vec_i,Vec_s)

  use declarations
  implicit none

  integer:: he, hs, ipl, isp1, isp2, isp3, isp4, ispfg, j1, je, jhe, jhs, js, ke, ks, n_rel, npl  
   
  complex(kind=db):: matpole, matpols
  complex(kind=db), dimension(npl):: Amplitude
  complex(kind=db), dimension(3,npl):: Polar_i, Polar_s
  complex(kind=db), dimension(3):: pol_i, pol_s, u_i, u_s
  complex(kind=db), dimension(3,3,2):: Tens_RIXS_E1M1
  complex(kind=db), dimension(3,3):: Tens_RIXS_M1M1
  complex(kind=db), dimension(3,3,3,2):: Tens_RIXS_E1E2
  complex(kind=db), dimension(3,3,3,3,2):: Tens_RIXS_E1E3
  complex(kind=db), dimension(3,3,3,3):: Tens_RIXS_E2E2
  complex(kind=db), dimension(3,3,n_rel):: Tens_RIXS_E1E1
  complex(kind=db), dimension(3,9,3,9):: Tens_RIXS_E3E3

  logical:: Dip_rel, E1E1, E1E2, E1E3, E1M1, E2E2, E3E3, M1M1, Monocrystal
  
  real(kind=db):: Fac, Omega_i, Omega_s, sin2_2t, Theta
  real(kind=db), dimension(3):: Vec_i, Vec_s

  Amplitude(:) = (0._db, 0._db )
    
  if( .not. Monocrystal ) then
 
    if( E1E1 ) then
      sin2_2t = sin( 2 * Theta )**2 
      Fac = sqrt( ( 1._db / 5._db ) * ( 1 - ( 2._db / 3._db ) * sin2_2t ) + 1._db / 15._db )  
      do ipl = 1,3
        Amplitude(ipl) = Amplitude(ipl) + Fac * Tens_RIXS_E1E1(ipl,ipl,1)
      end do
      Fac = sqrt( ( 1._db / 30._db ) * ( 2 + 3 * sin2_2t ) )  
      Amplitude(4) = Amplitude(4) + Fac * ( Tens_RIXS_E1E1(1,2,1) + Tens_RIXS_E1E1(2,1,1) )
      Amplitude(5) = Amplitude(5) + Fac * ( Tens_RIXS_E1E1(1,3,1) + Tens_RIXS_E1E1(3,1,1) )
      Amplitude(6) = Amplitude(6) + Fac * ( Tens_RIXS_E1E1(2,3,1) + Tens_RIXS_E1E1(3,2,1) )
! I do the hypothesis that there is no spin crossing contribution. I am not sure.
      if( n_rel /= 1 ) then
        Fac = sqrt( ( 1._db / 5._db ) * ( 1 - ( 2._db / 3._db ) * sin2_2t ) + 1._db / 15._db )  
        do ipl = 1,3
          Amplitude(ipl) = Amplitude(ipl) + Fac * Tens_RIXS_E1E1(ipl,ipl,n_rel)
        end do
        Fac = sqrt( ( 1._db / 30._db ) * ( 2 + 3 * sin2_2t ) )  
        Amplitude(4) = Amplitude(4) + Fac * ( Tens_RIXS_E1E1(1,2,n_rel) + Tens_RIXS_E1E1(2,1,n_rel) )
        Amplitude(5) = Amplitude(5) + Fac * ( Tens_RIXS_E1E1(1,3,n_rel) + Tens_RIXS_E1E1(3,1,n_rel) )
        Amplitude(6) = Amplitude(6) + Fac * ( Tens_RIXS_E1E1(2,3,n_rel) + Tens_RIXS_E1E1(3,2,n_rel) )
      endif
    endif

! not checked, I just take the spherical contribution
    if( E2E2 ) then
      Fac = 1 / 45._db
      Fac = Fac / ( 2._db * sqrt( 5._db) ) 
      Amplitude(:) = Amplitude(:) &
                 + Fac * ( 6 * ( Tens_RIXS_E2E2(1,3,1,3) + Tens_RIXS_E2E2(2,3,2,3) + Tens_RIXS_E2E2(1,2,1,2) ) &
                         + 2 * ( Tens_RIXS_E2E2(1,1,1,1) + Tens_RIXS_E2E2(2,2,2,2) + Tens_RIXS_E2E2(3,3,3,3) ) &
                             - ( Tens_RIXS_E2E2(1,1,2,2) + Tens_RIXS_E2E2(1,1,3,3) + Tens_RIXS_E2E2(3,3,2,2) &
                               + Tens_RIXS_E2E2(2,2,1,1) + Tens_RIXS_E2E2(3,3,1,1) + Tens_RIXS_E2E2(2,2,3,3) ) )
    endif

    if( M1M1 ) then
      sin2_2t = sin( 2 * Theta )**2 
      Fac = sqrt( ( 1._db / 5._db ) * ( 1 - ( 2._db / 3._db ) * sin2_2t ) + 1._db / 15._db )  
      do ipl = 1,3
        Amplitude(ipl) = Amplitude(ipl) + Fac * Tens_RIXS_M1M1(ipl,ipl)
      end do
      Fac = sqrt( ( 1._db / 30._db ) * ( 2 + 3 * sin2_2t ) )  
      Amplitude(4) = Amplitude(4) + Fac * ( Tens_RIXS_M1M1(1,2) + Tens_RIXS_M1M1(2,1) )
      Amplitude(5) = Amplitude(5) + Fac * ( Tens_RIXS_M1M1(1,3) + Tens_RIXS_M1M1(3,1) )
      Amplitude(6) = Amplitude(6) + Fac * ( Tens_RIXS_M1M1(2,3) + Tens_RIXS_M1M1(3,2) )
    endif

    if( E3E3 ) then  ! not checked : taken like E2E2...
      Fac = 1 / 45._db
      Fac = Fac / ( 2._db * sqrt( 5._db) ) 
      Amplitude(:) = Amplitude(:) &
                 + Fac * ( 6 * ( Tens_RIXS_E3E3(1,3,1,3) + Tens_RIXS_E3E3(2,3,2,3) + Tens_RIXS_E3E3(1,2,1,2) ) &
                         + 2 * ( Tens_RIXS_E3E3(1,1,1,1) + Tens_RIXS_E3E3(2,2,2,2) + Tens_RIXS_E3E3(3,3,3,3) ) &
                             - ( Tens_RIXS_E3E3(1,1,2,2) + Tens_RIXS_E3E3(1,1,3,3) + Tens_RIXS_E3E3(3,3,2,2) &
                               + Tens_RIXS_E3E3(2,2,1,1) + Tens_RIXS_E3E3(3,3,1,1) + Tens_RIXS_E3E3(2,2,3,3) ) )
    endif

  else    ! Q_resolved

    do ipl = 1,npl
    
      Pol_i(:) = Polar_i(:,ipl)
      Pol_s(:) = Polar_s(:,ipl)
          
      if( M1M1 .or. E1M1 ) then
        u_i(1) = Vec_i(2) * Pol_i(3) - Vec_i(3) * Pol_i(2)
        u_i(2) = Vec_i(3) * Pol_i(1) - Vec_i(1) * Pol_i(3)
        u_i(3) = Vec_i(1) * Pol_i(2) - Vec_i(2) * Pol_i(1)
        u_s(1) = Vec_s(2) * Pol_s(3) - Vec_s(3) * Pol_s(2)
        u_s(2) = Vec_s(3) * Pol_s(1) - Vec_s(1) * Pol_s(3)
        u_s(3) = Vec_s(1) * Pol_s(2) - Vec_s(2) * Pol_s(1)
      endif
    
      if( E1E1 ) then
    
        if( .not. Dip_rel ) then
    
          do ke = 1,3
            Amplitude(ipl) = Amplitude(ipl) + pol_i(ke) * sum( conjg(pol_s(:)) *  Tens_RIXS_E1E1(:,ke,1) )
          end do
    
        else
    
          ispfg = 0
          do isp1 = 1,2
            do isp2 = 1,2
              do isp3 = 1,2
                do isp4 = 1,2
                  if( n_rel == 8 .and. isp1 /= isp4 ) cycle 
                  ispfg = ispfg + 1
      
                  do ks = 1,3
                    if( isp1 == isp2 ) then
                      matpols = Pol_s(ks)
                    else
                      matpols = (0._db, 0._db)
                    endif
                    if( isp1 == 1 .and. isp2 == 1 ) then
                      if( ks == 1 ) then
                        matpols = matpols - img * Omega_s * Pol_s(2)
                      elseif( ks == 2 ) then
                        matpols = matpols + img * Omega_s * Pol_s(1)
                      endif
                    elseif( isp1 == 1 .and. isp2 == 2 ) then
                      if( ks == 1 ) then
                        matpols = matpols + Omega_s * Pol_s(3)
                      elseif( ks == 2 ) then
                        matpols = matpols - img * Omega_s * Pol_s(3)
                      else
                        matpols = matpols + img * Omega_s * ( Pol_s(2) + img * Pol_s(1) )
                      endif
                    elseif( isp1 == 2 .and. isp2 == 1 ) then
                      if( ks == 1 ) then
                        matpols = matpols - Omega_s * Pol_s(3)
                      elseif( ks == 2 ) then
                        matpols = matpols - img * Omega_s * Pol_s(3)
                      else
                        matpols = matpols + img * Omega_s * ( Pol_s(2) - img * Pol_s(1) )
                      endif
                    elseif( isp1 == 2 .and. isp2 == 2 ) then
                      if( ks == 1 ) then
                        matpols = matpols + img * Omega_s * Pol_s(2)
                      elseif( ks == 2 ) then
                        matpols = matpols - img * Omega_s * Pol_s(1)
                      endif
                    endif                      
      
                    do ke = 1,3
                      if( isp3 == isp4 ) then
                        matpole = Pol_i(ke)
                      else
                        matpole = (0._db, 0._db)
                      endif
                      if( isp3 == 1 .and. isp4 == 1 ) then
                        if( ke == 1 ) then
                          matpole = matpole - img * Omega_i * Pol_i(2)
                        elseif( ke == 2 ) then
                          matpole = matpole + img * Omega_i * Pol_i(1)
                        endif
                      elseif( isp3 == 1 .and. isp4 == 2 ) then
                        if( ke == 1 ) then
                          matpole = matpole + Omega_i * Pol_i(3)
                        elseif( ke == 2 ) then
                          matpole = matpole - img * Omega_i * Pol_i(3)
                        else
                          matpole = matpole + img * Omega_i * ( Pol_i(2) + img * Pol_i(1) )
                        endif
                      elseif( isp3 == 2 .and. isp4 == 1 ) then
                        if( ke == 1 ) then
                          matpole = matpole - Omega_i * Pol_i(3)
                        elseif( ke == 2 ) then
                          matpole = matpole - img * Omega_i * Pol_i(3)
                        else
                          matpole = matpole + img * Omega_i * ( Pol_i(2) - img * Pol_i(1) )
                        endif
                      elseif( isp3 == 2 .and. isp4 == 2 ) then
                        if( ke == 1 ) then
                          matpole = matpole + img * Omega_i * Pol_i(2)
                        elseif( ke == 2 ) then
                          matpole = matpole - img * Omega_i * Pol_i(1)
                        endif
                      endif
      
                      Amplitude(ipl) = Amplitude(ipl) + conjg( matpols ) * matpole * Tens_RIXS_E1E1(ks,ke,ispfg)
                    
                    end do
                  end do
      
                end do
              end do
            end do
          end do
    
        endif
    
      endif
    
      if( E1E2 ) then
        do ke = 1,3
          do ks = 1,3   ! img was included in the Tens_RIXS_E1E2 calculation 
            Amplitude(ipl) = Amplitude(ipl) + conjg( Pol_s(ks) ) * Pol_i(ke) &
                      * sum( Vec_i(:) * Tens_RIXS_E1E2(ks,ke,:,1) + Vec_s(:) * Tens_RIXS_E1E2(ke,ks,:,2) )
          end do
        end do
      endif
    
      if( E2E2 ) then
        do ke = 1,3
          do je = 1,3
            do ks = 1,3
              Amplitude(ipl) = Amplitude(ipl) &
                             + conjg( Pol_s(ks) ) * Pol_i(ke) * Vec_i(je) * sum( Vec_s(:) * Tens_RIXS_E2E2(ks,:,ke,je) )
            end do
          end do
        end do
      endif
    
      if( E1E3 ) then
        do ke = 1,3
          do ks = 1,3
            do j1 = 1,3
              Amplitude(ipl) = Amplitude(ipl) + conjg( Pol_s(ks) ) * Pol_i(ke) &
                             * ( Vec_i(j1) * sum( Vec_i(:) * Tens_RIXS_E1E3(ks,ke,j1,:,1) ) &
                               + Vec_s(j1) * sum( Vec_s(:) * Tens_RIXS_E1E3(ks,ke,j1,:,2) ) )
            end do
          end do
        end do
      endif
      
      if( E3E3 ) then
        do ke = 1,3
          do je = 1,3
            do he = 1,3
              jhe = 3 * ( je - 1 ) + he
              do ks = 1,3
                do js = 1,3
                  do hs = 1,3
                    jhs = 3 * ( js - 1 ) + hs
                    Amplitude(ipl) = Amplitude(ipl) &
                                   + conjg( Pol_s(ks) ) * Vec_s(js) * Vec_s(hs) * Pol_i(ke) * Vec_i(je) * Vec_i(he) &
                                   * Tens_RIXS_E3E3(ks,jhs,ke,jhe)
                  end do
                end do
              end do
            end do
          end do
        end do
      endif
    
      if( M1M1 ) then
        do ke = 1,3
          Amplitude(ipl) = Amplitude(ipl) + u_i(ke) * sum( conjg(u_s(:)) * Tens_RIXS_M1M1(:,ke) )
        end do
      endif
    
      if( E1M1 ) then
        do ke = 1,3
          Amplitude(ipl) = Amplitude(ipl) + conjg( u_s(ke) ) * sum( Pol_i(:) * Tens_RIXS_E1M1(ke,:,1) ) &
                                          + conjg( Pol_s(ke) ) * sum( u_i(:) * Tens_RIXS_E1M1(ke,:,2) )  
        end do
      endif
              
    end do  ! end of loop over polarization ipl

  endif

  return
end

!*****************************************************************************************************************

! Indirect process 

Subroutine Indirect_process(alfpot,Analyzer,Betalor,Coef_g,Core_resolved,Dip_rel,dV0bdcF,E_cut_Rixs, &
      E_Fermi,E1E1,E1E2,E1E3,E1M1,E2E2,E3E3,Eclie,Eneg,Energ_emis,Energ_in,Energ_loss_rixs,Energ_s,Eseuil,File_name_rixs, &
      Full_potential,Gamma,Hubb_abs,Hubb_diag_abs,icheck,ip0,ip_max,is_g,isymeq,Lmax,Lmax_abs_t,Lmax_pot,Lmoins1,Lplus1,Lseuil, &
      m_g,m_hubb,M1M1,Moment_conservation,Monocrystal,Multipole_R,multi_run,n_multi_run,n_process,n_q_dim,n_rel,n_theta_rixs, &
      n_trans_out,natomsym,nbseuil,ne_loss_or_emis,nenerg_emis,nenerg_in,nenerg_loss_rixs,nenerg_oc,nenerg_s,ninit,ninit1, &
      ninitr,nlm_f,nlm_p_fp,nlm_pot,nlm_probe,nlmamax,nlms_n,npl,nr,nspin, &
      nspino,nspinp,numat_abs,Output_option,Pol_i_s_g,Pol_i_p_g,Pol_s_s_g,Pol_s_p_g,Powder,r,Rad_gon,Relativiste, &
      Renorm,Rmtg_abs,Rmtsd_abs,Rot_atom,rsato_abs,rsbdc,Shift,Spinorbite,Tau_abs,Temp,Theta_rixs,V_hubb,V_intmax, &
      V0bdcF,Vcato,Vec_i_g,Vec_s_g,Versus_loss,Vhbdc,VxcbdcF,Vxcato,Ylm_comp)

  use declarations
  implicit none

  integer:: i_f_out, i_q, i_theta, i_trans, icheck, ie_in, ie_loss, ie_out, index, ip_m, ip_max, ip0, ip1, ip2, ipr, isp, isp1, &
    isp2, L_emis, Lm1, Lm2, Lmax, Lmax_abs_t, Lmax_pot, Lseuil, m_hubb, multi_run, n_mpol, n_multi_run, n_process, n_q_dim, &
    n_rel, n_theta_rixs, n_trans, n_trans_out, natomsym, nb_Emis, nbseuil, ne_loss_or_emis, nenerg_emis, nenerg_in, nenerg_loss, &
    nenerg_loss_rixs, nenerg_oc, nenerg_s, nfinal, nfinal1, ninit, ninit1, ninitr, ninitr_dir, ninitr_indir, nlm_f, nlm_p_fp, &
    nlm_pot, nlm_probe, nlmamax, nlms_i, nlms_i_E1E1, nlms_i_E1E2, nlms_i_E1E3, nlms_i_E1M1, nlms_i_E2E2, nlms_i_E3E3, &
    nlms_i_M1M1, nlms_n, npl, nr, nspin, nspino, nspinp, numat_abs, Output_option

  integer, dimension(ninit,2):: m_g
  integer, dimension(ninit):: is_g
  integer, dimension(natomsym):: isymeq
  integer, dimension(:,:), allocatable:: m_f
  
  character(len=Length_name), dimension(n_multi_run):: File_name_rixs
  
  complex(kind=db), dimension(-m_hubb:m_hubb,nspinp,-m_hubb:m_hubb,nspinp):: V_hubb
  complex(kind=db), dimension(nenerg_s,nlm_probe,nlm_p_fp,nspinp,nspino,ip0:ip_max,nbseuil):: Rad_gon
  complex(kind=db), dimension(nenerg_s,nlmamax,nspinp,nlmamax,nspinp):: Tau_abs

  complex(kind=db), dimension(:,:,:), allocatable:: secmm
  complex(kind=db), dimension(:,:,:,:), allocatable:: Arof_sh, secdd, secmd
  complex(kind=db), dimension(:,:,:,:,:), allocatable:: secdq, secoo, secqq
  complex(kind=db), dimension(:,:,:,:,:,:), allocatable:: Mat_in, secdo
  complex(kind=db), dimension(:,:,:,:,:,:,:), allocatable:: F_gng, F_gng_L
  
  logical:: Analyzer, Core_resolved, Dip_rel, E1E1, E1E2, E1E3, E1M1, E2E2, E3E3, Eneg, Full_potential, HERFD, &
    Hubb_abs, Hubb_diag_abs, k_not_possible, Lmoins1, Lplus1, M1M1, Moment_conservation, Monocrystal, Powder, Relativiste, Renorm, &
    Spinorbite, Versus_loss, Write_bav, Ylm_comp

  logical, dimension(10):: Multipole_R

  real(kind=db):: alfpot, Conv_nelec_out, E_cut_rixs, E_emis, E_Fermi, E_i, E_loss, E_max, E_s, Ecinetic_i, &
    Ecinetic_s, Eclie, k_elec_i, k_elec_s, Mod_Q, Rmtg_abs, Rmtsd_abs, rsbdc, Temp, Theta, V_intmax, &
    V0, Vhbdc 

  real(kind=db), dimension(3):: Pol_i_s, Pol_i_p, Pol_s_s, Pol_s_p, Vec_i, Vec_s
  real(kind=db), dimension(3,3):: Rot_atom
  real(kind=db), dimension(nspin):: dV0bdcF, V0bdcF, VxcbdcF
  real(kind=db), dimension(nenerg_emis):: Energ_emis
  real(kind=db), dimension(nenerg_loss_rixs):: Energ_loss_rixs
  real(kind=db), dimension(nenerg_in):: Betalor, Energ_in
  real(kind=db), dimension(nenerg_s):: Energ_s
  real(kind=db), dimension(nbseuil):: Eseuil
  real(kind=db), dimension(ninitr):: Gamma, Shift
  real(kind=db), dimension(nr):: r, rsato_abs
  real(kind=db), dimension(ninit,2):: coef_g
  real(kind=db), dimension(n_theta_rixs,2):: Theta_rixs
  real(kind=db), dimension(nr,nlm_pot):: Vcato
  real(kind=db), dimension(nr,nlm_pot,nspin):: Vxcato
  real(kind=db), dimension(3,n_theta_rixs,n_q_dim):: Pol_i_s_g, Pol_i_p_g, Pol_s_s_g, Pol_s_p_g, Vec_i_g, Vec_s_g

  real(kind=db), dimension(:), allocatable:: Energ_loss
  real(kind=db), dimension(:,:), allocatable:: coef_f, RadMat_Emis, Val_trans
  real(kind=db), dimension(:,:,:), allocatable:: Int_RIXS
  real(kind=db), dimension(:,:,:,:,:,:), allocatable:: Int_out

  allocate( Int_out(nenerg_in,ne_loss_or_emis,npl,n_theta_rixs,n_q_dim,n_trans_out) )

  n_trans = max( n_trans_out - 1, 1 )
  
  Int_out(:,:,:,:,:,:) = 0._db

  HERFD = .false.
  ninitr_dir = 0
  ninitr_indir = ninitr

  nlms_i = 1  
  if( E1E1 ) then
    nlms_i_E1E1 = nlms_i
  else
    nlms_i_E1E1 = 0
  endif
  if( E1E2 ) then
    nlms_i_E1E2 = nlms_i
  else
    nlms_i_E1E2 = 0
  endif
  if( E1E3 ) then
    nlms_i_E1E3 = nlms_i
  else
    nlms_i_E1E3 = 0
  endif
  if( E1M1 ) then
    nlms_i_E1M1 = nlms_i
  else
    nlms_i_E1M1 = 0
  endif
  if( E2E2 ) then
    nlms_i_E2E2 = nlms_i
  else
    nlms_i_E2E2 = 0
  endif
  if( E3E3 ) then
    nlms_i_E3E3 = nlms_i
  else
    nlms_i_E3E3 = 0
  endif
  if( M1M1 ) then
    nlms_i_M1M1 = nlms_i
  else
    nlms_i_M1M1 = 0
  endif
  allocate( secdd(3,3,n_rel,nlms_i_E1E1 ) )
  allocate( secdq(3,3,3,2,nlms_i_E1E2) )
  allocate( secdo(3,3,3,3,2,nlms_i_E1E3) )
  allocate( secmd(3,3,2,nlms_i_E1M1) )
  allocate( secqq(3,3,3,3,nlms_i_E2E2) )
  allocate( secoo(3,9,3,9,nlms_i_E3E3) )
  allocate( secmm(3,3,nlms_i_E3E3) )

  nfinal = 0
  nfinal1 = 0
  allocate( Coef_f(nfinal,2) )
  allocate( m_f(nfinal,2) )
  nb_Emis = 0
  allocate( RadMat_Emis(nb_Emis,nbseuil) )
  L_emis = 0

  if( Versus_loss ) then
  
    nenerg_loss = nenerg_loss_rixs
    allocate( Energ_Loss(nenerg_loss) )
    Energ_Loss(:) = Energ_Loss_rixs(:)
     
  else
  
    nenerg_loss = 0
    allocate( Energ_Loss(nenerg_loss) ) 
    do index = 1,2
      call Loss_eval(Energ_emis,Energ_in,Energ_loss,index,nenerg_emis,nenerg_in,nenerg_loss)
      if( index == 2 ) exit
      deallocate( Energ_loss )
      allocate( Energ_Loss(nenerg_loss) )
    end do
  endif
    
  nlms_n = nlm_f * nspinp 

  n_mpol = ( ip_max - ip0 + 1 )**2 
  allocate( F_gng(nenerg_in,nenerg_loss,nlm_probe,nlm_probe,nspinp**2,n_mpol,ninitr_indir) )
  allocate( F_gng_L(nlm_probe,nspinp,nlm_probe,nspinp,ip0:ip_max,ip0:ip_max,ninitr_indir) )

! not used here, just for the call of Tensor_rixs
  allocate( Arof_sh(nlm_probe,nspinp,ip0:ip_max,ninitr_dir) )  
  allocate( Mat_in(nenerg_in,nlm_probe,nspinp,nlms_n,ip0:ip_max,ninitr_dir) )

  allocate( Val_trans(nenerg_loss,n_trans) ) 

  call Valence_transition(alfpot,dV0bdcF,E_cut_rixs,E_Fermi,Eclie,Eneg,Energ_loss,Energ_s,Full_potential, &
         Hubb_abs,Hubb_diag_abs,icheck,Lmax_abs_t,Lmax_pot,m_hubb,n_trans,nenerg_loss,nenerg_oc,nenerg_s,nlm_p_fp,nlm_pot, &
         nlmamax,nr,nspin,nspino,nspinp,numat_abs,r,Relativiste,Renorm,Rmtg_abs,Rmtsd_abs,rsato_abs, &
         rsbdc,Spinorbite,Tau_abs,V_hubb,V_intmax,Val_trans,Vcato,Vhbdc,VxcbdcF,Vxcato,Ylm_comp)
  
  Write(6,'(A)') ' Valence transitions calculated'
  
  call Excitation_process(alfpot,Betalor,Core_resolved,dV0bdcF,E_cut_Rixs,E_Fermi,Eclie,Eneg,Energ_in,Energ_Loss,Energ_s, &
           Eseuil,F_gng,Full_potential,Gamma,Hubb_abs,Hubb_diag_abs,icheck,ip0,ip_max,Lmax,Lmax_abs_t,Lmax_pot,m_hubb, &
           n_mpol,nbseuil,nenerg_in,nenerg_loss,nenerg_oc,nenerg_s,ninit1, &
           ninitr,nlm_p_fp,nlm_pot,nlm_probe,nlmamax,nr,nspin,nspino,nspinp,numat_abs,r,Rad_gon,Relativiste, &
           Renorm,Rmtg_abs,Rmtsd_abs,rsato_abs,rsbdc,Shift,Spinorbite,Tau_abs,Temp,V_hubb, &
           V_intmax,V0bdcF,Vcato,Vhbdc,VxcbdcF,Vxcato,Ylm_comp)

  allocate( Int_RIXS(npl,n_theta_rixs,n_q_dim) )
  
  do ie_in = 1,nenerg_in  ! loop over photon incoming energy

    write(6,'(/a12,f9.3,a3)') ' Energy in =', Energ_in(ie_in) * Rydb, ' eV'
    if( Versus_loss ) then 
      write(6,'(A)') ' Energy loss Intensity_s  Intensity_p'
    else
      write(6,'(A)') ' Energy out  Intensity_s  Intensity_p'
    endif

    E_i = Eseuil(nbseuil) + Energ_in(ie_in)

    if( icheck > 1 ) then
      write(3,'(/A)') ' ------------------------------------------------------------------------------------------ '
      write(3,'(/a27,f8.3,a3)') ' Incoming energy - E_edge =', Energ_in(ie_in) * Rydb, ' eV'
    endif
  
    do ie_out = 1,ne_loss_or_emis ! Loop over photon emission energy

      Int_RIXS(:,:,:) = 0._db     

      if( Versus_loss ) then
        E_loss = Energ_loss(ie_out) 
        E_emis = Energ_in(ie_in) - E_loss 
      else
        E_emis = Energ_emis(ie_out) 
        E_loss = Energ_in(ie_in) - E_emis  
      endif

      E_s = Eseuil(nbseuil) + E_emis

      if( E_loss > Energ_loss(nenerg_loss) + eps10 .or. E_loss < - eps10 ) cycle
 
      do ie_loss = 1,nenerg_loss
        if( abs( E_loss - Energ_loss(ie_loss) ) < eps10 ) exit
      end do
      if( ie_loss > nenerg_loss ) then
        call write_error
        do ipr = 6,9,3
          write(ipr,'(//a8,f13.5,a12/)') ' Eloss =', E_loss*rydb, ' not found !'
        end do
        stop
      endif

      E_max = min( Energ_in(ie_in), Energ_s(nenerg_s) )  

      if( icheck > 2 ) then
        write(3,'(/A)') ' ------------------------------------------------------------------------------------------ '
        write(3,'(/a9,f8.3,a3)') ' Outgoing energy - E_edge =', Energ_s(ie_out) * Rydb, ' eV'
      endif 
      
      Write_bav = icheck > 3 .or. ( icheck > 1 .and. ie_in == 1 .and. ie_out == 1 .and. i_f_out == 3 )

      isp = 0
      do isp1 = 1,nspinp
        do isp2 = 1,nspinp
          isp = isp + 1
          ip_m = 0
          do ip1 = ip0,ip_max
            do ip2 = ip0,ip_max
              ip_m = ip_m + 1
              do Lm1 = 1,nlm_probe
                do Lm2 = 1,nlm_probe
                  F_gng_L(Lm1,isp1,Lm2,isp2,ip1,ip2,:) = F_gng(ie_in,ie_loss,Lm1,Lm2,isp,ip_m,:)
                end do
              end do
            end do
          end do
        end do
      end do

! not used in indirect process      
      Conv_nelec_out = 1._db
      
      call Tensor_rixs(Arof_sh,Coef_f,Coef_g,Conv_nelec_out,Core_resolved,F_gng_L,HERFD,i_f_out,icheck,ie_in,ip_max,ip0, &
          is_g,L_emis,Lmax,Lmoins1,Lplus1,Lseuil,m_f,m_g,Mat_in,Multipole_R,n_rel,nb_Emis,nbseuil,nenerg_in,nfinal,nfinal1, &
          ninit1,ninit,ninitr_dir,ninitr_indir,nlm_probe,nlms_i,nlms_i_E1E1,nlms_i_E1E2,nlms_i_E1E3,nlms_i_E1M1,nlms_i_E2E2, &
          nlms_i_E3E3,nlms_i_M1M1,nspinp,nlms_n,numat_abs,RadMat_Emis,Rot_atom,secdd,secdo,secdq,secmd,secmm,secoo,secqq, &
          Write_bav,Ylm_comp)

      do i_q = 1,n_q_dim
        do i_theta = 1,n_theta_rixs
          if( Moment_conservation ) &
            write(6,'(a6,i3,a17,2f8.3)') ' i_q =',i_q,', Theta, 2Theta =', Theta_rixs(i_theta,:)*180/pi 
      
          Mod_Q = 0.5_db * alfa_sf * sqrt( E_s**2 + E_i**2 - 2 * E_s * E_i * cos( pi - Theta_rixs(i_theta,2) ) )
      
          if( Moment_conservation .and. .not. HERFD ) then
      
            V0 = sum( V0bdcF(:) ) / nspin
            Ecinetic_s = E_emis + E_Fermi - V0
            if( .not. Eneg ) Ecinetic_s = max( Ecinetic_s, Eclie ) 
            k_elec_s = sqrt( Ecinetic_s )
      
            Ecinetic_i = Energ_in(ie_in) + E_Fermi - V0
            if( .not. Eneg ) Ecinetic_i = max( Ecinetic_i, Eclie ) 
            k_elec_i = sqrt( Ecinetic_i )
      
            if( k_elec_s + k_elec_i < Mod_Q ) then
              if( icheck > 0 ) &
                Write(3,120) Energ_in(ie_in)*rydb, E_emis*rydb, k_elec_s, '+', k_elec_i, '<', Mod_Q
              k_not_possible = .true.
            endif
          
            if( abs( k_elec_i - k_elec_s ) > Mod_Q ) then
              if( icheck > 0 ) &
                Write(3,130) Energ_in(ie_in)*rydb, E_emis*rydb, k_elec_s, '-', k_elec_i, '>', Mod_Q
              k_not_possible = .true.
            endif
          
            if( k_not_possible ) then
              if( icheck > 0 ) Write(3,'(5x,A)') 'No possible channel at this energy loss'    
              cycle
            endif
            
          endif

          Pol_i_s(:) = Pol_i_s_g(:,i_theta,i_q)     
          Pol_i_p(:) = Pol_i_p_g(:,i_theta,i_q)     
          Pol_s_s(:) = Pol_s_s_g(:,i_theta,i_q)     
          Pol_s_p(:) = Pol_s_p_g(:,i_theta,i_q)     
          Pol_i_s(:) = Pol_i_s_g(:,i_theta,i_q)     
          Theta = Theta_rixs(i_theta,1)
          Vec_i(:) = Vec_i_g(:,i_theta,i_q)     
          Vec_s(:) = Vec_s_g(:,i_theta,i_q)
       
          call Cal_RIXS_int(Dip_rel,E_i,E_s,E1E1,E1E2,E1E3,E1M1,E2E2,E3E3,i_q,i_theta,isymeq,Int_RIXS,M1M1,Monocrystal, &
                  n_q_dim,n_rel,n_theta_rixs,natomsym,nlms_i,nlms_i_E1E1,nlms_i_E1E2, &
                  nlms_i_E1E3,nlms_i_E1M1,nlms_i_E2E2,nlms_i_E3E3,nlms_i_M1M1,npl,pol_i_s,pol_i_p,pol_s_s,pol_s_p, &
                  secdd,secdo,secdq,secmd,secmm,secoo,secqq,Theta,Vec_i,Vec_s)

        end do
      end do

      do i_trans = 1,n_trans
        if( n_trans == 1 ) then
          Int_out(ie_in,ie_out,:,:,:,i_trans) = Int_out(ie_in,ie_out,:,:,:,i_trans) &
                                              + ( E_i / E_s ) * Val_trans(ie_loss,i_trans) * Int_RIXS(:,:,:)
        else
          Int_out(ie_in,ie_out,:,:,:,1) = Int_out(ie_in,ie_out,:,:,:,1) &
                                              + ( E_i / E_s ) * Val_trans(ie_loss,i_trans) * Int_RIXS(:,:,:)
          Int_out(ie_in,ie_out,:,:,:,i_trans+1) = Int_out(ie_in,ie_out,:,:,:,i_trans+1) &
                                              + ( E_i / E_s ) * Val_trans(ie_loss,i_trans) * Int_RIXS(:,:,:)
        endif
      end do
 
      if( Versus_loss ) then
        write(6,'(f11.3,1p,2e13.5)') Energ_loss_rixs(ie_out)*Rydb, Int_out(ie_in,ie_out,1,1,1,1) + Int_out(ie_in,ie_out,2,1,1,1), &
                                                                Int_out(ie_in,ie_out,3,1,1,1) + Int_out(ie_in,ie_out,4,1,1,1)
      else 
        write(6,'(f11.3,1p,2e13.5)') Energ_emis(ie_out) * Rydb, Int_out(ie_in,ie_out,1,1,1,1) + Int_out(ie_in,ie_out,2,1,1,1), &
                                                                Int_out(ie_in,ie_out,3,1,1,1) + Int_out(ie_in,ie_out,4,1,1,1)
      endif
 
    end do ! end of loop over ie_out

  end do
  
  deallocate( secdd, secdq, secdo, secmd, secqq, secoo,secmm )
  deallocate( Arof_sh, Coef_f, F_gng, F_gng_L, Energ_Loss, m_f, Mat_in, RadMat_Emis, Val_trans )

  if( Versus_loss ) then
    call Write_out_RIXS(Analyzer,Energ_loss_rixs,Energ_in,File_name_rixs,HERFD,2,Int_out,multi_run,n_multi_run, &
                 n_process,n_q_dim,n_theta_rixs,n_trans_out,nenerg_loss_rixs,nenerg_in,npl,Output_option,Powder,Versus_loss)
  else  
    call Write_out_RIXS(Analyzer,Energ_emis,Energ_in,File_name_rixs,HERFD,2,Int_out,multi_run,n_multi_run, &
                      n_process,n_q_dim,n_theta_rixs,n_trans_out,nenerg_emis,nenerg_in,npl,Output_option,Powder,Versus_loss)
  endif
    
  return
  120 format(5x,'E_unoc =',f10.5,', E_occ =',f10.5,' eV, k_e_s =',f10.5,' ',a1,' k_e_i =',f10.5,' ',a1,' Q =',f10.5)
  130 format(5x,'E_unoc =',f10.5,', E_occ =',f10.5,' eV, abs( k_e_s =',f10.5,' ',a1,' k_e_i =',f10.5,') ',a1,' Q =',f10.5)
end

!***************************************************************

subroutine Loss_eval(Energ_emis,Energ_in,Energ_loss,index,nenerg_emis,nenerg_in,nenerg_loss)
 
  use declarations
  implicit none

  integer:: i, ie_in, ie_out, index, j, nenerg_emis, nenerg_in, nenerg_loss

  real(kind=db):: E
  real(kind=db), dimension(nenerg_emis):: Energ_emis
  real(kind=db), dimension(nenerg_in):: Energ_in
  real(kind=db), dimension(nenerg_loss):: Energ_loss
  real(kind=db), dimension(nenerg_in*nenerg_emis):: E_loss

  i = 0  
  do ie_in = 1,nenerg_in
    Loop_emis: do ie_out = 1,nenerg_emis
      if( Energ_in(ie_in) < Energ_emis(ie_out) - eps10 ) cycle
      E = Energ_in(ie_in) - Energ_emis(ie_out) 
      do j = 1,i
        if( abs( E - E_loss(j) ) < eps10 ) cycle Loop_emis 
      end do
      i = i + 1
      E_loss(i) = E
      if( index == 2 ) Energ_loss(i) = E
    end do Loop_emis
  end do
  
  nenerg_loss = i
  
  if( index == 1 ) return
  
! Ordering
  do i = 1,nenerg_loss
    do j = i+1,nenerg_loss
      if( Energ_loss(i) < Energ_loss(j) ) cycle
      E = Energ_loss(i)
      Energ_loss(i) = Energ_loss(j)
      Energ_loss(j) = E  
    end do
  end do
    
  return
end
 
!*****************************************************************************************************************

! Calculation of the the valence transition

subroutine Valence_transition(alfpot,dV0bdcF,E_cut_rixs,E_Fermi,Eclie,Eneg,Energ_loss,Energ_s,Full_potential, &
         Hubb_abs,Hubb_diag_abs,icheck,Lmax_abs_t,Lmax_pot,m_hubb,n_trans,nenerg_loss,nenerg_oc,nenerg_s,nlm_p_fp,nlm_pot, &
         nlmamax,nr,nspin,nspino,nspinp,numat_abs,r,Relativiste,Renorm,Rmtg_abs,Rmtsd_abs,rsato_abs, &
         rsbdc,Spinorbite,Tau_abs,V_hubb,V_intmax,Val_trans,Vcato,Vhbdc,VxcbdcF,Vxcato,Ylm_comp)

  use declarations
  implicit none

  integer:: i_trans, ich, icheck, ie, ie_loss, ie_min, ie_oc, ie_oc_2, ie_oc_3, ie_oc_min, ie_unoc, ie_unoc_2, &
    ie_unoc_3, iso1, iso12, iso2, iso3, iso34, iso4, isp, isp1, isp3, iss1, iss2, iss3, iss4, &
    L, L1, L2, L3, L4, L_hubbard, Lm1, Lm2, Lm3, Lm4, Lmax, Lmax_abs_t, Lmax_pot, Lmp1, Lmp2, Lmp3, Lmp4, Lmv1, Lmv2, Lmv3, Lmv4, &
    Lmt1, Lmt2, Lmt3, Lmt4, Lp1, Lp2, Lp3, Lp4, m_hubb, m1, m2, m3, m4, mp1, mp2, mp3, mp4, mv1, mv2, mv3, mv4, &
    n_trans, ne_oc, ne_unoc_max, ne_unoc_max_L, nenerg_loss, nenerg_oc, nenerg_s, nlm_p_fp, nlm_pot, &
    nlm0, nlm1, nlm2, nlmamax, nr, nspin, nspino, nspinp, numat_abs

  integer, dimension(:), allocatable:: ie_oc_first, ie_oc_last
  
  complex(kind=db), dimension(-m_hubb:m_hubb,nspinp,-m_hubb:m_hubb,nspinp):: V_hubb
  complex(kind=db), dimension(nenerg_s,nlmamax,nspinp,nlmamax,nspinp):: Tau_abs
  
  complex(kind=db), dimension(:,:,:,:,:,:,:), allocatable:: Val_rof

  logical:: Eneg, Ecomp, Full_potential, Hubb_abs, Hubb_diag_abs, Hubb_m, m_depend, Radial_comp, Relativiste, &
            Renorm, Spinorbite, Ylm_comp
       
  real(kind=db):: alfpot, E, E_cut_rixs, E_Fermi, E_loss, E_loss_max, Eclie, Eimag, Enervide, &
    f_integr3, intrad_r, Mat_tr, Mat_trans_ie, Rmtg_abs, Rmtsd_abs, rsbdc, Sum_Mat_trans, V_intmax, Vhbdc

  real(kind=db), dimension(nspin):: dV0bdcF, V0bdc, VxcbdcF
  real(kind=db), dimension(nr):: f, r, r1, r2, rsato_abs, t, t1, t2
  real(kind=db), dimension(nenerg_loss):: Energ_loss
  real(kind=db), dimension(nenerg_s):: Energ_s
  real(kind=db), dimension(nenerg_loss,n_trans):: Val_trans
  real(kind=db), dimension(nr,nlm_pot):: Vcato
  real(kind=db), dimension(nr,nlm_pot,nspin):: Vrato, Vxcato

  real(kind=db), dimension(:), allocatable:: Delta_E_oc2, E_loss_T, Mat_tr_Prod
  real(kind=db), dimension(:,:), allocatable:: Mat_trans_all
  real(kind=db), dimension(:,:,:,:,:,:), allocatable:: uv_i, uv_r

  Lmax = Lmax_abs_t
  
  ie_min = nenerg_oc + 1

  ich = max( icheck-1, 0 )

  Val_trans(:,:) = 0._db

  Eimag = 0._db

  if( icheck > 1 ) write(3,100)

  m_depend = Full_potential .or. Hubb_abs .or. Spinorbite
   
  if( m_depend ) then
    nlm1 = ( Lmax + 1 )**2
    nlm0 = 1
  else
    nlm0 = 0
    nlm1 = Lmax
  endif 
  if( Full_potential ) then
    nlm2 = nlm1
  elseif( Hubb_abs .and. .not. Hubb_diag_abs ) then
    nlm2 = nlm1
  else
    nlm2 = 1
  endif
  
  do ie_oc = 1,nenerg_s  
    if( Energ_s(ie_oc) > E_cut_rixs + eps10 ) exit
  end do
  ne_oc = ie_oc - 1
  
  E_loss_max = maxval( Energ_Loss )

! Energy range probed by the transitions
  do ie = 1,ne_oc
    if(  Energ_s(ie) + E_loss_max > E_cut_rixs + eps10 ) exit
  end do
  ie_oc_min = ie

  do ie = ne_oc + 1,nenerg_s
    if( Energ_s(ie) - E_loss_max > E_cut_rixs + eps10 ) exit
  end do
  ne_unoc_max = ie - 1

  allocate( uv_i(nr,nlm0:nlm1,nlm2,nspinp,nspino,ie_oc_min:ne_unoc_max) )
  allocate( uv_r(nr,nlm0:nlm1,nlm2,nspinp,nspino,ie_oc_min:ne_unoc_max) )
  uv_i(:,:,:,:,:,:) = 0._db
  uv_r(:,:,:,:,:,:) = 0._db

      
  do ie = ie_oc_min,ne_unoc_max

    E = Energ_s(ie)   

    call Radial_function(alfpot,dV0bdcF,E,E_Fermi,Eclie,Eimag,Eneg,Enervide,Full_potential,Hubb_abs,Hubb_diag_abs,ich,ie, &
             ie_oc_min,Lmax,Lmax_pot,m_hubb,ne_unoc_max,nlm_pot,nlm0,nlm1,nlm2,nr,nspin,nspino,nspinp,numat_abs, &
             r,Relativiste,Renorm,Rmtg_abs,Rmtsd_abs,rsato_abs,rsbdc,Spinorbite,uv_i,uv_r,V_hubb,V_intmax,V0bdc,Vcato,Vhbdc, &
             Vrato,Vxcato,VxcbdcF,Ylm_comp)

  end do

  allocate( Val_rof(ie_oc_min:ne_oc,ne_oc+1:ne_unoc_max,nlm0:nlm1,nspinp,nlm_p_fp,nlm_p_fp,nspino**2) )
  allocate( Mat_trans_all(ie_oc_min:ne_oc,ne_oc+1:ne_unoc_max) )

  Val_rof(:,:,:,:,:,:,:) = ( 0._db, 0._db )
  Mat_trans_all(:,:) = 0._db

! Radial integrals
  do L = 0,Lmax

    if( Hubb_abs .and. L == l_hubbard( numat_abs ) )  then
      Hubb_m = .true.
    else
      Hubb_m = .false.
    endif
    Radial_comp = Ecomp .or. ( Hubb_m .and. Ylm_comp )
  
    select case(L)
      case( 0 )
        r1(1:nr) = r(1:nr)
        r2(1:nr) = 1._db
      case( 1 )
        r1(1:nr) = r(1:nr)**2
        r2(1:nr) = 1 / r(1:nr)
      case default
        r1(1:nr) = r(1:nr)**(L+1)
        r2(1:nr) = r(1:nr)**(-L)
    end select

    do m1 = -L,L
      if( .not. ( m_depend .or. m1 == 0 ) ) cycle
      
      m2 = - m1
      
      if( m_depend ) then
        Lm1 = L**2 + L + 1 + m1
        Lm2 = L**2 + L + 1 + m2
      else
        Lm1 = L
        Lm2 = L
      endif

      do Lp1 = 0,Lmax
        if( .not. Full_potential .and. L /= Lp1 ) cycle
        do mp1 = -Lp1,Lp1
          if( nlm_p_fp == 1 .and. m1 /= mp1 ) cycle
          if( nlm_p_fp == 1 ) then
            Lmp1 = 1
          else
            Lmp1 = Lp1**2 + Lp1 + mp1    
          endif
   
          do Lp2 = 0,Lmax
            if( .not. Full_potential .and. L /= Lp2 ) cycle
            do mp2 = -Lp2,Lp2
              if( nlm_p_fp == 1 .and. m2 /= mp2 ) cycle
              if( nlm_p_fp == 1 ) then
                Lmp2 = 1
              else
                Lmp2 = Lp2**2 + Lp2 + mp2    
              endif

              do isp = 1,nspinp
              
                iso12 = 0
                
                do iso1 = 1,nspino
                  if( Spinorbite .and. nlm_p_fp == 1 ) then
                    mv1 = m1 - isp + iso1
                    if( mv1 > L .or. mv1 < -L ) cycle
                    Lmv1 = Lm1 + mv1 - m1
                  else
                    Lmv1 = Lm1
                  endif
                  
                  do iso2 = 1,nspino
      
                    if( Spinorbite .and. nlm_p_fp == 1 ) then
                      mv2 = m2 - isp + iso2
                      if( mv2 > L .or. mv2 < -L ) cycle
                      Lmv2 = Lm2 + mv2 - m2
                    else
                      Lmv2 = Lm2
                    endif
                    iso12 = iso12 + 1

                    do ie_oc = ie_oc_min,ne_oc

                      f(1:nr) = r1(1:nr) * uv_r(1:nr,Lm1,Lmp1,isp,iso1,ie_oc)
      
                      call ffintegr2_r(t1,f,r,nr,1,Rmtsd_abs)
      
                      f(1:nr) = r2(1:nr) * uv_r(1:nr,Lm1,Lmp1,isp,iso1,ie_oc)
      
                      call ffintegr2_r(t2,f,r,nr,-1,Rmtsd_abs)
                      
                      t(1:nr) = r2(1:nr) * t1(1:nr) + r1(1:nr) * t2(1:nr) 

                      do ie_unoc = ne_oc+1, ne_unoc_max 
 
                        f(1:nr) = uv_r(1:nr,Lm2,Lmp2,isp,iso2,ie_unoc) * t(1:nr)
                        intrad_r = f_integr3(r,f,1,nr,Rmtsd_abs)

! le 2 de 2 / (r-r')
                        Val_rof(ie_oc,ie_unoc,Lm1,isp,Lmp1,Lmp2,iso12) =  (-1)**m1 * 8 * pi * intrad_r / ( 2 * L + 1 )

                      end do  ! end of loop over ie_unoc
                    end do    ! end of loop over ie_oc
                     
                  end do ! end of loop over iso2
                end do   ! end of loop over iso1
              end do     ! end of loop over isp

            end do ! end of loop over mp2
          end do   ! end of loop over Lp1
        end do     ! end of loop over mp1
      end do       ! end of loop over Lp1

    end do ! end of loop over m1
  end do   ! end of loop over L

  allocate( Delta_E_oc2(ne_oc) ) 
  do ie_oc = 1,ne_oc
    if( ie_oc == 1 ) then      
      Delta_E_oc2(ie_oc) = Energ_s(ie_oc+1) - Energ_s(ie_oc )
    elseif( ie_oc < nenerg_s ) then
      Delta_E_oc2(ie_oc) = 0.5_db * ( Energ_s(ie_oc+1) - Energ_s(ie_oc-1 ) )
    else
      Delta_E_oc2(ie_oc) = Energ_s(ie_oc) - Energ_s(ie_oc - 1 )
    endif
  end do
  Delta_E_oc2(:) = Delta_E_oc2(:)**2

  do L1 = 0,Lmax
    L2 = L1
    do m1 = -L1,L1
      m2 = - m1

      Lmt1 = L1**2 + L1 + 1 + m1
      Lmt2 = L2**2 + L2 + 1 + m2
      if( m_depend ) then
        Lm1 = Lmt1
        Lm2 = Lmt2
      else
        Lm1 = L1
        Lm2 = L2
      endif

      do L3 = 0,Lmax
        L4 = L3
        do m3 = -L3,L3
          m4 = - m3
      
          Lmt3 = L3**2 + L3 + 1 + m3
          Lmt4 = L4**2 + L4 + 1 + m4
          if( m_depend ) then
            Lm3 = Lmt3
            Lm4 = Lmt4
          else
            Lm3 = L3
            Lm4 = L4
          endif

          do Lp1 = 0,Lmax
            if( .not. Full_potential .and. L1 /= Lp1 ) cycle
            do mp1 = -Lp1,Lp1
              if( nlm_p_fp == 1 .and. m1 /= mp1 ) cycle
              if( nlm_p_fp == 1 ) then
                Lmp1 = 1
              else
                Lmp1 = Lp1**2 + Lp1 + mp1    
              endif
    
              do Lp2 = 0,Lmax
                if( .not. Full_potential .and. L2 /= Lp2 ) cycle
                do mp2 = -Lp2,Lp2
                  if( nlm_p_fp == 1 .and. m2 /= mp2 ) cycle
                  if( nlm_p_fp == 1 ) then
                    Lmp2 = 1
                  else
                    Lmp2 = Lp2**2 + Lp2 + mp2    
                  endif

                  do Lp3 = 0,Lmax
                    if( .not. Full_potential .and. L3 /= Lp3 ) cycle
                    do mp3 = -Lp3,Lp3
                      if( nlm_p_fp == 1 .and. m3 /= mp3 ) cycle
                      if( nlm_p_fp == 1 ) then
                        Lmp3 = 1
                      else
                        Lmp3 = Lp3**2 + Lp3 + mp3    
                      endif
    
                      do Lp4 = 0,Lmax
                        if( .not. Full_potential .and. L4 /= Lp4 ) cycle
                        do mp4 = -Lp4,Lp4
                          if( nlm_p_fp == 1 .and. m4 /= mp4 ) cycle
                          if( nlm_p_fp == 1 ) then
                            Lmp4 = 1
                          else
                            Lmp4 = Lp4**2 + Lp4 + mp4    
                          endif

                          do isp1 = 1,nspinp
                            
                            iso12 = 0
                            do iso1 = 1,nspino
                              if( Spinorbite ) then
                                iss1 = iso1
                              else
                                iss1 = isp1
                              endif
                              if( Spinorbite .and. nlm_p_fp == 1 ) then
                                mv1 = m1 - isp1 + iso1
                                if( mv1 > L1 .or. mv1 < -L1 ) cycle
                                Lmv1 = Lmt1 + mv1 - m1
                              else
                                Lmv1 = Lmt1
                              endif

                              do iso2 = 1,nspino
                                if( Spinorbite ) then
                                  iss2 = iso2
                                else
                                  iss2 = isp1
                                endif
                                if( Spinorbite .and. nlm_p_fp == 1 ) then
                                  mv2 = m2 - isp1 + iso2
                                  if( mv2 > L1 .or. mv2 < -L1 ) cycle
                                  Lmv2 = Lmt2 + mv2 - m2
                                else
                                  Lmv2 = Lmt2
                                endif
                                iso12 = iso12 + 1
                              
                                do isp3 = 1,nspinp
                                  if( .not. Spinorbite .and. isp3 /= isp1 ) cycle

                                  iso34 = 0       
                                  do iso3 = 1,nspino
                                    if( Spinorbite ) then
                                      iss3 = iso3
                                    else
                                      iss3 = isp3
                                    endif
                                    if( Spinorbite .and. nlm_p_fp == 1 ) then
                                      mv3 = m3 - isp3 + iso3
                                      if( mv3 > L3 .or. mv3 < -L3 ) cycle
                                      Lmv3 = Lmt3 + mv3 - m3
                                    else
                                      Lmv3 = Lmt3
                                    endif
                                    do iso4 = 1,nspino
                                      if( Spinorbite ) then
                                        iss4 = iso4
                                      else
                                        iss4 = isp3
                                      endif
                                      if( Spinorbite .and. nlm_p_fp == 1 ) then
                                        mv4 = m4 - isp3 + iso4
                                        if( mv4 > L3 .or. mv3 < -L3 ) cycle
                                        Lmv4 = Lmt4 + mv4 - m4
                                      else
                                        Lmv4 = Lmt4
                                      endif
                                      iso34 = iso34 + 1

                                      do ie_unoc = ne_oc+1, ne_unoc_max
                                      
                                        if( icheck > 2 ) then
                                          E = Energ_s(ie_unoc) * Rydb 
                                          if( nlm_p_fp > 1 .and. Spinorbite ) then
                                            write(3,110) E, L1, m1, L3, m3, Lp1, mp1, Lp2, mp2, Lp3, mp3, isp1, iso1, iso2, &
                                                                                                          isp3, iso3, iso4
                                          elseif( nlm_p_fp > 1 ) then
                                            write(3,120) E, L1, m1, L3, m3, Lp1, mp1, Lp2, mp2, Lp3, mp3, isp1, isp3
                                          elseif( Spinorbite ) then
                                            write(3,130) E, L1, m1, L3, m3, isp1, iso1, iso2, isp3, iso3, iso4
                                          elseif( nspinp > 2 ) then
                                            write(3,140) E, L1, m1, L3, m3, isp1, isp3
                                          else
                                            write(3,150) E, L1, m1, L3, m3
                                          endif
                                        endif
                                         
                                        do ie_oc = ie_oc_min,ne_oc 
                                          Mat_trans_ie = Delta_E_oc2(ie_oc) &
                                                       * real( Val_rof(ie_oc,ie_unoc,Lm1,isp1,Lmp1,Lmp2,iso12) &
                                                       * Val_rof(ie_oc,ie_unoc,Lm3,isp3,Lmp3,Lmp4,iso34) &
                                                       * Tau_abs(ie_oc,Lmv1,iss1,Lmv3,iss3) &
                                                       * conjg( Tau_abs(ie_unoc,Lmv2,iss2,Lmv4,iss4) ), db )

                                          Mat_trans_all(ie_oc,ie_unoc) = Mat_trans_all(ie_oc,ie_unoc) + Mat_trans_ie

                                          if( icheck > 2 ) write(3,160) Energ_s(ie_oc)*rydb, real( Mat_trans_ie, db ) 
                                          
                                        end do                                        
                                        
                                      end do
                                                                       
                                    end do ! end of loop on iso4
                                  end do   ! end of loop on iso3        
                                end do     ! end of loop on isp3
       
                              end do ! end of loop on iso2
                            end do   ! end of loop on iso1
                          end do     ! end of loop on isp1
                          
                        end do ! end of loop on mp4
                      end do   ! end of loop on Lp4
                    end do ! end of loop on mp3
                  end do   ! end of loop on Lp3
                end do ! end of loop on mp2
              end do   ! end of loop on Lp2
            end do ! end of loop on mp1
          end do   ! end of loop on Lp1
       
        end do ! end of loop on m3
      end do   ! end of loop on L3
    
    end do ! end of loop on m1
  end do   ! end of Loop on L1

  if( nspinp == 1 ) Mat_trans_all(:,:) = 2 * Mat_trans_all(:,:)
  
  allocate( ie_oc_first(n_trans-1) )
  allocate( ie_oc_last(n_trans-1) )
  allocate( E_loss_T(n_trans-1) )
  allocate( Mat_tr_Prod(n_trans-1) )
  
  do ie_loss = 1,nenerg_loss

    do ie = ne_oc + 1,nenerg_s
      if( Energ_s(ie) - Energ_loss(ie_loss) > E_cut_rixs + eps10 ) exit
    end do
    ne_unoc_max_L = ie - 1

! Energy range probed for this loss energy
! for multiple transitions, to avoid double counting, the individual energy losses are decreasing versus the
! index of the transition.
! For the the first Loss range from Loss_Tot down to Loss_Tot / 2      
    do i_trans = 1,n_trans - 1
      E = Energ_loss(ie_loss) / 2**(i_trans - 1) 
      do ie = ie_oc_min,ne_oc
        if(  Energ_s(ie) + E > E_cut_rixs - eps10 ) exit
      end do
      ie_oc_first(i_trans) = ie
      if( i_trans > 1 ) then
        if( abs( Energ_s(ie) + E - E_cut_rixs ) < eps10 ) then
          ie_oc_last(i_trans-1) = ie
        else
          ie_oc_last(i_trans-1) = ie - 1
        endif
      endif
    end do
!    do i_trans = 1,n_trans - 2
!      ie_oc_last(i_trans) = ie_oc_first(i_trans+1) - 1
!    end do
    if( n_trans > 1 ) ie_oc_last(n_trans-1) = ne_oc

    E_loss = Energ_loss(ie_loss)
    if( E_loss < eps10 ) cycle
      
    Val_trans(ie_loss,1) = Sum_Mat_trans(ie_oc_min,E_cut_rixs,E_loss,Energ_s,Mat_trans_all,ne_oc,ne_unoc_max,nenerg_s)      

    if( n_trans == 1 ) cycle

    do ie_oc = ie_oc_first(1),ie_oc_last(1)
      do ie_unoc = ne_oc + 1, ne_unoc_max_L

        E_loss_T(1) = Energ_s(ie_unoc) - Energ_s(ie_oc)
        if( E_loss_T(1) > Energ_loss(ie_loss) + eps10 ) exit
        E_loss = Energ_loss(ie_loss) - E_loss_T(1)

        Mat_tr_Prod(1) = Mat_trans_all(ie_oc,ie_unoc)   
        if( ie_oc == ie_oc_last(1) .and. ie_oc_first(2) == ie_oc_last(1) ) Mat_tr_Prod(1) = 0.5_db * Mat_tr_Prod(1)  

        Mat_tr = Sum_Mat_trans(ie_oc_min,E_cut_rixs,E_loss,Energ_s,Mat_trans_all,ne_oc,ne_unoc_max,nenerg_s)

        Val_trans(ie_loss,2) = Val_trans(ie_loss,2) + Mat_tr_Prod(1) * Mat_tr 

        if( n_trans == 2 ) cycle    

        do ie_oc_2 = ie_oc_first(2),ie_oc_last(2)
          do ie_unoc_2 = ne_oc + 1, ne_unoc_max_L

            E_loss_T(2) = E_loss_T(1) + Energ_s(ie_unoc_2) - Energ_s(ie_oc_2)
            if( E_loss_T(2) > Energ_loss(ie_loss) + eps10 ) exit
            E_loss = Energ_loss(ie_loss) - E_loss_T(2)  

            Mat_tr_Prod(2) = Mat_tr_Prod(1) * Mat_trans_all(ie_oc_2,ie_unoc_2)   
            if( ie_oc_2 == ie_oc_last(2) .and. ie_oc_first(3) == ie_oc_last(2) ) Mat_tr_Prod(2) = 0.5_db * Mat_tr_Prod(2)  

            Mat_tr = Sum_Mat_trans(ie_oc_min,E_cut_rixs,E_loss,Energ_s,Mat_trans_all,ne_oc,ne_unoc_max,nenerg_s)

            Val_trans(ie_loss,3) = Val_trans(ie_loss,3) + Mat_tr_Prod(2) * Mat_tr 

            if( n_trans == 3 ) cycle    

            do ie_oc_3 = ie_oc_first(3),ie_oc_last(3)
              do ie_unoc_3 = ne_oc + 1, ne_unoc_max_L

                E_loss_T(3) = E_loss_T(2) + Energ_s(ie_unoc_3) - Energ_s(ie_oc_3)
                if( E_loss_T(3) > Energ_loss(ie_loss) + eps10 ) exit
                E_loss = Energ_loss(ie_loss) - E_loss_T(3)  
      
                Mat_tr_Prod(3) = Mat_tr_Prod(2) * Mat_trans_all(ie_oc_3,ie_unoc_3)   
      
                Mat_tr = Sum_Mat_trans(ie_oc_min,E_cut_rixs,E_loss,Energ_s,Mat_trans_all,ne_oc,ne_unoc_max,nenerg_s)
      
                Val_trans(ie_loss,4) = Val_trans(ie_loss,4) + Mat_tr_Prod(3) * Mat_tr
                
              end do
            end do
            
          end do
        end do 

      end do
    end do

  end do

  if( icheck > 0 ) then
    write(3,'(/a22,10(i1,12x))') ' Energ_loss Val_trans_', (i_trans, i_trans = 1,n_trans) 
    do ie_loss = 1,nenerg_loss
      write(3,170) Energ_loss(ie_loss)*rydb, Val_trans(ie_loss,:)
    end do
  endif
    
  deallocate( Delta_E_oc2, E_loss_T, ie_oc_first, ie_oc_last, Mat_tr_Prod, uv_i, uv_r, Val_rof )
    
  return
  100 format(/'---------- Valence_transition ',80('-'))
  110 format( ' Energ_Loss =',f10.3,', L1,m1,L3,m3 =',4i3,', Lp1,mp1,Lp2,mp2,Lp3,mp3,Lp4,mp4 =',8i3,', isp1,iso1,iso2 =',3i2, &
              ', isp3,iso3,iso4 =',3i2)
  120 format( ' Energ_Loss =',f10.3,', L1,m1,L3,m3 =',4i3,', Lp1,mp1,Lp2,mp2,Lp3,mp3,Lp4,mp4 =',8i3,', isp1,isp3 =',2i2)
  130 format( ' Energ_Loss =',f10.3,', L1,m1,L3,m3 =',4i3,', isp1,iso1,iso2 =',3i2,', isp3,iso3,iso4 =',3i2)
  140 format( ' Energ_Loss =',f10.3,', L1,m1,L3,m3 =',4i3,', isp1,isp3 =',2i2)
  150 format( ' Energ_Loss =',f10.3,', L1,m1,L3,m3 =',4i3)
  160 format(4x,'E_occ =',f10.3,' Mat =',1p,e13.5)
  170 format(f10.3,1p,10e13.5)
end

!*****************************************************************************************************************

subroutine Radial_function(alfpot,dV0bdcF,E,E_Fermi,Eclie,Eimag,Eneg,Enervide,Full_potential,Hubb_abs,Hubb_diag_abs,ich,ie, &
             ie_oc_min,Lmax,Lmax_pot,m_hubb,ne_unoc_max,nlm_pot,nlm0,nlm1,nlm2,nr,nspin,nspino,nspinp,numat_abs, &
             r,Relativiste,Renorm,Rmtg_abs,Rmtsd_abs,rsato_abs,rsbdc,Spinorbite,uv_i,uv_r,V_hubb,V_intmax,V0bdc,Vcato,Vhbdc, &
             Vrato,Vxcato,VxcbdcF,Ylm_comp)

  use declarations
  implicit none
  
  integer:: ich, ie, ie_oc_min, L, L_hubbard, Lfin, Lmax, Lmax_pot, m_hubb, n1, n2, ne_unoc_max, nlm, nlm_pot, nlm0, nlm1, &
    nlm2, nr, nrmtg, nrmtsd, nspin, nspino, nspinp, numat_abs

  logical:: Ecomp, Eneg, Hubb_abs, Hubb_diag_abs, Hubb_m, Full_potential, Radial_comp, Relativiste, Renorm, Spinorbite, Ylm_comp
 
  complex(kind=db), dimension(-m_hubb:m_hubb,nspinp,-m_hubb:m_hubb,nspinp):: V_hubb
  complex(kind=db), dimension(nspin):: konde 
 
  complex(kind=db), dimension(:,:,:,:), allocatable:: Tau

  real(kind=db):: alfpot, E, E_Fermi, Eclie, Eimag, Enervide, Rmtg_abs, Rmtsd_abs, rsbdc, V_intmax, Vhbdc

  real(kind=db), dimension(nspin):: dV0bdcF, Ecinetic, V0bdc, VxcbdcF
  real(kind=db), dimension(nr):: f2, r, rsato_abs
  real(kind=db), dimension(nr,nlm_pot):: Vcato
  real(kind=db), dimension(nr,nlm_pot,nspin):: Vrato, Vxcato
  real(kind=db), dimension(nr,nspin):: g0, gm, gp
  real(kind=db), dimension(nr,nspino):: gso
  real(kind=db), dimension(nr,nlm0:nlm1,nlm2,nspinp,nspino,ie_oc_min:ne_unoc_max):: uv_i, uv_r

  real(kind=db), dimension(:,:,:,:), allocatable:: V
  real(kind=db), dimension(:,:,:,:,:), allocatable:: ui, ur

  if( Full_potential ) then
    nlm = ( Lmax + 1 )**2
  else
    nlm = 1
  endif
  allocate( V(nr,nlm,nlm,nspin) )

  Enervide = E + E_Fermi

  call Potex_abs(alfpot,dV0bdcF,Enervide,ich,nlm_pot,nr,nspin,.false.,r,rsato_abs,rsbdc,V_intmax, &
                         V0bdc,Vcato,Vhbdc,Vrato,Vxcato,VxcbdcF)

  Ecinetic(:) = Enervide - V0bdc(:)
  if( .not. Eneg ) Ecinetic(:) = max( Ecinetic(:), Eclie )

  konde(:) = sqrt( cmplx(Ecinetic(:), Eimag,db) )
  
  if( ich > 1 ) then
    if( nspin == 1 ) then
      write(3,110) E*rydb, Ecinetic(:)*rydb, ( V0bdc(:) - E_Fermi )*rydb, real( konde(:) )
    else
      write(3,120) E*rydb, Ecinetic(:)*rydb, ( V0bdc(:) - E_Fermi )*rydb, real( konde(:) )
    endif
  endif

  call mod_V(ich,Lmax,Lmax_pot,nlm,nlm_pot,nr,nrmtg,nrmtsd,nspin,r,Rmtg_abs,Rmtsd_abs,V,V_intmax,V0bdc,Vrato,Ylm_comp)

  call coef_sch_rad(Enervide,f2,g0,gm,gp,gso,nlm,nr,nspin,nspino,numat_abs,r,Relativiste,Spinorbite,V)
  
  if( abs(Eimag) > eps10 .or. Ecinetic(1) < eps10 .or. Ecinetic(nspin) < eps10 ) then
    Ecomp = .true.
  else
    Ecomp = .false.
  endif
  
  if( Full_potential ) then
    Lfin = 0
  else
    Lfin = Lmax
  endif
  
  do L = 0,Lfin
 
    if( Hubb_abs .and. L == l_hubbard( numat_abs ) )  then
      Hubb_m = .true.
    else
      Hubb_m = .false.
    endif
    Radial_comp = Ecomp .or. ( Hubb_m .and. Ylm_comp )
 
    if( Full_potential ) then
      n1 = nlm    ! = ( Lmax + 1 )**2
      n2 = nlm
    elseif( Hubb_m .and. .not. Hubb_diag_abs ) then
      n1 = 2*L + 1
      n2 = n1
    elseif( Spinorbite .or. Hubb_m ) then
      n1 = 2*L + 1
      n2 = 1
    else
      n1 = 1
      n2 = 1
    endif

    allocate( ui(nr,n1,n2,nspinp,nspino) )
    allocate( ur(nr,n1,n2,nspinp,nspino) )
    allocate( Tau(n1,nspinp,n1,nspinp) )

    call Sch_radial(Ecinetic,Ecomp,Eimag,f2,Full_potential,g0,gm,gp,gso,Hubb_abs,Hubb_diag_abs,ich,konde, &
       L,Lmax,m_hubb,nlm,n1,n2,nr,nrmtg,nspin,nspino,nspinp,numat_abs,r,Radial_comp,Relativiste,Renorm,Rmtg_abs, &
       Spinorbite,Tau,ui,ur,V,V_hubb)

    if( Full_potential ) then
      uv_r(:,:,:,:,:,ie) = ur(:,:,:,:,:)
      if( Radial_comp ) uv_i(:,:,:,:,:,ie) = ui(:,:,:,:,:)
    elseif( Hubb_m .and. .not. Hubb_diag_abs ) then
      uv_r(:,L**2+1:L**2+nlm1,L**2+1:L**2+nlm1,:,:,ie) = ur(:,:,:,:,:)
      if( Radial_comp ) uv_i(:,L**2+1:L**2+nlm1,L**2+1:L**2+nlm1,:,:,ie) = ui(:,:,:,:,:)
    elseif( Spinorbite .or. Hubb_m ) then
      uv_r(:,L**2+1:L**2+nlm1,1,:,:,ie) = ur(:,:,1,:,:)
      if( Radial_comp ) uv_i(:,L**2+1:L**2+nlm1,1,:,:,ie) = ui(:,:,1,:,:)
    else
      uv_r(:,L,1,:,:,ie) = ur(:,1,1,:,:)
      if( Radial_comp ) uv_i(:,L,1,:,:,ie) = ui(:,1,1,:,:)
    endif

    deallocate( Tau, ui, ur )

  end do   ! end of loop over L

  deallocate( V )
  
  return
  110 format(/' Energ =',f8.3,', Ecinetic =',f8.3,', V0bdc =',f8.3,' eV, konde =',f8.5)
  120 format(/' Energ =',f8.3,', Ecinetic =',2f8.3,', V0bdc =',2f8.3,' eV, konde =',2f8.5)
end

!*****************************************************************************************************************

function Sum_Mat_trans(ie_oc_min,E_cut_rixs,E_loss,Energ_s,Mat_trans_all,ne_oc,ne_unoc_max,nenerg_s)

  use declarations
  implicit none

  integer:: ie, ie_oc_min, ie_oc, ne_oc, ne_unoc_max, nenerg_s
  
  real(kind=db):: E_cut_rixs, E_loss, E_oc_min, E_unoc, p, Sum_Mat_trans
  
  real(kind=db), dimension(nenerg_s):: Energ_s 
  real(kind=db), dimension(ie_oc_min:ne_oc,ne_oc+1:ne_unoc_max):: Mat_trans_all

  Sum_Mat_trans = 0._db

  E_oc_min = E_cut_rixs - E_loss - eps10     
  
  do ie_oc = ie_oc_min,ne_oc
  
    if( Energ_s(ie_oc) < E_oc_min ) cycle

    E_unoc = Energ_s(ie_oc) + E_loss
    if( E_unoc > Energ_s(nenerg_s) + eps10 ) exit
     
    do ie = ne_oc + 1,nenerg_s
      if( Energ_s(ie) > E_unoc - eps10 ) then
        if( abs( Energ_s(ie) - E_unoc ) < eps10 ) then
          Sum_Mat_trans = Sum_Mat_trans + Mat_trans_all(ie_oc,ie)
        else
          p = ( Energ_s(ie) - E_unoc ) / ( Energ_s(ie) - Energ_s(ie-1) )
          Sum_Mat_trans = Sum_Mat_trans + p * Mat_trans_all(ie_oc,ie-1) + ( 1 - p ) * Mat_trans_all(ie_oc,ie)
        endif 
        exit
      endif 
    end do

  end do
  
  return
end

!*****************************************************************************************************************

! Evaluation of the amplitude of the g - n - g transition (F_gng) 

subroutine Excitation_process(alfpot,Betalor,Core_resolved,dV0bdcF,E_cut_Rixs,E_Fermi,Eclie,Eneg,Energ_in,Energ_Loss,Energ_s, &
           Eseuil,F_gng,Full_potential,Gamma,Hubb_abs,Hubb_diag_abs,icheck,ip0,ip_max,Lmax,Lmax_abs_t,Lmax_pot,m_hubb, &
           n_mpol,nbseuil,nenerg_in,nenerg_loss,nenerg_oc,nenerg_s,ninit1, &
           ninitr,nlm_p_fp,nlm_pot,nlm_probe,nlmamax,nr,nspin,nspino,nspinp,numat_abs,r,Rad_gon,Relativiste, &
           Renorm,Rmtg_abs,Rmtsd_abs,rsato_abs,rsbdc,Shift,Spinorbite,Tau_abs,Temp,V_hubb, &
           V_intmax,V0bdcF,Vcato,Vhbdc,VxcbdcF,Vxcato,Ylm_comp)

  use declarations
  implicit none

  integer:: i_mp, icheck, ie, ie_p, ie_min, ie_in, ie_loss, initr, ip, ip0, ip_max, ip_p, is, iseuil, iso1, iso2, &
    iso23, iso3, iso4, isp, isp1, isp2, isp3, isp4, iss1, iss2, iss3, iss4, je, je_p, &
    L1, L2, L3, L4, Lm1, Lm2, Lm3, Lm4, Lmax, Lmax_abs_t, Lmax_m, Lmax_pot, Lmp1, Lmp2, Lmp3, Lmp4, Lmv1, Lmv2, Lmv3, Lmv4, &
    m_hubb, m1, m2, m3, m4, mv1, mv2, mv3, mv4, n_e, n_mpol, nbseuil, nenerg_e, nenerg_in, nenerg_loss, nenerg_oc, &
    nenerg_s, ninit1, ninitr, nlm_p, nlm_p_fp, nlm_pot, nlm_probe, nlmamax, nr, nspin, nspino, nspinp, numat_abs
    
  character(len=4), dimension(16):: Lm_string
  character(len=1), dimension(2):: Spin

  complex(kind=db):: CFac_p, Coef_Lorentz, f, f_p, f_p_scal

  complex(kind=db), dimension(nlm_probe,nlm_probe,nspinp**2,n_mpol):: f_nnp
  complex(kind=db), dimension(nenerg_s,nlmamax,nspinp,nlmamax,nspinp):: Tau_abs
  complex(kind=db), dimension(-m_hubb:m_hubb,nspinp,-m_hubb:m_hubb,nspinp):: V_hubb
  complex(kind=db), dimension(nenerg_in,nenerg_loss,nlm_probe,nlm_probe,nspinp**2,n_mpol,ninitr):: F_gng
  complex(kind=db), dimension(nenerg_s,nlm_probe,nlm_p_fp,nspinp,nspino,ip0:ip_max,nbseuil):: Rad_gon

  complex(kind=db), dimension(:), allocatable:: Cfac
  
  logical:: Core_resolved, Eneg, Full_potential, Hubb_abs, Hubb_diag_abs, Relativiste, Renorm, Spinorbite, Ylm_comp  
    
  real(kind=db):: alfpot, Conv_nelec, Delta, E_cut_Rixs, E_Fermi, E_m_V0, Eclie, Energ_out, Eph, Eph_min, &
    Ephoton_in, Ephoton_out, Gamma_tot, Rmtg_abs, Rmtsd_abs, rsbdc, Sca, Step, Temp, V_intmax, V0, Vhbdc

  real(kind=db), dimension(nenerg_in):: Betalor, Energ_in
  real(kind=db), dimension(nenerg_loss):: Energ_Loss
  real(kind=db), dimension(nenerg_s):: Energ_s
  real(kind=db), dimension(nbseuil):: Eseuil
  real(kind=db), dimension(nspin):: dV0bdcF, V0bdcF, VxcbdcF
  real(kind=db), dimension(ninitr):: Gamma, Shift
  real(kind=db), dimension(nr):: r, rsato_abs
  real(kind=db), dimension(nr,nlm_pot):: Vcato
  real(kind=db), dimension(nr,nlm_pot,nspin):: Vxcato

  real(kind=db), dimension(:), allocatable:: Energ_e, E_inf, E_sup
  real(kind=db), dimension(:,:,:,:,:,:,:), allocatable:: Scalar_L

  data Lm_string/ '0,+0','1,-1','1,+0','1,+1','2,-2','2,-1','2,+0','2,+1','2,+2','3,-3','3,-2','3,-1','3,+0','3,+1','3,+2','3,+3' /
  data Spin/ 'u','d' /

  F_gng(:,:,:,:,:,:,:) = (0._db, 0._db)

  V0 = sum( V0bdcF(:) ) / nspin
  E_m_V0 = E_Fermi - V0

  if( Temp > eps10 ) then
    ie_min = 1
  else
    ie_min = nenerg_oc + 1
  endif
  
  nlm_p = min( nlm_p_fp, nlmamax )
  Lmax_m = min( Lmax, Lmax_abs_t )
  
  allocate( Scalar_L(ie_min:nenerg_s,ie_min:nenerg_s,nlm_probe,nlm_p_fp,nlm_p_fp,nspinp,nspino**2) )   

  call Scalar_rad(alfpot,dV0bdcF,E_Fermi,Eclie,Eneg,Energ_s,Full_potential,Hubb_abs, &
         Hubb_diag_abs,icheck,ie_min,Lmax,Lmax_pot,m_hubb,nenerg_s,nlm_p_fp,nlm_pot, &
         nlm_probe,nr,nspin,nspino,nspinp,numat_abs,r,Relativiste,Renorm,Rmtg_abs,Rmtsd_abs,rsato_abs, &
         rsbdc,Spinorbite,Scalar_L,V_hubb,V_intmax,Vcato,Vhbdc,VxcbdcF,Vxcato,Ylm_comp)

  write(6,'(A)') ' Scalar product calculated'
  
  if( nenerg_s > 1 ) then
    Step = max( Energ_s(nenerg_s) - Energ_s(nenerg_s-1), 2._db / Rydb ) 
    Delta = 50 * ( Betalor(nenerg_in) + max( 2 * Gamma(1), 0.5_db ) )   ! Gamma hole is already divided by 2
    n_e = nint( Delta / Step )
    Step = Delta / n_e
    nenerg_e = nenerg_s + n_e
  else
    n_e = 0
    nenerg_e = nenerg_s
  endif
  allocate( Energ_e(nenerg_e) )
  Energ_e(1:nenerg_s) =  Energ_s(1:nenerg_s)
  do ie = 1,n_e
    Energ_e(nenerg_s+ie) = Energ_s(nenerg_s) + ie * Step 
  end do

! To avoid photon energy < 0 !  
  Eph_min = 0.001_db / Rydb
  
  allocate( E_inf(ie_min:nenerg_e) )
  allocate( E_sup(ie_min:nenerg_e) )
  allocate( Cfac(ie_min:nenerg_e) )
  
  if( nenerg_e - nenerg_oc == 1 ) then
    E_sup(ie_min) = Energ_e(nenerg_e) + 0.05_db / Rydb
    E_inf(ie_min) = Energ_e(nenerg_e) - 0.05_db / Rydb
  else
    if( Temp > eps10 ) then
      E_inf(ie_min) = 1.5_db * Energ_e(ie_min) - 0.5_db * Energ_e(ie_min+1)
    else
      E_inf(ie_min) = E_cut_Rixs
    endif  
    E_sup(ie_min) = ( Energ_e(ie_min+1) + Energ_e(ie_min) ) / 2
    do ie = ie_min+1,nenerg_e-1
      E_sup(ie) = ( Energ_e(ie+1) + Energ_e(ie) ) / 2
      E_inf(ie) = ( Energ_e(ie-1) + Energ_e(ie) ) / 2
    end do
    E_inf(nenerg_e) = ( Energ_e(nenerg_e-1) + Energ_e(nenerg_e ) ) / 2
    E_sup(nenerg_e) = 1.5_db * Energ_e(nenerg_e) - 0.5_db * Energ_e(nenerg_e-1) 
  endif

  do initr = 1,ninitr       ! ----------> Loop over edges or core states

    if( Core_resolved ) then
      if( initr <= ninit1 ) then
        iseuil = 1
      else
        iseuil = min(2, nbseuil)
      endif
    else
      iseuil = min(initr, nbseuil)
    endif
             
    do ie_in = 1,nenerg_in ! loop over photon incoming energy
 
      Ephoton_in = Eseuil(iseuil) + Energ_in(ie_in)
      
      Gamma_tot = Betalor(ie_in) + Gamma(initr)

      do ie = ie_min, nenerg_e  
        CFac(ie) = Coef_Lorentz(E_cut_Rixs,E_inf(ie),E_sup(ie),Energ_e(ie),Energ_in(ie_in),Gamma_tot,Shift(initr),Temp)
!      Conv_nelec = sqrt( cst * Conv_mbarn_nelec(Ephoton) / pi )
        Eph = Ephoton_in + Energ_e(ie)
        Eph  = max( Eph_min, Eph )   
        Conv_nelec = sqrt( 0.5_db ) * Eph
        CFac(ie) = CFac(ie) * Conv_nelec 
      end do

! In fact Betalor = 0, thus one does not mind that it is different for in and out energies 
      Gamma_tot = Gamma(initr) + Betalor(ie_in)
      
      do ie_loss = 1,nenerg_loss ! Loop over photon loss loss energy

        Energ_out = Energ_in(ie_in) - Energ_loss(ie_loss)          
        Ephoton_out = Eseuil(iseuil) + Energ_out
       
        do ie_p = ie_min, nenerg_e  ! Outward loop over non occupied electron states
            
          CFac_p = Coef_Lorentz(E_cut_Rixs,E_inf(ie_p),E_sup(ie_p),Energ_e(ie_p),Energ_out,Gamma_tot,Shift(initr),Temp)
!      Conv_nelec = sqrt( cst * Conv_mbarn_nelec(Ephoton) / pi )
          Eph = Ephoton_out + Energ_e(ie_p)
          Eph  = max( Eph_min, Eph )   
          Conv_nelec = sqrt( 0.5_db ) * Eph
          CFac_p = CFac_p * Conv_nelec 

          je_p = min( ie_p, nenerg_s )

          do ie = ie_min, nenerg_e  ! Inward loop over non occupied electron states

            je = min( ie, nenerg_s )
        
            f_nnp(:,:,:,:) = (0._db, 0._db )
            
    ! right part of state n_p    
            Lm1 = 0 
            do L1 = 0,Lmax_m
              do m1 = -L1,L1
                Lm1 = Lm1 + 1

                do isp1 = 1,nspinp

                  do Lmp1 = 1,nlm_p
                                  
                  do iso1 = 1,nspino
                    if( Spinorbite .and. nlm_p == 1 ) then
                      mv1 = m1 - isp1 + iso1  ! the m of the amplitude
                      if( mv1 > L1 .or. mv1 < -L1 ) cycle
                      Lmv1 = Lm1 + mv1 - m1
                      iss1 = iso1
                    elseif( nlm_p == 1 ) then 
                      Lmv1 = Lm1
                      iss1 = isp1
                    else
                      Lmv1 = Lmp1
                      iss1 = isp1
                    endif

    ! left part of state n_p with the scalar product
                    Lm2 = 0 
                    do L2 = 0,Lmax_m
                      do m2 = -L2,L2
                        Lm2 = Lm2 + 1
          
                        do isp2 = 1,nspinp
                          if( .not. Spinorbite .and. isp2 /= isp1 ) cycle 
                        
                          do Lmp2 = 1,nlm_p

                          iso23 = 0
                          
                          do iso2 = 1,nspino
                            if( Spinorbite .and. nlm_p == 1 ) then
                              mv2 = m2 - isp2 + iso2  ! the m of the amplitude
                              if( mv2 > L2 .or. mv2 < -L2 ) cycle
                              Lmv2 = Lm2 + mv2 - m2
                              iss2 = iso2
                            elseif( nlm_p == 1 ) then
                              Lmv2 = Lm2
                              iss2 = isp2
                            else
                              Lmv2 = Lmp2
                              iss2 = isp2
                            endif
                
                            do ip_p = ip0,ip_max
                  
                              f_p = Rad_gon(je_p,Lm1,Lmp1,isp1,iso1,ip_p,iseuil) * Tau_abs(je_p,Lmv1,iss1,Lmv2,iss2)

 ! Right part of the scalar product, state n: same L, m and isp
                              L3 = L2
                              m3 = m2
                              Lm3 = Lm2
                              isp3 = isp2
                                             
                             do Lmp3 = 1,nlm_p

                              do iso3 = 1,nspino
                                if( Spinorbite .and. nlm_p == 1 ) then
                                  mv3 = m3 - isp3 + iso3  ! the m of the amplitude
                                  if( mv3 > L3 .or. mv3 < -L3 ) cycle
                                  Lmv3 = Lm3 + mv3 - m3
                                  iss3 = iso3
                                elseif( nlm_p == 1 ) then
                                  Lmv3 = Lm3
                                  iss3 = isp3
                                else
                                  Lmv3 = Lmp3
                                  iss3 = isp3
                                endif
                                
                                if( Lmp3 == 1 .and. ip_p == ip0 ) iso23 = iso23 + 1
               
                                Sca = Scalar_L(je_p,je,Lm2,Lmp2,Lmp3,isp2,iso23)
                                
                                f_p_scal = f_p * Sca
               
      ! Left part of state n
                                Lm4 = 0 
                                do L4 = 0,Lmax_m
                                  do m4 = -L4,L4
                                    Lm4 = Lm4 + 1
               
                                    do isp4 = 1,nspinp
 
                                      is = isp4 + ( isp1 - 1 ) * nspinp
                                   
                                      do Lmp4 = 1,nlm_p

                                      do iso4 = 1,nspino
                                        if( Spinorbite .and. nlm_p == 1 ) then
                                          mv4 = m4 - isp4 + iso4  ! the m of the amplitude
                                          if( mv4 > L4 .or. mv4 < -L4 ) cycle
                                          Lmv4 = Lm4 + mv4 - m4
                                          iss4 = iso4
                                        elseif( nlm_p == 1 ) then
                                          Lmv4 = Lm4
                                          iss4 = isp4
                                        else
                                          Lmv4 = Lmp4
                                          iss4 = isp4
                                        endif
                      
                                        do ip = ip0,ip_max
               
                                          i_mp = ip - ip0 + 1 + ( ip_p - ip0 ) * ( ip_max - ip0 + 1 ) 

! conjugate to take into account the fact that tau_abs is multiplied by "- img", the product of 2 of them gives a "-" sign.                
                                          f = Rad_gon(je,Lm3,Lmp3,isp3,iso3,ip,iseuil) * conjg( Tau_abs(je,Lmv3,iss3,Lmv4,iss2) )
                                          
                                          f_nnp(Lm1,Lm4,is,i_mp) = f_nnp(Lm1,Lm4,is,i_mp) + f_p_scal * f 
                                        
                                        end do ! end of loop on ip
                              
                                      end do ! end of loop on iso4
                                      end do ! end of loop on Lmp4
                                    end do   ! end of loop isp4
                                  end do     ! end of loop on m4
                                end do       ! end of loop L4
               
                              end do  ! end of loop on iso3
                              end do ! end of loop on Lmp3
                              
                            end do   ! end of loop on ip_p
                            
                          end do  ! end of loop on iso2
                          end do  ! end of loop on Lmp2
                        end do    ! end of loop on isp2
                      end do      ! end of loop on m2
                    end do        ! end of loop on L2

                  end do  ! end of loop on iso1
                  end do  ! end of loop on Lmp1
                end do    ! end of loop on isp1
              end do      ! end of loop on m1
            end do        ! end of loop on L1
            
            F_gng(ie_in,ie_loss,:,:,:,:,initr) = F_gng(ie_in,ie_loss,:,:,:,:,initr) + Cfac_p * Cfac(ie) * f_nnp(:,:,:,:)   
            
          end do ! end of loop on ie 
        end do ! end of loop on ie_p

      end do  ! end of loop on ie_loss
    end do ! end of loop on ie_in
  end do ! end of loop on initr
  
  if( icheck > 0 ) then
    write(3,'(/A)') ' F_gng(ie_in,L1,m1,spin1,L2,m2,spin2,i_mp,initr)' 
    do ie_in = 1,nenerg_in
      write(3,110) Energ_in(ie_in)*rydb
      write(3,120) ((((( Lm_string(Lm1), Spin(isp), Lm_string(Lm2), Spin(isp), i_mp, initr, Lm1 = 1,nlm_probe), &
                                    Lm2 = 1,nlm_probe), isp = 1,nspinp), i_mp = 1,n_mpol,max(n_mpol-1,1)), initr = 1,ninitr )
      do ie_loss = 1,nenerg_loss
        write(3,130) Energ_loss(ie_loss)*rydb, ((((( F_gng(ie_in,ie_loss,Lm1,lm2,is,i_mp,initr), Lm1 = 1,nlm_probe), &
                                    Lm2 = 1,nlm_probe), is = 1,nspinp**2,3), i_mp = 1,n_mpol,max(n_mpol-1,1)), initr = 1,ninitr ) 
      end do
    end do
  endif

  deallocate( Cfac, E_inf, E_sup, Energ_e, Scalar_L )
    
  return
  110 format(/' Energ_in =',f13.3,' eV')
  120 format('  Energ_loss ', 10000('(',a4,',',a1,',',a4,',',a1,')',i1,'_',i1,2x,'im',1x))
  130 format(f13.3,1p,10000(1x,2e11.3))
end

!***********************************************************************

! The factor CFac = Delta_E / (  Energ_in(ie_in) - Energ_s(ie) - Shift(initr) + img * Gamma_tot )
! is replaced by the corresponding integral over energy for this energy step
         
function Coef_Lorentz(E_cut_Rixs,E_inf,E_sup,Energ_e,Energ_in,Gamma_tot,Shift,Temp)

  use declarations
  implicit none

  complex(kind=db):: Coef_Lorentz
  
  real(kind=db):: Delta_E_inf, Delta_E_sup, E_cut_Rixs, E_inf, E_sup, Energ_e, Energ_in, F_TM, Gamma_tot, kBT_i, Lorentzian_i, &
                  Lorentzian_r, Shift, Temp

  Delta_E_inf = Energ_in - E_inf - Shift
  Delta_E_sup = Energ_in - E_sup - Shift
              
  if( Gamma_tot > eps10 ) then                 
    Lorentzian_i = atan( Delta_E_sup / Gamma_tot ) - atan( Delta_E_inf / Gamma_tot )
    Lorentzian_r = - 0.5_db * log( ( Gamma_tot**2 + Delta_E_sup**2 ) / ( Gamma_tot**2 + Delta_E_inf**2 ) )
  else
    if( Delta_E_inf * Delta_E_sup < eps10 ) then
      if( abs( Delta_E_inf + Delta_E_sup ) < eps10 ) then
        Lorentzian_r = 0._db
      elseif( abs( Delta_E_inf ) < eps10 .or. abs( Delta_E_sup ) < eps10 ) then
        Lorentzian_r = - log( 4._db )   ! limitation on divergence
      else
        Lorentzian_r = log( - Delta_E_inf / Delta_E_sup )
      endif
      Lorentzian_i = - pi
    else
      Lorentzian_r = log( Delta_E_inf / Delta_E_sup )
      Lorentzian_i = 0._db
    endif
  endif  

  Coef_Lorentz = cmplx( Lorentzian_r, Lorentzian_i, db )

! Thomas-Fermi distribution (to avoid peak at E_F on f')
  if( Temp > eps10 ) then
    kBT_i = 10000 * Rydb / ( k_Boltzmann * e_electron * Temp )
    F_TM = 1 / ( 1 + exp( ( E_cut_Rixs - Energ_e ) * kBT_i ) )
    Coef_Lorentz = Coef_Lorentz * F_TM
  endif
  
  return
  
end
      
!*****************************************************************************************************************

! Calculation of the scalar product

subroutine Scalar_rad(alfpot,dV0bdcF,E_Fermi,Eclie,Eneg,Energ_s,Full_potential,Hubb_abs, &
         Hubb_diag_abs,icheck,ie_min,Lmax,Lmax_pot,m_hubb,nenerg_s,nlm_p_fp,nlm_pot, &
         nlm_probe,nr,nspin,nspino,nspinp,numat_abs,r,Relativiste,Renorm,Rmtg_abs,Rmtsd_abs,rsato_abs, &
         rsbdc,Spinorbite,Scalar_L,V_hubb,V_intmax,Vcato,Vhbdc,VxcbdcF,Vxcato,Ylm_comp)

  use declarations
  implicit none

  integer:: ich, icheck, ie, ie_min, ie_p, iso1, iso12, iso2, isp, L, Lm, Lm1, Lm2, Lmax, Lmax_pot, Lmp1, Lmp2, Lmt, &
    Lmv1, Lmv2, Lp1, Lp2, m, m_hubb, mp, mp1, mp2, mv1, mv2, nenerg_s, nlm_p_fp, nlm_pot, nlm_probe, nlm0, nlm1, nlm2, nr, &
    nspin, nspino, nspinp, numat_abs

  complex(kind=db), dimension(-m_hubb:m_hubb,nspinp,-m_hubb:m_hubb,nspinp):: V_hubb

  logical:: Eneg, Full_potential, Hubb_abs, Hubb_diag_abs, m_depend, Relativiste, Renorm, Spinorbite, Ylm_comp
       
  real(kind=db):: alfpot, E, E_Fermi, Eclie, Eimag, Enervide, f_integr3, Integral, &
    Rmtg_abs, Rmtsd_abs, rsbdc, V_intmax, Vhbdc

  real(kind=db), dimension(nspin):: dV0bdcF, V0bdc, VxcbdcF
  real(kind=db), dimension(nenerg_s):: Energ_s
  real(kind=db), dimension(nr):: f, r, r2, rsato_abs
  real(kind=db), dimension(nr,nlm_pot):: Vcato
  real(kind=db), dimension(nr,nlm_pot,nspin):: Vrato, Vxcato
  real(kind=db), dimension(ie_min:nenerg_s,ie_min:nenerg_s,nlm_probe,nlm_p_fp,nlm_p_fp,nspinp,nspino**2):: Scalar_L

  real(kind=db), dimension(:,:,:,:,:,:), allocatable:: uv_i, uv_r

  ich = max( icheck-1, 0 )

  Scalar_L(:,:,:,:,:,:,:) = 0._db

  if( icheck > 1 ) write(3,100)

  r2(1:nr) = r(1:nr)**2

  Eimag = 0._db

  m_depend = Full_potential .or. Hubb_abs .or. Spinorbite
   
  if( m_depend ) then
    nlm1 = ( Lmax + 1 )**2
    nlm0 = 1
  else
    nlm0 = 0
    nlm1 = Lmax
  endif 
  if( Full_potential ) then
    nlm2 = nlm1
  elseif( Hubb_abs .and. .not. Hubb_diag_abs ) then
    nlm2 = nlm1
  else
    nlm2 = 1
  endif

  allocate( uv_i(nr,nlm0:nlm1,nlm2,nspinp,nspino,ie_min:nenerg_s) )
  allocate( uv_r(nr,nlm0:nlm1,nlm2,nspinp,nspino,ie_min:nenerg_s) )
  uv_i(:,:,:,:,:,:) = 0._db
  uv_r(:,:,:,:,:,:) = 0._db

  do ie = ie_min,nenerg_s

    E = Energ_s(ie)   

    call Radial_function(alfpot,dV0bdcF,E,E_Fermi,Eclie,Eimag,Eneg,Enervide,Full_potential,Hubb_abs,Hubb_diag_abs,ich,ie,ie_min, &
             Lmax,Lmax_pot,m_hubb,nenerg_s,nlm_pot,nlm0,nlm1,nlm2,nr,nspin,nspino,nspinp,numat_abs,r,Relativiste,Renorm,Rmtg_abs, &
             Rmtsd_abs,rsato_abs,rsbdc,Spinorbite,uv_i,uv_r,V_hubb,V_intmax,V0bdc,Vcato,Vhbdc,Vrato,Vxcato,VxcbdcF,Ylm_comp)

  end do

  do L = 0,Lmax

    do m = -L,L
   !   the m of the Ylm
      if( .not. ( m_depend .or. m == 0 ) ) cycle 
      
      Lmt = L**2 + L + 1 + m
      if( m_depend ) then
        Lm = Lmt
      else
        Lm = L
      endif
      
      do Lp1 = 0,Lmax
        if( .not. Full_potential .and. Lp1 /= L ) cycle
        do mp1 = -Lp1,Lp1
          if( nlm_p_fp == 1 .and. mp1 /= m ) cycle
          if( nlm_p_fp == 1 ) then
            Lmp1 = 1
          else
            Lmp1 = Lp1**2 + Lp1 + mp1    
          endif

          do Lp2 = 0,Lmax
            if( .not. Full_potential .and. Lp2 /= L ) cycle
            do mp2 = -Lp2,Lp2
              if( nlm_p_fp == 1 .and. mp2 /= m ) cycle
              if( nlm_p_fp == 1 ) then
                Lmp2 = 1
              else
                Lmp2 = Lp2**2 + Lp2 + mp2    
              endif

              do isp = 1,nspinp

                iso12 = 0
                do iso1 = 1,nspino
                  if( Spinorbite .and. nlm_p_fp == 1 ) then
                    mv1 = m - isp + iso1  ! the m of the amplitude
                    if( mv1 > L .or. mv1 < -L ) cycle
                    Lmv1 = Lm + mv1 - m
                  else
                    Lmv1 = Lm
                  endif
 
                  do iso2 = 1,nspino

                    if( Spinorbite .and. nlm_p_fp == 1 ) then
                      mv2 = m - isp + iso2
                    if( mv2 > L .or. mv2 < -L ) cycle
                      Lmv2 = Lm1 - mv2 + m
                    else
                      Lmv2 = Lm2
                    endif
                    
                    iso12 = iso12 + 1
      
                    do ie_p = ie_min, nenerg_s  ! Outward loop over non occupied electron states
                      do ie = ie_p, nenerg_s  ! Inward loop over non occupied electron states

                        f(1:nr) = r2(1:nr) * uv_r(1:nr,Lm,Lmp1,isp,iso1,ie_p) * uv_r(1:nr,Lm,Lmp2,isp,iso2,ie)

                        Integral = f_integr3(r,f,1,nr,Rmtsd_abs)
            
                        if( m_depend ) then 
                          Scalar_L(ie_p,ie,Lmt,Lmp1,Lmp2,isp,iso12) = Integral
                        else
                          do mp = -L,L
                            Scalar_L(ie_p,ie,Lmt+mp,Lmp1,Lmp2,isp,iso12) = Integral
                          end do                 
                        endif

                      end do
                    end do

                  end do
                end do
              end do
            end do
            
          end do
        end do
      end do

    end do
  end do ! end of loop over L
            
  do ie_p = ie_min,nenerg_s
    do ie = ie_p + 1,nenerg_s
      Scalar_L(ie,ie_p,:,:,:,:,:) = Scalar_L(ie_p,ie,:,:,:,:,:)
    end do
  end do
  
  if( icheck > 1 ) then

    if( m_depend ) then
      write(3,'(/A)') '  Versus (L, m, Lmp1, Lmp2, isp, iso1, iso2)'
      write(3,130) ((((((( L, m, Lmp1, Lmp2, isp, iso1, iso2, Lmp1 = 1,nlm_p_fp), Lmp2 = 1,nlm_p_fp), iso1 = 1,nspino), &
                                                                iso2 = 1,nspino), m = -L,L), L = 0,Lmax), isp = 1,nspinp)
    elseif( nspinp == 2 ) then
      write(3,140) (( L, isp, L = 0,Lmax), isp = 1,nspinp)
    else
      write(3,150) ( L, L = 0,Lmax )
    endif 
    do ie_p = ie_min,nenerg_s
      do ie = ie_min,nenerg_s
        if( m_depend ) then 
          write(3,160) Energ_s(ie_p)*rydb, Energ_s(ie)*rydb, &
                         (((((( Scalar_L(ie_p,ie,L**2+L+1+m,Lmp1,Lmp2,isp,iso12), Lmp1 = 1,nlm_p_fp), Lmp2 = 1,nlm_p_fp), &
                                                           iso12 = 1,nspino**2), m = -L,L), L = 0,Lmax), isp = 1,nspinp)
        else
          write(3,160) Energ_s(ie_p)*rydb, Energ_s(ie)*rydb, (( Scalar_L(ie_p,ie,L**2+L+1,1,1,isp,1), L = 0,Lmax), isp = 1,nspinp)
        endif 
      end do
    end do
  endif
    
  deallocate( uv_i, uv_r )
    
  return
  100 format(/'---------- Scalar_rad ',80('-'))
  130 format(/'        E_p         E     ',1000(1x,8i2))
  140 format(/'        E_p         E     ',1000('  L =',i2,', isp =',i2,1x))   
  150 format(/'        E_p         E     ',1000(6x,'L =',i2,6x))
  160 format(2f13.3,1p,1000e17.9)
end

!*****************************************************************************************************************

!              L(x) = (1/(pi*b)) * 1 / ( 1 + ( (x-a)/b )**2 )

subroutine Conv_out_RIXS(Energ_emis,Gamma_hole,HERFD,Int_out,n_theta_rixs,n_q_dim,nenerg_emis,nenerg_in,npl)

  use declarations
  implicit none
 
  integer:: ie, je, n_theta_rixs, n_q_dim, nenerg_in, nenerg_emis, npl

  logical:: HERFD
  
  real(kind=db):: Gamma_hole
  real(kind=db), dimension(nenerg_emis):: Energ_emis, dE
  real(kind=db), dimension(nenerg_in,nenerg_emis,npl,n_theta_rixs,n_q_dim):: Int_out, Int_out_s

  if( HERFD .or. abs( Gamma_hole ) > eps10 .or. nenerg_emis < 3 ) return

  Int_out_s(:,:,:,:,:) = 0._db
    
  do ie = 2,nenerg_emis-1
    dE(ie) = ( Energ_emis(ie+1) - Energ_emis(ie-1) ) / 2  
  end do
  dE(1) = dE(2) / 2
  dE(nenerg_emis) = dE(nenerg_emis-1) / 2
  
  do ie = 1,nenerg_emis
    do je = 1,nenerg_emis
      Int_out_s(:,ie,:,:,:) = Int_out_s(:,ie,:,:,:) + dE(je) * Int_out_s(:,je,:,:,:) &
                                                    / ( 1 + ( ( Energ_emis(je) - Energ_emis(ie) ) / Gamma_hole )**2 )  
    end do
  end do

! No division by Gamma-hole because in input, inut is per energy. The guess is that Gamma is the analyzor width   
  Int_out(:,:,:,:,:) = Int_out_s(:,:,:,:,:) / pi

  return
end

!*****************************************************************************************************************

subroutine Write_out_RIXS(Analyzer,Energ_emis,Energ_in,File_name_rixs,HERFD,i_process,Int_out,multi_run,n_multi_run, &
                      n_process,n_q_dim,n_theta_rixs,n_trans_out,nenerg_emis,nenerg_in,npl,Output_option,Powder,Versus_loss)
  use declarations
  implicit none
 
  integer:: i_out, i_process, i_q, i_theta, i_trans, ie, ie_b, ipl, L, multi_run, n_multi_run, n_process, &
    n_theta_rixs, n_q_dim, n_trans, n_trans_out, ne_a, ne_b, ne_out, nenerg_in, nenerg_emis, npl, npl_out, Output_option

  integer, dimension(2):: ncol
  
  character(len=Length_name):: File_name
  character(len=Length_name), dimension(n_multi_run):: File_name_rixs

 ! character(len=13), dimension(4):: pol_name_in
 ! character(len=13), dimension(8):: pol_name
  
  logical:: Analyzer, E_incoming, HERFD, Powder, Versus_loss
  
  real(kind=db):: dE, delta 
  real(kind=db), dimension(nenerg_in):: Energ_in
  real(kind=db), dimension(nenerg_emis):: Energ_emis
  real(kind=db), dimension(nenerg_in,nenerg_emis,npl,n_theta_rixs,n_q_dim,n_trans_out):: Int_out

  real(kind=db), dimension(:), allocatable:: E_a, E_b
  real(kind=db), dimension(:,:,:,:), allocatable:: Sum_Int
  real(kind=db), dimension(:,:,:,:,:), allocatable:: Int_out_s

 ! data pol_name/ '  sigma-sigma', '    sigma-pi ', '    pi-sigma ', '     pi-pi   ','  right-sigma', '   left-sigma', &
 !                '    right-pi ', '    left-pi  ' /
 ! data pol_name_in/ '     sigma   ', '       pi    ', '     right   ', '      left   '/

  if( n_trans_out > 1 ) then
    n_trans = n_trans_out - 1
  else
    n_trans = 1
  endif
  
  do i_out = 1,2
  
    if( i_out == Output_option ) cycle

    E_incoming = i_out == 1   
 
 ! Output file name   
    File_name = File_name_rixs(multi_run)
    L = Len_trim(File_name) - 4
    call File_name_extention(E_incoming,File_name,i_process,L,n_process,Versus_loss)
    open(2, file = File_name )

    if( Powder ) then
      npl_out = 1
    elseif( .not. Analyzer ) then
      npl_out = npl / 2
    else
      npl_out = npl
    endif

    if( E_incoming ) then
      ne_a = nenerg_in
      ne_b = nenerg_emis
    else
      ne_a = nenerg_emis
      ne_b = nenerg_in
    endif
    if( ne_b > 1 ) then
      ne_out = ne_b + 1
    else
      ne_out = ne_b
    endif
    ncol(i_out) = ne_out * npl_out * n_theta_rixs 

    allocate( E_a(ne_a) )
    allocate( E_b(ne_b) )

    if( E_incoming ) then
      E_a(:) = Energ_in(:) * Rydb
      E_b(:) = Energ_emis(:) * Rydb
      if( Versus_loss ) E_b(:) = - E_b(:) 
    else
      E_a(:) = Energ_emis(:) * Rydb
      if( Versus_loss ) E_a(:) = - E_a(:) 
      E_b(:) = Energ_in(:) * Rydb
    endif

! Column names
    call Column_RIXS_name(Analyzer,E_b,E_incoming,HERFD,n_theta_rixs,n_trans_out,ne_b,ne_out,npl_out,Versus_loss)

    if( ne_b > 1 ) allocate( Sum_int(npl,n_theta_rixs,n_q_dim,n_trans_out) )
    if( .not. Analyzer ) allocate( Int_out_s(ne_out,npl_out,n_theta_rixs,n_q_dim,n_trans_out) )    
   
    do ie = 1,ne_a

      if( ne_b > 1 ) then
        Sum_Int(:,:,:,:) = 0._db
        Delta = 0._db  
        do ie_b = 1,ne_b
          if( E_incoming .and. HERFD ) then
            dE = 1._db
          else 
            if( ie_b == 1 ) then
              dE = 0.5_db * ( E_b(ie_b + 1) - E_b(ie_b) ) 
            elseif( ie_b == ne_b ) then
              dE = E_b(ie_b) - E_b(ie_b-1) 
            else
              dE = 0.5_db * ( E_b(ie_b + 1) - E_b(ie_b-1) ) 
            endif
            Delta= Delta + dE
          endif
          if( E_incoming ) then
            Sum_Int(:,:,:,:) = Sum_Int(:,:,:,:) + dE * Int_out(ie,ie_b,:,:,:,:)
          else
            Sum_Int(:,:,:,:) = Sum_Int(:,:,:,:) + dE * Int_out(ie_b,ie,:,:,:,:)
          endif
        end do
        if( .not. ( E_incoming .and. HERFD ) ) Sum_Int(:,:,:,:) = Sum_Int(:,:,:,:) / Delta
      endif

      if( E_incoming ) then
  
        if( Powder ) then

          if( ne_b > 1 ) then
            write(2,130) E_a(ie), ((( Int_out(ie,:,1,i_theta,i_q,i_trans), Sum_Int(1,i_theta,i_q,i_trans), &
                                                    i_trans = 1,n_trans_out), i_theta = 1,n_theta_rixs), i_q = 1,n_q_dim )
          else
            write(2,130) E_a(ie), ((( Int_out(ie,:,1,i_theta,i_q,i_trans), i_trans = 1,n_trans_out), i_theta = 1,n_theta_rixs), &
                                                                                                              i_q = 1,n_q_dim )
          endif
        elseif( .not. Analyzer ) then
          do ipl = 1,npl_out 
            Int_out_s(1:ne_b,ipl,:,:,:) = Int_out(ie,1:ne_b,2*ipl-1,:,:,:) + Int_out(ie,1:ne_b,2*ipl,:,:,:)
            if( ne_b > 1 ) Int_out_s(ne_out,ipl,:,:,:) = Sum_Int(2*ipl-1,:,:,:) + Sum_Int(2*ipl,:,:,:) 
          end do
          write(2,130) E_a(ie), (((( Int_out_s(:,ipl,i_theta,i_q,i_trans), i_trans = 1,n_trans_out), ipl = 1,npl_out), &
                                                                                i_theta = 1,n_theta_rixs), i_q = 1,n_q_dim)
        else
          if( ne_b > 1 ) then
            write(2,130) E_a(ie), (((( Int_out(ie,:,ipl,i_theta,i_q,i_trans), Sum_Int(ipl,i_theta,i_q,i_trans), &
                                               i_trans = 1,n_trans_out), ipl = 1,npl), i_theta = 1,n_theta_rixs), i_q = 1,n_q_dim)
          else
            write(2,130) E_a(ie), (((( Int_out(ie,:,ipl,i_theta,i_q,i_trans), i_trans = 1,n_trans_out), ipl = 1,npl), &
                                                                             i_theta = 1,n_theta_rixs), i_q = 1,n_q_dim)
          endif
        endif
      
      else

        if( Powder ) then
          if( ne_b > 1 ) then
            write(2,130) E_a(ie), ((( Int_out(:,ie,1,i_theta,i_q,i_trans), Sum_Int(1,i_theta,i_q,i_trans), &
                                                          i_trans = 1,n_trans_out), i_theta = 1,n_theta_rixs), i_q = 1,n_q_dim)
          else
            write(2,130) E_a(ie), ((( Int_out(:,ie,1,i_theta,i_q,i_trans), i_trans = 1,n_trans_out), i_theta = 1,n_theta_rixs), &
                                                                                                              i_q = 1,n_q_dim)
          endif
        elseif( .not. Analyzer ) then
          do ipl = 1,npl_out 
            Int_out_s(1:ne_b,ipl,:,:,:) = Int_out(1:ne_b,ie,2*ipl-1,:,:,:) + Int_out(1:ne_b,ie,2*ipl,:,:,:)
            if( ne_b > 1 ) Int_out_s(ne_out,ipl,:,:,:) = Sum_Int(2*ipl-1,:,:,:) + Sum_Int(2*ipl,:,:,:) 
          end do
          write(2,130) E_a(ie), (((( Int_out_s(:,ipl,i_theta,i_q,i_trans), i_trans = 1,n_trans_out), ipl = 1,npl_out), &
                                                                                    i_theta = 1,n_theta_rixs), i_q = 1,n_q_dim)
        else
          if( ne_b > 1 ) then
            write(2,130) E_a(ie), (((( Int_out(:,ie,ipl,i_theta,i_q,i_trans), Sum_Int(ipl,i_theta,i_q,i_trans), &
                                             i_trans = 1,n_trans_out), ipl = 1,npl), i_theta = 1,n_theta_rixs), i_q = 1,n_q_dim)
          else
            write(2,130) E_a(ie), (((( Int_out(:,ie,ipl,i_theta,i_q,i_trans), i_trans = 1,n_trans_out), ipl = 1,npl), &
                                                                                   i_theta = 1,n_theta_rixs), i_q = 1,n_q_dim)
          endif
        endif
  
      endif
      
    end do  
        
    Close(2)
  
    deallocate( E_a, E_b )
    if( ne_b > 1 ) deallocate( Sum_int )
    if( .not. Analyzer ) deallocate( Int_out_s )    

  end do
    
  return
  130 format(f10.3,1p,1000000e13.5)
end

!**********************************************************************************************************

! Column names

subroutine Column_RIXS_name(Analyzer,E_b,E_incoming,HERFD,n_theta_rixs,n_trans_out,ne_b,ne_out,npl_out,Versus_loss)

  use declarations
  implicit none
 
  integer:: i, i_t, i_trans, ie, ipl, Length, n_theta_rixs, n, n_trans_out, ne_b, ne_out, npl_out

  character(len=13):: Word
  character(len=2), dimension(4):: pol_suf_no_ana
  character(len=13), dimension(8):: pol_suf

  character(len=13), dimension(ne_out):: E_string
  character(len=13), dimension(n_trans_out*ne_out*npl_out*n_theta_rixs):: E_string_full
  
  logical:: Analyzer, E_incoming, HERFD, Versus_loss
  
  real(kind=db):: E
  
  real(kind=db), dimension(ne_b):: E_b

  data pol_suf/ '_ss', '_sp', '_ps', '_pp','_rs', '_ls', '_rp', '_lp' /
  data pol_suf_no_ana/ '_s', '_p', '_r', '_l' /

  i = 0
  do i_t = 1,n_theta_rixs
    do ipl = 1,npl_out
      do i_trans = 1,n_trans_out
        do ie = 1,ne_out
          i = i + 1
          Word = ' '
          if( ie == ne_out ) then
          if( ne_out == 1 ) then
              Word = 'Int'
            elseif( E_incoming .and. HERFD ) then
              Word = ' Sum'
            else
              Word = 'Aver'
            endif
          else
            E = E_b(ie)
            if( abs( nint( 10*E ) - 10*E ) < eps10 ) then
              write(Word,'(f13.1)') E
            elseif( abs( nint( 100*E ) - 100*E ) < eps10 ) then
              write(Word,'(f13.2)') E
            elseif( abs( nint( 1000*E ) - 1000*E ) < eps10 .or. E > 999.99_db .or. ( n_trans_out > 1 .and. i_trans > 1 ) ) then
              write(Word,'(f13.3)') E
            elseif( abs( nint( 1000*E ) - 1000*E ) < eps10 ) then
              write(Word,'(f13.4)') E
          else
              write(Word,'(f13.5)') E
            endif
          endif
          Word = adjustr(Word)
          E_string(ie) = Word
    
          if( n_trans_out > 1 .and. i_trans > 1 ) then
            Word = adjustl(Word)
            Length = len_trim(Word)
            if( Length+3 < 13 ) then 
              Word(Length+1:Length+3) = '_t'
              call ad_number(i_trans-1,Word,13)
            elseif( Length+2 < 13 ) then 
              Word(Length+1:Length+1) = '_'
              call ad_number(i_trans-1,Word,13)
            endif
          endif

          if( npl_out > 1 ) then
            Word = adjustl(Word)
            Length = len_trim(Word)
            if( Analyzer ) then
              if( Length+3 < 13 ) Word(Length+1:Length+3) = pol_suf(ipl)
            else
              if( Length+2 < 13 ) Word(Length+1:Length+3) = pol_suf_no_ana(ipl)
            endif
          endif
    
          if( n_theta_rixs > 1 ) then
            Word = adjustl(Word)
            Length = len_trim(Word)
            if( Length+3 < 13 ) then 
              Word(Length+1:Length+2) = '_a'
              call ad_number(i_t,Word,13)
            elseif( Length+2 < 13 ) then 
              Word(Length+1:Length+1) = '_'
              call ad_number(i_t,Word,13)
            endif
          endif
    
          Word = adjustr(Word)
          E_string_full(i) = Word
        end do
      end do
    end do
  end do

  n = n_trans_out * npl_out * n_theta_rixs 
  if( E_incoming ) then
    if( n == 1 ) then
      write(2,120) '   E_incom', ( E_string(:), i = 1, n )
    else
      write(2,120) '   E_incom', E_string_full(:)
    endif
  elseif( Versus_loss ) then
    if( n == 1 ) then
      write(2,120) '   E_loss ', ( E_string(:), i = 1, n )
    else
      write(2,120) '   E_loss ', E_string_full(:)
    endif
  else
    if( n == 1 ) then
      write(2,120) '   E_emis ', ( E_string(:), i = 1, n )
    else
      write(2,120) '   E_emis ', E_string_full(:)
    endif
  endif

  return
  120 format(a10,1000000a13)
end

!*****************************************************************************************************************

subroutine Write_out_RIXS_tot(File_name_rixs,multi_run,n_multi_run,nenerg_emis,nenerg_in,Output_option,Versus_loss)

  use declarations
  implicit none
 
  integer:: i, i_out, i_process, ie, j, L, Length, multi_run, n_multi_run, n_trans_out, ne_a, nenerg_in, nenerg_emis, &
    nnombre, Output_option
  
  integer, dimension(2):: ncol
  
  character(len=Length_name):: File_name
  character(len=10000):: Title
  character(len=Length_name), dimension(n_multi_run):: File_name_rixs

  logical:: E_incoming, Versus_loss
  
  real(kind=db), dimension(:), allocatable:: E_a
  real(kind=db), dimension(:,:,:), allocatable:: Int_out

 ! data pol_name/ '  sigma-sigma', '    sigma-pi ', '    pi-sigma ', '     pi-pi   ','  right-sigma', '   left-sigma', &
 !                '    right-pi ', '    left-pi  ' /
 ! data pol_name_in/ '     sigma   ', '       pi    ', '     right   ', '      left   '/

  do i_out = 1,2
    
    E_incoming = i_out == 1   
 
    if( i_out == Output_option ) cycle

    if( E_incoming ) then
      ne_a = nenerg_in
    else
      ne_a = nenerg_emis
    endif

    allocate( E_a(ne_a) )
 
 ! indirect process can have more columns 
    do i_process = 2,1,-1
  
 ! Output file name   
      File_name = File_name_rixs(multi_run)
      L = Len_trim(File_name) - 4
      call File_name_extention(E_incoming,File_name,i_process,L,2,Versus_loss)
      
      open(1, file = File_name )

      if( i_process == 2 ) then
        read(1,'(A)') Title
      else
        read(1,*)
      endif

      ncol(i_process) = nnombre(1,100000) - 1
      
      if( i_process == 2 ) then
        allocate( Int_out(ne_a,ncol(2),2) )
        Int_out(:,:,:) = 0._db
      endif
      
      do ie = 1,ne_a
        read(1,*) E_a(ie), Int_out(ie,1:ncol(i_process),i_process)
      end do
    
      Close(1)
    end do

    n_trans_out = ncol(2) / ncol(1)
    
    if( E_incoming ) then
      File_name(L+1:L+10) = '_total.txt'
    elseif( Versus_loss ) then
      File_name(L+1:L+15) = '_loss_total.txt'
    else
      File_name(L+1:L+15) = '_emis_total.txt'
    endif

    open(2, file = File_name )

    Length = Len_trim(Title)    
    write(2,'(A)') Title(1:Length)
    
    do ie = 1,ne_a
      do i = 1,ncol(1)
        j = ( n_trans_out ) * ( i - 1 ) + 1
        Int_out(ie,j,2) = Int_out(ie,j,2) + Int_out(ie,i,1) 
      end do 
      write(2,130) E_a(ie), Int_out(ie,1:ncol(2),2)
    end do

    deallocate( E_a, Int_out )
    
  end do
      
  return
  130 format(f10.3,1p,1000000e13.5)
end

!*****************************************************************************************************************

! Sum over the different sites

subroutine Write_out_RIXS_sum(Analyzer,Energ_emis,Energ_in,File_name_rixs,HERFD,multi_run,n_multi_run,n_process,n_theta_rixs, &
                              n_trans_out,nenerg_emis,nenerg_in,npl,Only_indirect,Output_option,Powder,Shift_vs_XANES,Versus_loss)

  use declarations
  implicit none
 
  integer:: i_out, i_process, ie, L, Length, multi_run, n_multi_run, ncol, ncol_2, ne_a, ne_b, n_process, n_theta_rixs, &
    n_trans_out, ne_out, nenerg_emis, nenerg_in, nnombre, npl, npl_out, Output_option

  character(len=Length_name):: File_name
  character(len=Length_name), dimension(n_multi_run):: File_name_rixs

  logical:: Analyzer, E_incoming, HERFD, Only_indirect, Powder, Versus_loss
 
  real(kind=db):: Shift_vs_XANES, Shift_vs_XANES_L
   
  real(kind=db), dimension(nenerg_in):: Energ_in
  real(kind=db), dimension(nenerg_emis):: Energ_emis

  real(kind=db), dimension(:), allocatable:: E_a, E_b
  real(kind=db), dimension(:,:), allocatable:: Int_out, Int_out_tot

 ! data pol_name/ '  sigma-sigma', '    sigma-pi ', '    pi-sigma ', '     pi-pi   ','  right-sigma', '   left-sigma', &
 !                '    right-pi ', '    left-pi  ' /
 ! data pol_name_in/ '     sigma   ', '       pi    ', '     right   ', '      left   '/
 
  Shift_vs_XANES_L = Shift_vs_XANES * Rydb

  do i_out = 1,2
    
    E_incoming = i_out == 1   
 
    if( i_out == Output_option ) cycle

    if( E_incoming ) then
      ne_a = nenerg_in
      ne_b = nenerg_emis
    else
      ne_a = nenerg_emis
      ne_b = nenerg_in
    endif

    allocate( E_a(ne_a) )
    allocate( E_b(ne_b) )

    if( E_incoming ) then
      if( Versus_loss ) then
        E_b(:) = - Energ_emis(:) * Rydb
      else
        E_b(:) = Energ_emis(:) * Rydb + Shift_vs_XANES_L
      endif
    else
      E_b(:) = Energ_in(:) * Rydb + Shift_vs_XANES_L
    endif

    if( ne_b > 1 ) then
      ne_out = ne_b + 1
    else
      ne_out = ne_b
    endif

    if( Powder ) then
      npl_out = 1
    elseif( .not. Analyzer ) then
      npl_out = npl / 2
    else
      npl_out = npl
    endif

    do i_process = 1,3
  
      if( Only_indirect .and. i_process /= 2 ) cycle
       
      do multi_run = 1,n_multi_run
      
 ! input file name   
        File_name = File_name_rixs(multi_run)
        L = Len_trim(File_name) - 4
        call File_name_extention(E_incoming,File_name,i_process,L,n_process,Versus_loss)
        open(1, file = File_name )

        read(1,*)

        ncol = nnombre(1,100000) - 1
        
        if( multi_run == 1 .and. ( i_process == 1 .or. Only_indirect ) ) then
          if( i_process == 1 ) then
            ncol_2 = ncol * n_trans_out
          else
            ncol_2 = ncol
          endif
          allocate( Int_out(ne_a,ncol_2) )
          allocate( Int_out_tot(ne_a,ncol_2) )
        endif
        if( multi_run == 1 ) Int_out_tot(:,:) = 0._db

        do ie = 1,ne_a
          read(1,*) E_a(ie), Int_out(ie,1:ncol)
        end do

        Int_out_tot(:,1:ncol) = Int_out_tot(:,1:ncol) + Int_out(:,1:ncol)
        
        Close(1)
         
      end do

      File_name = File_name_rixs(1)
      Length = Len_trim(File_name)
      do L = Length,1,-1
        if( File_name(L:L) == '_' ) exit
      end do
      L = L - 1
      call File_name_extention(E_incoming,File_name,i_process,L,n_process,Versus_loss)
      open(2, file = File_name )

! Column names
      if( i_process == 1 ) then
        call Column_RIXS_name(Analyzer,E_b,E_incoming,HERFD,n_theta_rixs,1,ne_b,ne_out,npl_out,Versus_loss)
      else
        call Column_RIXS_name(Analyzer,E_b,E_incoming,HERFD,n_theta_rixs,n_trans_out,ne_b,ne_out,npl_out,Versus_loss)
      endif

      if( E_incoming .or. .not. Versus_loss ) E_a(:) = E_a(:) + Shift_vs_XANES_L
 
      do ie = 1,ne_a
        write(2,130) E_a(ie), Int_out_tot(ie,1:ncol)
      end do
      
      Close(2)

    end do
    
    deallocate( E_a, E_b, Int_out, Int_out_tot )
      
  end do
      
  return
  130 format(f10.3,1p,1000000e13.5)
end

!*****************************************************************************************************************

! Extension of the file names

subroutine File_name_extention(E_incoming,File_name,i_process,L,n_process,Versus_loss)
  
  use declarations
 
  integer:: i_process, L, n_process

  character(len=Length_name):: File_name

  logical:: E_incoming, Versus_loss
  
  if( n_process == 1 ) then
  
    if( E_incoming ) then
      File_name(L+1:L+9) = '_emis.txt'
    elseif( Versus_loss ) then
      File_name(L+1:L+9) = '_loss.txt'
    else
      File_name(L:L+3) = '.txt'
    endif
    
  elseif( i_process == 1 ) then
  
    if( E_incoming ) then
      File_name(L+1:L+8) = '_dir.txt'
    elseif( Versus_loss ) then
      File_name(L+1:L+13) = '_loss_dir.txt'
    else
      File_name(L+1:L+13) = '_emis_dir.txt'
    endif
    
  elseif( i_process == 2 ) then
  
    if( E_incoming ) then
      File_name(L+1:L+10) = '_indir.txt'
    elseif( Versus_loss ) then
      File_name(L+1:L+15) = '_loss_indir.txt'
    else
      File_name(L+1:L+15) = '_emis_indir.txt'
    endif
    
  else
  
    if( E_incoming ) then
      File_name(L+1:L+10) = '_total.txt'
    elseif( Versus_loss ) then
      File_name(L+1:L+15) = '_loss_total.txt'
    else
      File_name(L+1:L+14) = 'emis_total.txt'
    endif
  endif

  return
end


